<?php

session_start();
require("class/config.php");
require("class/db.php");
$config = Config::get_instance();
$sql = "SELECT
  extrinsic_evaluation.idextrinsic_evaluation,
  extrinsic_evaluation.service,
  extrinsic_evaluation.scenario,
  extrinsic_evaluation.`condition`,
  extrinsic_evaluation.task,
  extrinsic_evaluation.sysuser,
  extrinsic_evaluation.compilation_time,
  extrinsic_evaluation.sysdate,
  extrinsic_conversation.filename as conversation,
  extrinsic_question.question,
  extrinsic_evaluation_item.value
FROM extrinsic_evaluation_item
  INNER JOIN extrinsic_evaluation
    ON extrinsic_evaluation_item.idextrinsic_evaluation = extrinsic_evaluation.idextrinsic_evaluation
  INNER JOIN extrinsic_conversation
    ON extrinsic_evaluation_item.idextrinsic_conversation = extrinsic_conversation.idextrinsic_conversation
  inner join extrinsic_question on extrinsic_evaluation_item.idextrinsic_question=extrinsic_question.idextrinsic_question";
$db = Database::get_instance();
$db->connect();
$out = $db->fetch_array($sql);
$jsondata = json_encode($out);
require_once './lib/PHPExcel.php';





// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Teleperformance")
        ->setLastModifiedBy($_SESSION["username"])
        ->setTitle("Sensei")
        ->setSubject("Sensei Report")
        ->setDescription("Task1 Export")
        ->setKeywords("")
        ->setCategory("");

// Set fixed Column label
$colLabel = array();


// Set Column [showcol] label
$colLabel[] = "Id";
$colLabel[] = "Service";
$colLabel[] = "Scenario";
$colLabel[] = "Condition";
$colLabel[] = "Task";
$colLabel[] = "User";
$colLabel[] = "Compilation time";
$colLabel[] = "Date";
$colLabel[] = "Conversation";
$colLabel[] = "Question";
$colLabel[] = "Answer";

// Set Column [showsubitem] label
// Print Column label
for ($i = 0; $i < count($colLabel); $i++) {
    $colString = PHPExcel_Cell::stringFromColumnIndex($i);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString . '1', $colLabel[$i]);
}

foreach ($out as $value) {
    $curr_row = $objPHPExcel->getActiveSheet()->getHighestRow();
    $curr_row++;
    $currentcol = 0;
    foreach ($value as $value2) {

        $currentcoString = PHPExcel_Cell::stringFromColumnIndex($currentcol);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($currentcoString . $curr_row, str_replace("\\n", " ", $value2));
        $currentcol++;
    }
}

// Set Column width

$objPHPExcel->getActiveSheet()->setTitle('Sensei Report');

// Add new sheet
$objWorkSheet = $objPHPExcel->createSheet(1);

//show post data
$i = 0;
foreach ($out as $k => $v) {
    $i++;
    $objWorkSheet->setCellValue('A' . $i, $k);
    $objWorkSheet->setCellValue('B' . $i, $v);
}



$objWorkSheet->setTitle("Postdata");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . date('Ymd') . '_report.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
