<?php

session_start();
require("class/config.php");
require("class/db.php");
$config = Config::get_instance();
$sql="  SELECT extrinsic_evaluation.idextrinsic_evaluation,
       extrinsic_evaluation.compilation_time,
       extrinsic_evaluation.sysuser,
       extrinsic_evaluation.sysdate,
       extrinsic_evaluation.scenario,
       extrinsic_evaluation.`condition`,
       extrinsic_evaluation.service,
       extrinsic_evaluation.task,
       extrinsic_question.question,
       extrinsic_evaluation_item.value
FROM extrinsic_evaluation_item
INNER JOIN extrinsic_evaluation ON extrinsic_evaluation_item.idextrinsic_evaluation = extrinsic_evaluation.idextrinsic_evaluation
INNER JOIN extrinsic_question ON extrinsic_question.idextrinsic_question = extrinsic_evaluation_item.idextrinsic_question
WHERE extrinsic_evaluation.task = '2' ";
$db = Database::get_instance();
$db->connect();
$out=$db->fetch_array($sql);
echo "{\"data\":" .json_encode($out). "}";