<?php
session_start();

if (!$_SESSION["username"]) header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/lang.php");
require("class/acof.php");
require("class/report.php");
require("class/elastic.php");

$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('Rep Page', $_SESSION["username"] );

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$report = Report::get_instance();

if ($_SERVER["REQUEST_METHOD"] == "POST" || $_SESSION['language']==''){
	//load the default language
	$lang -> translate($_POST['lang']);
	
	if ($_POST['flagxls']==1){
		$filename = "report_sensei_".date("Ymd").".xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: inline; filename=$filename");
	}
	
	
	//storage all session variables into post variables
	if (explode("?", basename($_SERVER["HTTP_REFERER"]))[0]=='listen_edit.php' || explode("?", basename($_SERVER["HTTP_REFERER"]))[0]=='listen_view.php' ){
		foreach ($_SESSION as $key => $value) {
			$_POST[$key] = $value;						
		}
	} 
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
	<?php echo $acof -> head_tag(); ?>	
	<script language="javascript">
		var strSt = "<?php echo $lang -> get_language($_SESSION["username"],'LabSpeechListen',$_SESSION["language"]);?>"; 

		//take the value of each checkbox 
		function ConCheckbox(idsubitem){
			if (document.getElementById('ckGen'+idsubitem).checked == true){
				document.getElementById('FlagGen'+idsubitem).value = "Y";
				document.getElementById('turn'+idsubitem).value = "";
				document.getElementById('turn'+idsubitem).className = "nodroppable";
			}
			else{
				document.getElementById('FlagGen'+idsubitem).value = "N";
				document.getElementById('turn'+idsubitem).className = "droppable";
				document.getElementById('turn'+idsubitem).value = strSt;
			}
		}
	</script>
  </head>
	
  <body>
	<?php 
		$acof -> navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']); 
                $consideration = $lang -> get_language($_SESSION["username"],'LabConsListen',$_SESSION["language"]);		
                
                
	?>
	
	<div class="container">
		<form action="report.php" method="post" id="form1" name="form1">
			<input type="hidden" name="lang" id="lang" value="<?php echo $_POST['lang'];?>"/>
			<div class="col-md-8">
				<br/>
				<?php 
					$LabInstrReport = $lang -> get_language($_SESSION["username"],'LabInstrReport',$_SESSION["language"]);
					echo $acof -> show_info('', $LabInstrReport, '', $case='report');
					
					echo $report -> show_search_form($_POST, $_SESSION);
					echo '<br />';				
					
					if ($_SERVER["REQUEST_METHOD"] == "POST") echo $report -> show_result ($_POST, $_SESSION);
				?>			
			</div>
			<input type="hidden" id="flagxls" name="flagxls" value="0"/>		
		</form>
	</div>

	<?php echo $acof -> foot_page('report'); ?>
	<script language="javascript">
		function View(idListen){ //show the monitoring
                    //window.open
		}
		
		 $(function(){
                    //date menu
                    $('#datesel').daterangepicker({		
                        posX: 175,
                        posY: 185
                    }); 		
		 });
	</script>
  </body>
</html>

<?php
    $db->close();
?>