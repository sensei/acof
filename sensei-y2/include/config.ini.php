<?php
//client ip address 
$_SESSION['ipaddress'] = getenv('HTTP_CLIENT_IP')?:
						getenv('HTTP_X_FORWARDED_FOR')?:
						getenv('HTTP_X_FORWARDED')?:
						getenv('HTTP_FORWARDED_FOR')?:
						getenv('HTTP_FORWARDED')?:
						getenv('REMOTE_ADDR');

//DB
define("_HOST_", "localhost");
define("_USERNAME_", "root");
define("_PASSWORD_", "sensei");
define("_DATABASE_", "sensei");

//PATH
//define("_PATH_TRS_LUNA_", "/var/www/html/sensei/trs/LUNA/TRANS");
//define("_PATH_AUDIO_LUNA_", "/var/www/html/sensei/trs/LUNA/AUDIO");
//define("_WEBPATH_AUDIO_LUNA_", "/sensei/trs/LUNA/AUDIO");

//define("_PATH_TRS_DECODA_", "/var/www/html/sensei/trs/DECODA/TRANS");
//define("_PATH_AUDIO_DECODA_", "/var/www/html/sensei/trs/DECODA/AUDIO");
//define("_WEBPATH_AUDIO_DECODA_", "/sensei/trs/DECODA/AUDIO");

define ("_PATH_TRS_", serialize (array ("LUNA"=>"/var/www/html/sensei/trs/LUNA/TRANS", "DECODA"=>"/var/www/html/sensei/trs/DECODA/TRANS")));
define ("_PATH_AUDIO_", serialize (array ("LUNA"=>"/var/www/html/sensei/trs/LUNA/AUDIO", "DECODA"=>"/var/www/html/sensei/trs/DECODA/AUDIO")));
define ("_WEBPATH_AUDIO_", serialize (array ("LUNA"=>"/sensei/trs/LUNA/AUDIO", "DECODA"=>"/sensei/trs/DECODA/AUDIO")));
define("_PATH_TEMPLATE_", "/var/www/html/sensei/xsl_templates");
?>