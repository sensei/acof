<?php
class MysqlClass
{
	public $host = ' ';   
	public $username = ' ';   
	public $password = ' ';   
	public $dbname = ' ';
	public $query = ' ';

	function connectdb($dbhost,$dbusername,$dbpassword,$dbname){		
		
		//initialize the connection
		$conndb = new mysqli($dbhost,$dbusername,$dbpassword);	
		
		/* check connection */
		if ($conndb->connect_errno) {
			printf("Connect failed: %s\n", $conndb->connect_error);
			return false;
		}
		else
			return true;
		
		//close the connection
		//$mysqli->close();		
	}

	function numRows($strQuery, $dbhost,$dbusername,$dbpassword,$dbname){
		$conndb = new mysqli($dbhost,$dbusername,$dbpassword);			
		// Change database
		mysqli_select_db($conndb,$dbname);	
		//run the query
		$result = $conndb->query($strQuery);
		return $result->num_rows;
	}
	
	function selectQuery($strQuery,$dbhost,$dbusername,$dbpassword,$dbname){
	
		//echo $strQuery;
	
		//initialize the connection
		$conndb = new mysqli($dbhost,$dbusername,$dbpassword);			
		// Change database
		mysqli_select_db($conndb,$dbname);	
		//run the query
		$result = $conndb->query($strQuery);
		$num_fields = mysqli_num_fields($result); 
		
		if(!$result) {
			die("SQL Error: " . mysqli_error($conndb));
		}
		else //if result is ok
		{			
			$arrVal = array();
			//push the record into arrData
			while($row = $result->fetch_array()){
				$arrVal[] = $row;
			}
			
			$arrData = array();
			$row = 0;
			
			for ($row = 0; $row < count($arrVal); $row++){
			
				//echo "row:".$row." ";				
				$strValueRow = "";
				
				//and get the value of each field of current row
				for ($col = 0; $col < ($num_fields); $col++)
				{
					//echo "col".$col.": ";
					//echo $arrVal[$row][$col].;
					
					$strValueRow = $strValueRow.$arrVal[$row][$col]."|";
				}
				//echo substr($strValueRow, 0, -1)."<br>";
				
				//put the data of current row
				$arrData[] = substr($strValueRow, 0, -1);				
			}
			
			$result->close();
			
			return $arrData;
		}
	
		$conndb->close();		
	}

	function selectQueryAssoc($strQuery,$dbhost,$dbusername,$dbpassword,$dbname){
	
		//echo $strQuery;
	
		//initialize the connection
		$conndb = new mysqli($dbhost,$dbusername,$dbpassword);			
		// Change database
		mysqli_select_db($conndb,$dbname);	
		//run the query
		$result = $conndb->query($strQuery);
		if(!$result) {
			die("SQL Error: " . mysqli_error($conndb));
		}
		else //if result is ok
		{			
			$arrVal = array();
			//push the record into arrData
			while($row = $result->fetch_assoc()){
				$arrVal[] = $row;
			}
			
			return $arrVal;
		}
	
		$conndb->close();		
	}
	
	function updateQuery($table,$updates,$id,$dbhost,$dbusername,$dbpassword,$dbname){
 
		//initialize the connection
		$conndb = new mysqli($dbhost,$dbusername,$dbpassword);			
		// Change database
		mysqli_select_db($conndb,$dbname);	
		
		$upds = array();
		
		foreach ($updates as $k=>$v) {
			$upds[] = $k .' = \'' .str_replace("'","\\'", $v).'\'';
		}
		
		$upd = "UPDATE ".$table." SET ".implode(', ', $upds)." WHERE ".$id.";";
		//echo $upd.'<br>';
		//exit;
		$conndb->query($upd);

		$lastid = $conndb->insert_id;

		$conndb->close();
		
		return 	$lastid;	
    }
	
	function InsertQuery($table,$inserts,$dbhost,$dbusername,$dbpassword,$dbname){
 
		//initialize the connection
		$conndb = new mysqli($dbhost,$dbusername,$dbpassword);			
		// Change database
		mysqli_select_db($conndb,$dbname);	
		
		$values = array_values($inserts);
		$keys = array_keys($inserts);
		
		foreach ($values as $k=>$v) {
			$values[$k] = str_replace("'","\\'", $v);
		}
		
		$ins = "INSERT INTO ".$table." (".implode(",", $keys).") VALUES ('".implode("','", $values)."');";
		//echo $ins.'<br>';
		
		//$conndb->query($ins);

		if (!$conndb->query($ins)) {
			printf("Errormessage: %s\n", $conndb->error);
		}
		else
			$lastid = $conndb->insert_id;
		

		$conndb->close();
		
		return 	$lastid;	
    }
	
	function DeleteQuery($table,$id,$dbhost,$dbusername,$dbpassword,$dbname){
 
		//initialize the connection
		$conndb = new mysqli($dbhost,$dbusername,$dbpassword);			
		// Change database
		mysqli_select_db($conndb,$dbname);	
		
		$queydel = "DELETE FROM ".$table." WHERE ".$id.";";
		//echo $queydel.'<br>';
		
		if (!$conndb->query($queydel)) {
			//printf("Errormessage: %s\n", $conndb->error);
			return printf("Errormessage: %s\n", $conndb->error);
		}
		else
			return 	'';
		
		$conndb->close();
		
    }
	
	function selectQueryJson($strQuery,$dbhost,$dbusername,$dbpassword,$dbname){
	
		//initialize the connection
		$conndb = new mysqli($dbhost,$dbusername,$dbpassword);			
		// Change database
		mysqli_select_db($conndb,$dbname);	
		//run the query
		$result = $conndb->query($strQuery);
		$num_fields = mysqli_num_fields($result); 
		
		if(!$result) {
			die("SQL Error: " . mysqli_error($conndb));
		}
		else //if result is ok
		{			
			$arrVal = array();
			
			//push the record into arrData
			while($row = $result->fetch_array()){
				$arrVal[] = $row;				
			}			
			
			$result->close();
			
			reset($arrVal);
			return $arrVal;
		}
	
		$conndb->close();		
	}
	
}
?>