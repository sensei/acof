<?php

require 'include/db.class.php';

session_start();

//take the client ip address 
$_SESSION['ipaddress'] = getenv('HTTP_CLIENT_IP')?:
						getenv('HTTP_X_FORWARDED_FOR')?:
						getenv('HTTP_X_FORWARDED')?:
						getenv('HTTP_FORWARDED_FOR')?:
						getenv('HTTP_FORWARDED')?:
						getenv('REMOTE_ADDR');

function Translate($lang){
	if ($lang!=''){
		if ($_SESSION['language'] != $lang){
			$_SESSION['language'] = $lang;
		}		
	}
	ELSE
		$_SESSION['language'] = 'Ita';
}

function ConvDate($datepost){	
	
	$date = DateTime::createFromFormat('d/m/Y', $datepost);
	return $date->format('Ymd');
}

function getLanguage($LabelName,$FlagLang){

	$msg = '';
	$strLabel = '';
	if ($LabelName!='' && $FlagLang!=''){

		//try to open db connection	
		if(OpenConn() === 1) {
		
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			//InsLog($message,$_POST['user']);

		}
		else
		{
			//check the user in database
			$query = "SELECT ".$FlagLang." FROM tblLanguage WHERE label = '".$LabelName."'";
			$arrReturn = array();
			//call the query function
			$arrReturn = QuerySelect($query);
			
			if (count($arrReturn)==0){
				//insert log
					//InsLog('Login Failed',$username,$conn);
					
				$msg = "<div class='alert alert-danger' role='alert'><strong>Error! </strong>Login failed. Wrong input data.</div>";
				return $msg;
			}
			else
			{
				//current language label
				$strLabel = $arrReturn[0];
				
				return checkinput($strLabel);
			}		
		}		
	}
}
	
function parseIniFile($section, $value) {
    $conf = parse_ini_file("config.ini", true);
	return $conf[$section][$value];
}						

function OpenConn() {
	$dbhost = parseIniFile('DB','HOST');
	$dbusername = parseIniFile('DB','USERNAME');
	$dbpassword = parseIniFile('DB','PASSWORD');  
	$dbname = parseIniFile('DB','DATABASE');
	$mysql = new MysqlClass;
	return $mysql->connectdb($dbhost,$dbusername,$dbpassword,$dbname);
}	

function QueryNumRows($query) {
	$dbhost = parseIniFile('DB','HOST');
	$dbusername = parseIniFile('DB','USERNAME');
	$dbpassword = parseIniFile('DB','PASSWORD');  
	$dbname = parseIniFile('DB','DATABASE');
	$mysql = new MysqlClass;
	return $mysql->numRows($query,$dbhost,$dbusername,$dbpassword,$dbname);
}

function QuerySelect($query) {
	$dbhost = parseIniFile('DB','HOST');
	$dbusername = parseIniFile('DB','USERNAME');
	$dbpassword = parseIniFile('DB','PASSWORD');  
	$dbname = parseIniFile('DB','DATABASE');
	$mysql = new MysqlClass;
	return $mysql->selectQuery($query,$dbhost,$dbusername,$dbpassword,$dbname);
}	

function QuerySelectJson($query) {
	$dbhost = parseIniFile('DB','HOST');
	$dbusername = parseIniFile('DB','USERNAME');
	$dbpassword = parseIniFile('DB','PASSWORD');  
	$dbname = parseIniFile('DB','DATABASE');
	$mysql = new MysqlClass;
	return $mysql->selectQueryJson($query,$dbhost,$dbusername,$dbpassword,$dbname);
}

function QuerySelectAssoc($query) {
	$dbhost = parseIniFile('DB','HOST');
	$dbusername = parseIniFile('DB','USERNAME');
	$dbpassword = parseIniFile('DB','PASSWORD');  
	$dbname = parseIniFile('DB','DATABASE');
	$mysql = new MysqlClass;
	return $mysql->selectQueryAssoc($query,$dbhost,$dbusername,$dbpassword,$dbname);
}	

function QueryInsert($table,$inserts) {
	$dbhost = parseIniFile('DB','HOST');
	$dbusername = parseIniFile('DB','USERNAME');
	$dbpassword = parseIniFile('DB','PASSWORD');  
	$dbname = parseIniFile('DB','DATABASE');
	
	$mysql = new MysqlClass;
	return $mysql->InsertQuery($table,$inserts,$dbhost,$dbusername,$dbpassword,$dbname);
}

function QueryUpdate($table,$updates,$id) {
	$dbhost = parseIniFile('DB','HOST');
	$dbusername = parseIniFile('DB','USERNAME');
	$dbpassword = parseIniFile('DB','PASSWORD');  
	$dbname = parseIniFile('DB','DATABASE');
	
	$mysql = new MysqlClass;
	return $mysql->UpdateQuery($table,$updates,$id,$dbhost,$dbusername,$dbpassword,$dbname);
}

function checkinput($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = html_entity_decode($data);
   return $data;
}

function checkLogin($user,$pass){

	$msg = '';
	if ($user!='' && $pass!=''){

		//try to open db connection	
		if(OpenConn() === 1) {
		
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			//InsLog($message,$_POST['user']);

		}
		else
		{
			$username = $_POST['user'];
			$password = $_POST['pass'];
				
			//check the user in database
			$query = "SELECT nome, cognome, FlagWrite, FlagRead, FlagAdmin FROM tblUser WHERE login = '".$username."' and password = md5('".$password."')";
			$arrReturn = array();
			//call the query function
			$arrReturn = QuerySelect($query);
			
			if (count($arrReturn)==0){
				//insert log
				InsLog('Login Failed',$username);
					
				$msg = "<div class='alert alert-danger' role='alert'><strong>Error! </strong>Login failed. Wrong input data.</div>";
				return $msg;
			}
			else
			{
				//divided by "," the 2 field(name,surname)
				$arrLog = explode("|",$arrReturn[0]);
				
				$name = $arrLog[0];
				$surname = $arrLog[1];
				$flagW = $arrLog[2];
				$flagR = $arrLog[3];
				$flagA = $arrLog[4];
				
				//save in the session data
				$_SESSION['username'] = $username;
				$_SESSION['name'] = $name;
				$_SESSION['surname'] = $surname;
				$_SESSION['flagW'] = $flagW;
				$_SESSION['flagR'] = $flagR;
				$_SESSION['flagA'] = $flagA;

				//insert log
				InsLog('Login Success',$username);
				return $msg;
			}		
		}		
	}
}

/*trasforma il file trs in xml */
function processXSL($xml, $template) {
	error_reporting(E_ALL);
	$xsl = new DOMDocument();
	$xsl->load($template);

	$xsl_proc = new XsltProcessor();
	$xsl_proc->registerPHPFunctions();
	$xsl_proc->importStyleSheet($xsl);

	$xml_doc = new DOMDocument();
	$xml_doc->loadXML($xml);

	$xmlDiv = $xsl_proc->transformToXML($xml_doc);
	
	//ritorna la lunghezza del solo testo + il testo con i tempi inizio fine
	return '|'.LenValueXML($xml).'|'.$xmlDiv;
	
}

/*estraggo la lunghezza delle stringhe contenute nei nodi dell'xml*/
function LenValueXML($xml){
	
	/*estraggo tutti i contenuti dei nodi dall'xml*/
	$xmlstr = new SimpleXMLElement($xml);
	$strTrs = $xmlstr->asXML();
	$lenTrs = strlen($strTrs);
	
	return $lenTrs;
}

//log insert function for each user action
function InsLog($action,$user){	

	QueryInsert('tblLog', array(
		'Action' => $action,
		'sysuser' => $user,
		'SessionID' => session_id(),
		'IpAddress' => $_SESSION['ipaddress'],
	));	
}

function getTranscriptionsToBeProcessed($service) { 

	// get all transcriptions inside the folder:
	$files = opendir(parseIniFile('PATH', 'PATH_TRS_'.$service));
	$ret = array();
	while ($filename = readdir($files)){
		//take the file extensions
		$extn = explode('.',$filename);
		$extn = array_pop($extn);
	
		if ($filename != '.' && $filename != '..' && $extn=='trs') {
			//check the item has been done
			$query  = "SELECT * FROM tblListen WHERE FileName = '$filename' AND sysuser = '".$_SESSION["username"]."'";
			if (QueryNumRows($query) <= 0) {
				$ret[] = $filename;
			}
		}

	}
	sort($ret);
	return $ret;
}

//da un array, creo un array muldim. con json da passare al js per i charts
function getValuesByJsonFromQuery($arrReturnTot) { 
	
	/* output example
	{
		type: 'test' ,
		percent: 90,
		color: getRandomColor(),
		subs: [
				{ type: "Oil", percent: 45 },
				{ type: "Coal", percent: 40 },
				{ type: "Nuclear", percent: 5 }
			]
	},
	*/
	
	$strKeyVal = '';
	$jsonData = json_encode($arrReturnTot, JSON_PRETTY_PRINT);
	$phpArray = json_decode($jsonData);
	
	foreach ($phpArray as $key => $value) { 		
		foreach ($value as $keys => $values) { 
			if ($keys=='type'){
				//echo "<p>$keys | $values</p>";
				$arrKey[] = ''.$keys.': '.'"'.$values.'", ';
			}
			if ($keys=='percent'){
				//echo "<p>$keys | $values</p>";
				$arrVal[] = $keys.': '.$values;
			}			
		}
	}
	//make the output string for the js
	for ($i = 0; $i < count($arrKey); $i++){
		$strKeyVal .= $arrKey[$i].$arrVal[$i].',|';
	}
	
	//echo substr($strKeyVal, 0, strlen($strKeyVal)-2);
		
	//remove the last ","
	return substr($strKeyVal, 0, strlen($strKeyVal)-2);	
}
?>