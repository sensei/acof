<?php
session_start();

error_reporting(0);
ini_set("display_errors", 0);

if (!$_SESSION["username"])
    header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/annotationform.php");

//implementing ElasticSearch
require("class/elastic.php");

$config = Config::get_instance();

$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('Listen Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$annotationform = Annotationform::get_instance();

$elastic = Elastic::get_instance();
$client = $elastic->connect();

$msgerr = "";
$nerr = 0;
$str = "*";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $lang->translate($_POST['lang']);

    //if postback is true by save button
    if ($_POST["flagsave"] === "1") {
        if (empty($_POST["filename"])) {
            $flagerrfile = "<span class='error'>" . $str . "</span>";
        } else {
            $filename = $acof->checkinput($_POST["filename"]);
            $flagerrfile = '';
        }

        if ($_POST["finalnote"] === '') {
            $flagerrcons = "<span class='error'>" . $str . "</span>";
        } else {
            $finalnote = $acof->checkinput($_POST["finalnote"]);
            $flagerrcons = '';
        }

        if ($_POST["synopsisnote"] === '') {
            $flagerrsyn = "<span class='error'>" . $str . "</span>";
        } else {
            $synopsisnote = $acof->checkinput($_POST["synopsisnote"]);
            $flagerrsyn = '';
        }


        foreach ($_POST as $key => $value) {
            switch ($key) {
                case stripos($key, "idsub"):
                    $ArrIdSub[] = $_POST[$key];

                    //if empty, show "*" alert
                    if ($_POST["radio" . $_POST[$key]] == '') {
                        $_POST["flagerr" . $_POST[$key]] = $str;
                    } else {
                        $_POST["flagerr" . $_POST[$key]] = "";
                    }
                    break;

                //selected value
                case stripos($key, "radio"):
                    $ArrRadio[] = $_POST[$key];
                    break;

                case stripos($key, "subweight"):
                    $_POST[$key] . "<br>";
                    $ArrWeight[] = $_POST[$key];
                    break;

                case stripos(strtolower($key), "note"):
                    $ArrNote[] = $acof->checkinput($_POST[$key]);

                    //if empty, show "*" alert
                    if ($_POST["note" . $_POST[$key]] == '') {
                        $_POST["flagerr" . $_POST[$key]] = $str;
                    } else
                        $_POST["flagerr" . $_POST[$key]] = "";
                    break;

                case stripos($key, "turn"):
                    if ($acof->checkinput($_POST[$key]) == $lang->get_language($_SESSION["username"], 'LabSpeechListen', $_SESSION["language"])) {
                        $ArrTurn[] = '-';
                    } else {
                        $ArrTurn[] = $acof->checkinput($_POST[$key]);
                    }
                    break;

                case stripos($key, "FlagGen"):
                    $ArrGen[] = $_POST[$key];
                    break;
            }
        }

        for ($z = 0; $z < count($ArrIdSub); $z++) {
            //count each obj-error not empty
            if ($_POST["flagerr" . $ArrIdSub[$z]] === $str) {
                $nerr++;
            }

            //it assigns the weight of each item
            if ($ArrRadio[$z] == 'PASS') {
                $weight = $ArrWeight[$z];
                $ArrWeight[$z] = $weight;
            } elseif ($ArrRadio[$z] == 'FAIL') {
                $weight = $ArrWeight[$z];
                $ArrWeight[$z] = (-$weight);
            } else
                $ArrWeight[$z] = 0;
        }

        //it sum the item weight for the total score
        $TotScore = array_sum($ArrWeight);

        //if all obj are compileted then insert them.
        if ($flagerrfile === '' && $flagerrcons === '' && $flagerrsyn === '' && $nerr === 0) {
            //insert in DB
            $lastinsid = $db->insert('tblListen', array(
                'FileName' => $filename,
                'Comment' => $finalnote,
                'SynopsisNew' => $synopsisnote,
                'LenSynopsis' => $_POST["lenxml"],
                'Service' => $_POST["service"],
                'IpAddress' => $_SESSION['ipaddress'],
                'Score' => $TotScore,
                'sysuser' => $_SESSION["username"],
                'startdate' => $_POST["startdate"],
                'enddate' => $_POST["enddate"]
            ));

            //check the last id record
            if ($lastinsid != '') {
                for ($i = 0; $i < count($ArrRadio); $i++) {
                    $idsubitem = $ArrIdSub[$i];
                    $score = $ArrRadio[$i];
                    $note = $ArrNote[$i];
                    $scorevalue = $ArrWeight[$i];
                    $ckgen = $ArrGen[$i];
                    $turn = $ArrTurn[$i];

                    $lastinsidsub = $db->insert('tblListenScore', array(
                        'idlisten' => $lastinsid,
                        'idsubitem' => $idsubitem,
                        'score' => $score,
                        'scorevalue' => $scorevalue,
                        'note' => $note,
                        'turn' => $turn,
                        'FlagGeneral' => $ckgen
                    ));
                    //if 0 then error
                    if ($lastinsidsub === 0)
                        $msgerr = 'e';
                }
            } else
                $msgerr = 'e';

            //insert in Elastic
            $body = array();

            $body['Comment'] = utf8_encode($finalnote);
            $body['EndDate'] = date('Y-m-d\TH:i:s\Z', strtotime($_POST["enddate"]));
            $body['FileName'] = $filename;
            $body['IpAddress'] = $_SESSION['ipaddress'];
            $body['LenSynopsis'] = $_POST["lenxml"];
            $body['Scorelisten'] = $TotScore;
            $body['Service'] = $_POST["service"];
            $body['StartDate'] = date('Y-m-d\TH:i:s\Z', strtotime($_POST["startdate"]));
            $body['Synopsis'] = utf8_encode($synopsisnote);
            $body['SynopsisPredicted'] = utf8_encode($db->getSynopsisPredicted($filename));
            $body['idListen'] = $lastinsid;
            $body['idUser'] = $db->getUserID($_SESSION["username"]);
            $body['sysdate'] = date('Y-m-d\TH:i:s\Z');
            $body['sysdatemod'] = date('Y-m-d\TH:i:s\Z');
            for ($i = 0; $i < count($ArrRadio); $i++) {
                $body['ScoreQuestionValue' . ($i + 1)] = $ArrWeight[$i];
                $body['ScoreQuestion' . ($i + 1)] = $ArrRadio[$i];
                $body['Turn' . ($i + 1)] = utf8_encode($ArrTurn[$i]);
                $body['Note' . ($i + 1)] = utf8_encode($ArrNote[$i]);
                $body['FlagGeneral' . ($i + 1)] = $ArrGen[$i];
            }

            $type = $config->get_ini_value("ELASTIC", "TYPE");

            $return = $elastic->insert($client, json_encode($body), $type, NULL);
            if ($return['created'] != 1)
                $msgerr = 'el ' . $return['created'];

            //if ok
            if ($msgerr == '') {
                $confirm = "<div class='alert alert-success' role='alert'>" . $lang->get_language($_SESSION["username"], 'LabSavedListen', $_SESSION["language"]) . "</div><br>";
                $log->ins_log('Insert Listen OK ' . $return['created'], $_SESSION["username"]);
            } else {
                if ($msgerr == 'el')
                    $log->ins_log('Insert Listen KO - Elastic ' . $msgerr, $_SESSION["username"]);
                else
                    $log->ins_log('Insert Listen KO ' . $return['created'], $_SESSION["username"]);
                $msgerr = "<div class='alert alert-danger' role='alert'>" . $lang->get_language($_SESSION["username"], 'LabErrSaveListen', $_SESSION["language"]) . "</div>";
            }
        } else
            $msgerr = "<div class='alert alert-danger' role='alert'><strong>* " . $lang->get_language($_SESSION["username"], 'LabReqFieldsListen', $_SESSION["language"]) . "</strong></div>";
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <?php echo $acof->head_tag(); ?>
		<script src="js/extrinsic.js"></script>
		<script language="javascript">
            var strSt = "<?php echo $lang->get_language($_SESSION["username"], 'LabSpeechListen', $_SESSION["language"]); ?>";

            //take the value of each checkbox 
            function ConCheckbox(idsubitem) {
                if (document.getElementById('ckGen' + idsubitem).checked == true) {
                    document.getElementById('FlagGen' + idsubitem).value = "Y";
                    document.getElementById('turn' + idsubitem).value = "";
                    document.getElementById('turn' + idsubitem).className = "nodroppable";
                }
                else {
                    document.getElementById('FlagGen' + idsubitem).value = "N";
                    document.getElementById('turn' + idsubitem).className = "droppable";
                    document.getElementById('turn' + idsubitem).value = strSt;
                }
            }
        </script>
        <style>
            .tdcenter{
                text-align: center !important;
            }
            .table-bordered {
                border: 1px solid #ddd;
            }
            label {
                float: left;
                padding: 12px;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <?php $acof->navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']); ?>

        <div class="container">
            <div class="col-md-12 pre-scrollable" style="min-height: 100%;">
                <form action="listen_extrinsic.php" method="post" id="form1" name="form1">
                    <input type="hidden" name="lang" id="lang" value="<?php echo $_POST['lang']; ?>"/>
                    <input type="hidden" name="flagsave" id="flagsave" value="0"/>
                    <input type="hidden" name="startdate" id="startdate"  value="<?php echo $_POST['startdate']; ?>" />
                    <input type="hidden" name="enddate" id="enddate" value="<?php echo $_POST['enddate']; ?>"/>

                    <?php
//show the confirm/alert message
                    if ($msgerr != '') {
                        echo $msgerr;
                    } else {
                        if ($confirm != '') {
                            $btn = $lang->get_language($_SESSION["username"], 'LabRefrBtn', $_SESSION['language']);
                            echo $confirm . "<p align=left><a href='listen.php'><button type='button' class='btn btn-primary'>" . $btn . "</button></a></p>";
                            exit;
                        }
                    }

                    $LabInstrListen = ($lang->get_language($_SESSION["username"], 'LabInstrListen', $_SESSION['language']));
                    $LabInfoListen = ($lang->get_language($_SESSION["username"], 'LabInfoListen', $_SESSION['language']));
                    echo '<br />';

                    $doc = $config->get_ini_value("DOC", "GUIDE");
                    echo $acof->show_info($doc, $LabInstrListen, $LabInfoListen, $case = 'listen');

                    /*                     * *** */

                    $LabServListen = ($lang->get_language($_SESSION["username"], 'LabServListen', $_SESSION['language']));
                    $LabFileListen = ($lang->get_language($_SESSION["username"], 'LabFileListen', $_SESSION['language']));
                    $LabLoadFBtn = ($lang->get_language($_SESSION["username"], 'LabLoadFBtn', $_SESSION['language']));

                    
                        $service = 'DECODA';
                    
                    echo ($annotationform->show_file_select_extr($LabServListen, $LabFileListen, $LabLoadFBtn, $service, $_POST["filtfile"], $_REQUEST['filename'], "insert"));

                    /*                     * *** */
                    if ($_POST["service"] != '')
                        $FlagService = $_POST["service"];
                    else
                        $FlagService = 'LUNA';

                    echo '<br />';
                    //echo ($annotationform->show_questionnaire($_SESSION['language'], $_SESSION['username'], $FlagService, $_POST, $_GET, "insert"));
                    ?>
					<div id="tablequestions">
					</div>

                   
                    
                </form>
            </div>
            <div id="transcription-area" class="col-md-4 pre-scrollable" style=" display:none; min-height: 100%;"></div>	
        </div>	

        <?php echo $acof->foot_page(); ?>
        <script language="javascript">
            $(function () {
                var strSt = "<?php echo $lang->get_language($_SESSION["username"], 'LabSpeechListen', $_SESSION["language"]); ?>";
                $(".droppable").droppable({
                    drop: function (event, ui) {
                        var $this = $(this);

                        if ($this.val() == '' || $this.val() == strSt) {
                            $this.focus().val(ui.draggable.text().replace(/(\r\n|\n|\r)/gm, " ").replace(/ +(?= )/g, ''));
                        }
                        else {
                            $this.focus().val($this.val() + "|" + ui.draggable.text().replace(/(\r\n|\n|\r)/gm, " ").replace(/ +(?= )/g, ''));
                        }
                    }
                });

                $("#synopsisnote").keyup(function () {

                    var lenSys = $(this).val().length;
                    var lenXml = $("#lenxml").val();
                    //7% of the text xml
                    var lenFromPerc = Math.round((lenXml * 7) / 100).toFixed(0);
                    var diff = parseInt(lenFromPerc) - (parseInt(lenFromPerc) - parseInt(lenSys));
                    var str = '<?php echo $lang->get_language($_SESSION["username"], 'LabCharsSyn', $_SESSION["language"]); ?> ' + diff + ' su ' + lenFromPerc + ' [max 1000]';
                    //put the value of number into badge
                    $("#lenSys").text(str);

                    if (lenSys > lenFromPerc)
                        $("#lenSys").css("background-color", "red");
                    else
                        $("#lenSys").css("background-color", "grey");

                }
                );
            });
        </script>	
    </body>
</html>

<?php
$db->close();
?>