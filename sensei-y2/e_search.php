<?php

session_start();

if (!$_SESSION["username"]) header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/lang.php");
require("class/acof.php");
require("class/report.php");
require("class/elastic.php");

$config = Config::get_instance();

$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('E Search Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$report = Report::get_instance();

$elastic = Elastic::get_instance();
$client = $elastic->connect();

if ($_SERVER["REQUEST_METHOD"] == "POST" || $_SESSION['language']==''){
	//load the default language
	$lang -> translate($_POST['lang']);
	
	if ($_POST['flagxls']==1){
		$filename = "report_sensei_".date("Ymd").".xls";
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: inline; filename=$filename");
	}
	
	
	//storage all session variables into post variables
	if (explode("?", basename($_SERVER["HTTP_REFERER"]))[0]=='listen_edit.php' || explode("?", basename($_SERVER["HTTP_REFERER"]))[0]=='listen_view.php' ){
		foreach ($_SESSION as $key => $value) {
			//${$key} = $value;
			$_POST[$key] = $value;						
		}
	} 
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
	<?php echo $acof -> head_tag(); ?>	
	<script language="javascript">
		var strSt = "<?php echo $lang -> get_language($_SESSION["username"],'LabSpeechListen',$_SESSION["language"]);?>"; 

		//take the value of each checkbox 
		function ConCheckbox(idsubitem){
			if (document.getElementById('ckGen'+idsubitem).checked == true){
				document.getElementById('FlagGen'+idsubitem).value = "Y";
				document.getElementById('turn'+idsubitem).value = "";
				document.getElementById('turn'+idsubitem).className = "nodroppable";
			}
			else{
				document.getElementById('FlagGen'+idsubitem).value = "N";
				document.getElementById('turn'+idsubitem).className = "droppable";
				document.getElementById('turn'+idsubitem).value = strSt;
			}
		}
	</script>
    </head>
    
    <body>
	<?php 
            $acof -> navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']);
            
            $ckComment=array();
            $ckSynopsis=array();
            $ckStartTime=array();
            $ckEndTime=array();

            $consideration = $lang -> get_language($_SESSION["username"],'LabConsListen',$_SESSION["language"]);		
            if ($_POST['ckComment']=='on') {
                $ckComment['checked']='checked';
                $ckComment['query']=" ,list.Comment as ".$consideration;
                $ckComment['td']="<td>".$consideration."</td>";
            }

            if ($_POST['ckSynopsis']=='on') {
                $ckSynopsis['checked']='checked';
                $ckSynopsis['query']=" ,list.SynopsisNew";
                $ckSynopsis['td']="<td>Synopsis</td>";
            }

            if ($_POST['ckStartTime']=='on') {
                $ckStartTime['checked']='checked';
                $ckStartTime['query']=" ,DATE_FORMAT(list.StartDate, '%d/%m/%Y %H:%i')StartDate";
                $ckStartTime['td']="<td>Monit.&nbsp;Start&nbsp;Date&nbsp;&nbsp;&nbsp;</td>";
            }

            if ($_POST['ckEndTime']=='on') {
                $ckEndTime['checked']='checked';
                $ckEndTime['query']=" ,DATE_FORMAT(list.StartDate, '%d/%m/%Y %H:%i')StartDate";
                $ckEndTime['td']="<td>Monit.&nbsp;End&nbsp;Date&nbsp;&nbsp;&nbsp;</td>";
            }
        ?>
        
        <div class="container">
            <form action="e_search.php" method="post" id="form1" name="form1">
                <input type="hidden" name="lang" id="lang" value="<?php echo $_POST['lang'];?>"/>
                <div class="col-md-9">
                    <br/>
                    <?php 
                        $LabInstrReport = $lang -> get_language($_SESSION["username"],'LabInstrReport',$_SESSION["language"]);
                        echo $acof -> show_info('', $LabInstrReport, '', $case='e_search');

                        echo $report -> show_e_search_form($ckComment, $ckSynopsis, $ckStartTime, $ckEndTime, $_POST, $_SESSION);
                        echo '<br />';				

                        //if ($_SERVER["REQUEST_METHOD"] == "POST") echo $report -> show_result ($ckComment, $ckSynopsis, $ckStartTime, $ckEndTime, $_POST, $_SESSION);
                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                            foreach ($_POST as $k => $v) {
                                echo $k.' = '.$v.'<br/>';
                            }
                        }
                        
                        /*
                            datesel = tbllistensysdate oppure tbllistensysdatemod
                            typedate = <sysdate> oppure <sysdatemod>
                            sysuser = idUser
                            service = Service
                            subitem = idSubItem
                            scoresubitem = tbllistenscoreScore
                            txtFile = FileName
                            note = Note
                            comment = Comment
                            synopsisnew = SynopsisNew
                        
                         *  $dataset = $elastic->search($client, $bodysearch, $type, $size);
                            $result2 = $dataset['hits']['hits'];
                             $count = 1;
                            foreach ($result2 as $item) {
                                //$elastic->delete($client,$item['_id'], "line");
                                echo $count++."       ".$item['_id']."    ".$item['_source']['speaker']."         ".$item['_source']['text_entry']."<br>";
                            }
                        */
                        
                        //prepare filter
                        $i=0;
                        if ($_POST['service']!="") $filter['bool']['must'][$i++]['term']['Service']=$_POST['service'];
                        if ($_POST['sysuser']!="") $filter['bool']['must'][$i++]['term']['idUser']=$_POST['sysuser'];
                        
                        if ($_POST['subitem']!="") {
                            $subitem=explode("|",$_POST['subitem']);
                            $filter['bool']['must'][$i++]['term']['idSubItem']=$subitem[0];
                            
                            if ($_POST['scoresubitem']!="") $filter['bool']['must'][$i++]['term']['tbllistenscoreScore']=$_POST['scoresubitem'];
                        }
                        
                        if ($_POST['txtFile']!="") $filter['bool']['must'][$i++]['term']['FileName']=$_POST['txtFile'];
                        
                        //prepare query
                        $i=0;
                        if ($_POST['note']!="") $query['bool']['must'][$i++]['match']['Note']=$_POST['note'];
                        if ($_POST['comment']!="") $query['bool']['must'][$i++]['match']['Comment']=$_POST['comment'];
                        if ($_POST['synopsisnew']!="") $query['bool']['must'][$i++]['match']['SynopsisNew']=$_POST['synopsisnew'];
                                                
                        $bodysearch['query']['filtered']= array ("filter" => $filter, "query" => $query);                                            
                        $type = $config -> get_ini_value("ELASTIC", "TYPE");
                        
                        //call search method
                        $dataset = $elastic->search($client, $bodysearch, $type);
                        $result = $dataset['hits']['hits'];
                        echo $dataset['hits']['total'];
                        //foreach ($result as $i => $v) echo $i.' = '.$v.'<br />';
                    ?>			
                </div>
                <input type="hidden" id="flagxls" name="flagxls" value="0"/>		
            </form>
	</div>
        
        <?php echo $acof -> foot_page('report'); ?>
	<script language="javascript">
		function View(idListen){ //show the monitoring
                    //window.open
		}
		
		 $(function(){
                    //date menu
                    $('#datesel').daterangepicker({		
                        posX: 175,
                        posY: 185
                    }); 		
		 });
	</script>
  </body>
</html>

<?php
    $db->close();
?>