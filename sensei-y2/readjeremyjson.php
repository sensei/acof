<?php

$base_url = './json/';
$filename = $_REQUEST['filename'];
$full_path = $base_url . "jeremy.json";


$obj = json_decode(file_get_contents($full_path));
echo(findtranscription($filename, $obj));

function findtranscription($string, $array) {
    foreach ($array as $key) {
        $searching = $key->transcriptionfilename;
        if ($searching == $string) {
            return json_encode($key);
        }
    }
    return NULL;
}
