<?php

session_start();
require("class/config.php");
require("class/db.php");
$config = Config::get_instance();
$db = Database::get_instance();
$db->connect();


$service = $_REQUEST['service'];
$condition = $_REQUEST['condition'];
$scenario = $_REQUEST['scenario'];
$conversations = $_REQUEST['conversations'];
$task = $_REQUEST['task'];
$singleconversationarray = explode(";", $conversations);
$toreturn = "OK";
foreach ($singleconversationarray as $value) {
    $conv = explode("|", $value);
    $lastinsid = $db->insert('extrinsic_evaluation_item', array(
        'value' => $conv[2],
        'idextrinsic_question' => $conv[0],
        'idextrinsic_conversation' => $conv[1],
        'service' => $service,
        'condition' => $condition,
        'scenario' => $scenario,
        'task' => $task,
        'sysuser' => $_SESSION["username"],
        'idextrinsic_evaluation' => $_SESSION['idevaluation']
    ));
    if ($lastinsid < 1) {
        $toreturn = "KO";
    }
}

//Verifico che l'inserimento sia andato a buon fine e elimino l'idconversation dall'array
if ($toreturn == "OK") {
    if (isset($_SESSION['idsconversation'])) {
        $ids = $_SESSION['idsconversation'];
    }
    if (count($ids) > 0) {
        $currentid = array_pop($ids)['idextrinsic_conversation'];
        $_SESSION['idsconversation'] = $ids;
    }
}
echo $toreturn;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

