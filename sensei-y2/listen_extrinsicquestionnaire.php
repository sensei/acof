<?php
session_start();

error_reporting(0);
ini_set("display_errors", 0);

if (!$_SESSION["username"])
    header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/annotationform.php");

//implementing ElasticSearch
require("class/elastic.php");

$config = Config::get_instance();

$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('Listen Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$annotationform = Annotationform::get_instance();

$elastic = Elastic::get_instance();
$client = $elastic->connect();

$msgerr = "";
$nerr = 0;
$str = "*";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $lang->translate($_POST['lang']);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <style>
            .note {
                color: #514f4f;
                font-size: 11px;
            }
        </style>
        <?php echo $acof->head_tag(); ?>
        <script src="js/extrinsic.js"></script>
        <script language="javascript">
            var strSt = "<?php echo $lang->get_language($_SESSION["username"], 'LabSpeechListen', $_SESSION["language"]); ?>";

            //take the value of each checkbox 
            function ConCheckbox(idsubitem) {
                if (document.getElementById('ckGen' + idsubitem).checked == true) {
                    document.getElementById('FlagGen' + idsubitem).value = "Y";
                    document.getElementById('turn' + idsubitem).value = "";
                    document.getElementById('turn' + idsubitem).className = "nodroppable";
                } else {
                    document.getElementById('FlagGen' + idsubitem).value = "N";
                    document.getElementById('turn' + idsubitem).className = "droppable";
                    document.getElementById('turn' + idsubitem).value = strSt;
                }
            }
        </script>
        <style>
            .tdcenter{
                text-align: center !important;
            }
            .table-bordered {
                border: 1px solid #ddd;
            }
            label {
                float: left;
                padding: 12px;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <?php $acof->navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']); ?>

        <div class="container">
            <div class="col-md-12 pre-scrollable" style="min-height: 100%;">
                <form action="listen_extrinsicquestionnaire.php" method="post" id="form1" name="form1">
                    <input type="hidden" name="lang" id="lang" value="<?php echo $_POST['lang']; ?>"/>

                    <div id="userquestionnaire">
                        <h3><?php echo $lang->get_language($_SESSION["username"], 'LabexpquestTitle', $_SESSION["language"]) ?></h3>
                        <table id="tablequestionnaire" class="table-bordered" width="100%">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <table border="0" width="100%">
                                        <tr>
                                            <td class="tdcenter">
                                                1
                                            </td>
                                            <td class="tdcenter">
                                                2
                                            </td>
                                            <td class="tdcenter">
                                                3
                                            </td>
                                            <td class="tdcenter">
                                                4
                                            </td>
                                            <td class="tdcenter">
                                                5
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="100" width="80%">
                                    <?php echo $lang->get_language($_SESSION["username"], 'LabExpquest1', $_SESSION["language"]) ?>
                                </td>
                                <td class="tdcenter">
                                    <table border="0" width="100%">
                                        <tr>
                                            <td class="tdcenter">
                                                <input id="exp1" name="exp1" type="radio" value="1"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp1" name="exp1" type="radio" value="2"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp1" name="exp1" type="radio" value="3"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp1" name="exp1" type="radio" value="4"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp1" name="exp1" type="radio" value="5"></input>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="100" width="80%">
                                    <?php echo $lang->get_language($_SESSION["username"], 'LabExpquest2', $_SESSION["language"]) ?>
                                </td>
                                <td class="tdcenter">
                                    <table border="0" width="100%">
                                        <tr>
                                            <td class="tdcenter">
                                                <input id="exp2" name="exp2" type="radio" value="1"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp2" name="exp2" type="radio" value="2"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp2" name="exp2" type="radio" value="3"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp2" name="exp2" type="radio" value="4"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp2" name="exp2" type="radio" value="5"></input>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <table border="0" width="100%">
                                        <tr>
                                            <td width="33%" class="tdcenter">
                                                C1
                                            </td>
                                            <td  width="33%"class="tdcenter">
                                                C2
                                            </td>
                                            <td width="33%" class="tdcenter">
                                                <?php echo $lang->get_language($_SESSION["username"], 'LabExpquestNone', $_SESSION["language"]) ?>
                                            </td>
                                           
                                        </tr>
                                    </table>
                                </td>
                            </tr>


                            <tr>
                                <td height="100" width="80%">
                                    <?php echo $lang->get_language($_SESSION["username"], 'LabExpquest3', $_SESSION["language"]) ?>
                                </td>
                                <td class="tdcenter">
                                    <table border="0" width="100%">
                                        <tr>
                                            <td class="tdcenter">
                                                <input id="exp3" name="exp3" type="radio" value="1"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp3" name="exp3" type="radio" value="2"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp3" name="exp3" type="radio" value="3"></input>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td height="100" width="80%">
                                    <?php echo $lang->get_language($_SESSION["username"], 'LabExpquest4', $_SESSION["language"]) ?>
                                </td>
                                <td class="tdcenter">
                                    <table border="0" width="100%">
                                        <tr>
                                            <td class="tdcenter">
                                                <input id="exp4" name="exp4" type="radio" value="1"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp4" name="exp4" type="radio" value="2"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp4" name="exp4" type="radio" value="3"></input>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="100" width="80%">
                                    <?php echo $lang->get_language($_SESSION["username"], 'LabExpquest5', $_SESSION["language"]) ?>
                                </td>
                                <td class="tdcenter">
                                    <table border="0" width="100%">
                                        <tr>
                                            <td class="tdcenter">
                                                <input id="exp5" name="exp5" type="radio" value="1"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp5" name="exp5" type="radio" value="2"></input>
                                            </td>
                                            <td class="tdcenter">
                                                <input id="exp5" name="exp5" type="radio" value="3"></input>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td height="100" width="80%">
                                    <?php echo $lang->get_language($_SESSION["username"], 'LabExpquest6', $_SESSION["language"]) ?>
                                </td>
                                <td class="tdcenter">
                                    <table border="0" width="100%">
                                        <tr>
                                            <td class="tdcenter">
                                                <textarea class="3"  id="exp6" name="exp6" cols="50" rows="4"></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="100" width="80%">
                                    <?php echo $lang->get_language($_SESSION["username"], 'LabExpquest7', $_SESSION["language"]) ?>
                                </td>
                                <td class="tdcenter">
                                    <table border="0" width="100%">
                                        <tr>
                                            <td class="tdcenter">
                                                <textarea class="3"  id="exp7" name="exp7" cols="50" rows="4"></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>


                        </table>
                        <p align="center" style="padding-top:30px;">
                            <button onclick="SaveUserExperience()" class="btn btn-lg btn-success" style="" id="" type="button">Save</button>
                        </p>
                    </div>



                </form>
            </div>
            <div id="transcription-area" class="col-md-4 pre-scrollable" style=" display:none; min-height: 100%;"></div>	
        </div>	

        <?php echo $acof->foot_page(); ?>
        <script language="javascript">
            $(function () {
                var strSt = "<?php echo $lang->get_language($_SESSION["username"], 'LabSpeechListen', $_SESSION["language"]); ?>";
                $(".droppable").droppable({
                    drop: function (event, ui) {
                        var $this = $(this);

                        if ($this.val() == '' || $this.val() == strSt) {
                            $this.focus().val(ui.draggable.text().replace(/(\r\n|\n|\r)/gm, " ").replace(/ +(?= )/g, ''));
                        } else {
                            $this.focus().val($this.val() + "|" + ui.draggable.text().replace(/(\r\n|\n|\r)/gm, " ").replace(/ +(?= )/g, ''));
                        }
                    }
                });

                $("#synopsisnote").keyup(function () {

                    var lenSys = $(this).val().length;
                    var lenXml = $("#lenxml").val();
                    //7% of the text xml
                    var lenFromPerc = Math.round((lenXml * 7) / 100).toFixed(0);
                    var diff = parseInt(lenFromPerc) - (parseInt(lenFromPerc) - parseInt(lenSys));
                    var str = '<?php echo $lang->get_language($_SESSION["username"], 'LabCharsSyn', $_SESSION["language"]); ?> ' + diff + ' su ' + lenFromPerc + ' [max 1000]';
                    //put the value of number into badge
                    $("#lenSys").text(str);

                    if (lenSys > lenFromPerc)
                        $("#lenSys").css("background-color", "red");
                    else
                        $("#lenSys").css("background-color", "grey");

                }
                );
            });
        </script>	
    </body>
</html>

<?php
$db->close();
?>