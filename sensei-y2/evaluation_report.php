<?php

session_start();
require("class/config.php");
require("class/db.php");
$config = Config::get_instance();
$sql="SELECT
  extrinsic_evaluation.idextrinsic_evaluation,
  extrinsic_evaluation.service,
  extrinsic_evaluation.scenario,
  extrinsic_evaluation.`condition`,
  extrinsic_evaluation.task,
  extrinsic_evaluation.sysuser,
  extrinsic_evaluation.compilation_time,
  extrinsic_evaluation.sysdate,
  extrinsic_conversation.filename as conversation,
  extrinsic_question.question,
  extrinsic_evaluation_item.value
FROM extrinsic_evaluation_item
  INNER JOIN extrinsic_evaluation
    ON extrinsic_evaluation_item.idextrinsic_evaluation = extrinsic_evaluation.idextrinsic_evaluation
  INNER JOIN extrinsic_conversation
    ON extrinsic_evaluation_item.idextrinsic_conversation = extrinsic_conversation.idextrinsic_conversation
  inner join extrinsic_question on extrinsic_evaluation_item.idextrinsic_question=extrinsic_question.idextrinsic_question";
$db = Database::get_instance();
$db->connect();
$out=$db->fetch_array($sql);
echo "{\"data\":" .json_encode($out). "}";