<?php

//ini_set('display_errors','On');
//error_reporting(E_ALL & ~E_NOTICE);

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/annotationform.php");
require("class/elastic.php");

$config = Config::get_instance();

$db = Database::get_instance();
$db->connect();

$lang = Lang::get_instance();
$annotationform = Annotationform::get_instance();

echo '<!DOCTYPE html><html lang="en"><head></head><body>';
echo '<div>';

switch ($_REQUEST['action']) {
    case 'saveFlgGeneral':
        $ret = $db->update('tblListenScore', array(
            'FlagGeneral' => 'Y',
            'Turn' => '-',
            'sysdatemod' => date("Y-m-d H:i:s")), 'idScore = \'' . $_REQUEST['id'] . '\'');
        break;

    case 'saveTurn':
        $ret = $db->update('tblListenScore', array(
            'FlagGeneral' => 'N',
            'Turn' => htmlentities($_REQUEST['value']),
            'sysdatemod' => date("Y-m-d H:i:s")), 'idScore = \'' . $_REQUEST['id'] . '\'');
        break;

    case 'loadTranscription':
        $service = $_REQUEST['service'];

        $transcription_path = $config->get_ini_value("PATH", "PATH_TRS_" . $service);
        $templates_path = $config->get_ini_value("PATH", "PATH_TEMPLATE");
        $audio_path = $config->get_ini_value("PATH", "PATH_AUDIO_" . $service);
        $audio_webpath = $config->get_ini_value("PATH", "WEBPATH_AUDIO_" . $service);

        $path = $_REQUEST['file']; //"file" is the value posted by default.js
        //if file selected
        if ($path != '') {

            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $filename = basename($path, $ext);

            $filenameaudio = $filename . "wav";
            $filenametrs = $filename . "trs";

            $filenameaudio = str_replace('Ritest_', '', $filenameaudio);
            $filenametrs = str_replace('Ritest_', '', $filenametrs);

            //if exist the audio of selected file 
            if (file_exists($audio_path . DIRECTORY_SEPARATOR . $filenameaudio)) {
                //if exist the trs of selected file 
                if (file_exists($transcription_path . DIRECTORY_SEPARATOR . $filenametrs)) {
                    echo '<div id="content">';

                    //check the filename into the database
                    $query = "SELECT idListen FROM tblListen WHERE FileName = '" . $path . "' AND sysuser = '" . $_SESSION["username"] . "'";
                    $arrReturn = array();
                    $arrReturn = $db->fetch_array($query);

                    if (count($arrReturn) > 0)
                        echo '<div class="alert alert-danger" role="alert"><strong>' . $lang->get_language($_SESSION["username"], 'LabCheckFListen', $_SESSION['language']) . '</strong></div>';
                    //show the play controls and transcription
                    echo '<audio class="audio_transcription" controls>
							<source src="' . $audio_webpath . DIRECTORY_SEPARATOR . $filenameaudio . '" type="audio/wav" />
							Your browser does not support the audio element.
						</audio>';

                    //extract the trs text
                    echo 'nuovo script';
                    echo $annotationform->process_xsl(file_get_contents($transcription_path . DIRECTORY_SEPARATOR . $filenametrs), $templates_path . DIRECTORY_SEPARATOR . $ext . '.xsl');

                    echo '</div >';
                } else
                    echo '<div id="content"><div class="alert alert-danger" role="alert"><strong>' . $lang->get_language($_SESSION["username"], 'LabNoFileTrsListen', $_SESSION['language']) . '</div></div>';
            } else
                echo '<div id="content"><div class="alert alert-danger" role="alert"><strong>' . $lang->get_language($_SESSION["username"], 'LabNoFileListen', $_SESSION['language']) . '</div></div>';
        } else
            echo '<div id="content"><div class="alert alert-danger" role="alert"><strong>' . $lang->get_language($_SESSION["username"], 'LabEmptyFListen', $_SESSION['language']) . '</div></div>';
        break;
}

echo '</div>';
echo '</body></html>';

$db->close();
?>