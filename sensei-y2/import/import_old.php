<?php
error_reporting(E_ALL);ini_set("display_errors", 1);
//require '../class/elastic.php';

require("../class/config.php");
$config = Config::get_instance();
require '../class/elastic.php';
$elastic = Elastic::get_instance();
$client = $elastic->connect();



$json = file_get_contents('http://cassandra.disi.unitn.it/sensei-new/import/TP_NET_luna.json');
$obj = json_decode($json);

$importazione = array();
foreach ($obj as $key) {
    $body = array();
    $body['id'] = $key->id;
    $body['name'] = $key->name;
    $body['type'] = $key->type;
    $body['ACOF_Synopsis'] = $key->ACOF_Synopsis;
    $body['start'] = $key->start;
    $body['end'] = $key->end;
    $body['features'] = array();
    for ($i = 0; $i < count($key->features); $i++) {
        $body['features'][$i]['question_id'] = $key->features[$i]->question_id;
        $body['features'][$i]['annotations']['annotator_id'] = $key->features[$i]->annotations->annotator_id;
        $body['features'][$i]['annotations']['value'] = $key->features[$i]->annotations->value;
        $body['features'][$i]['annotations']['note'] = $key->features[$i]->annotations->note;
        $body['features'][$i]['annotations']['turn_id'] = array();
        for ($j = 0; $j < count($key->features[$i]->annotations->turn_id); $j++) {
            $body['features'][$i]['annotations']['turn_id'][$j] = $key->features[$i]->annotations->turn_id[$j];
        }
    }
    $importazione[]=$body;
    unset($body);
}
$log=array();
foreach ($importazione as $corpo) {
    
   $log[]= insert2("acof", $client, $corpo, "luna", NULL);
    
}
echo "pippo";

function insert2($index, $client, $body, $type, $id) {
    $params = array();
    if ($id != NULL) {
        $params['id'] = $id;
    }
    $params['index'] = $index;
    $params['type'] = $type;
    $params['body'] = $body;
    $ret = $client->index($params);
    return $ret;
}
