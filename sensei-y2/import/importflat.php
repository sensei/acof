<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
require '../lib/vendor/autoload.php';

$parame = array();
$parame['hosts'] = array(
    'http://es_admin:UrJ3veuL@localhost:9200' // IP + Port
);
$client = new Elasticsearch\Client($parame);

$json = file_get_contents('http://cassandra.disi.unitn.it/sensei-y2/import/TP_NET_flatcassandra_new.json');

$obj = json_decode($json);
//$errore=json_last_error_msg();
//echo $errore;
$importazione = array();


//echo "pippo";
foreach ($obj as $key) {
    $body = array();
    $body['idListen'] = $key->idListen;
        $body['Conversation_Name'] = $key->Conversation_Name;
        $body['Comment'] = $key->Comment;
        $body['Synopsis'] = $key->Synopsis;
        $body['Synopsis_Predicted'] = $key->Synopsis_Predicted;
        $body['LenSynopsis'] = $key->LenSynopsis;
        $body['Service'] = $key->Service;
        $body['Scorelisten'] = $key->Scorelisten;
        $body['IpAddress'] = $key->IpAddress;
        $body['Annotator'] = $key->Annotator;
        $body['sysdate'] = date('Y-m-d\TH:i:s\Z', strtotime($key->sysdate));
        $body['sysdatemod'] = date('Y-m-d\TH:i:s\Z', strtotime($key->sysdatemod));
        $body['StartDate'] = date('Y-m-d\TH:i:s\Z', strtotime($key->StartDate));
        $body['EndDate'] = date('Y-m-d\TH:i:s\Z', strtotime($key->EndDate));
        $body['ScoreQuestion1'] = $key->ScoreQuestion1;
        $body['ScoreQuestionValue1'] = $key->ScoreQuestionValue1;
        $body['Note1'] = $key->Note1;
        $body['Turn1'] = $key->Turn1;
        $body['FlagGeneral1'] = $key->FlagGeneral1;

        $body['ScoreQuestion2'] = $key->ScoreQuestion2;
        $body['ScoreQuestionValue2'] = $key->ScoreQuestionValue2;
        $body['Note2'] = $key->Note2;
        $body['Turn2'] = $key->Turn2;
        $body['FlagGeneral2'] = $key->FlagGeneral2;

        $body['ScoreQuestion3'] = $key->ScoreQuestion3;
        $body['ScoreQuestionValue3'] = $key->ScoreQuestionValue3;
        $body['Note3'] = $key->Note3;
        $body['Turn3'] = $key->Turn3;
        $body['FlagGeneral3'] = $key->FlagGeneral3;

        $body['ScoreQuestion4'] = $key->ScoreQuestion4;
        $body['ScoreQuestionValue4'] = $key->ScoreQuestionValue4;
        $body['Note4'] = $key->Note4;
        $body['Turn4'] = $key->Turn4;
        $body['FlagGeneral4'] = $key->FlagGeneral4;

        $body['ScoreQuestion5'] = $key->ScoreQuestion5;
        $body['ScoreQuestionValue5'] = $key->ScoreQuestionValue5;
        $body['Note5'] = $key->Note5;
        $body['Turn5'] = $key->Turn5;
        $body['FlagGeneral5'] = $key->FlagGeneral5;

        $body['ScoreQuestion6'] = $key->ScoreQuestion6;
        $body['ScoreQuestionValue6'] = $key->ScoreQuestionValue6;
        $body['Note6'] = $key->Note6;
        $body['Turn6'] = $key->Turn6;
        $body['FlagGeneral6'] = $key->FlagGeneral6;

        $body['ScoreQuestion7'] = $key->ScoreQuestion7;
        $body['ScoreQuestionValue7'] = $key->ScoreQuestionValue7;
        $body['Note7'] = $key->Note7;
        $body['Turn7'] = $key->Turn7;
        $body['FlagGeneral7'] = $key->FlagGeneral7;

        $body['ScoreQuestion8'] = $key->ScoreQuestion8;
        $body['ScoreQuestionValue8'] = $key->ScoreQuestionValue8;
        $body['Note8'] = $key->Note8;
        $body['Turn8'] = $key->Turn8;
        $body['FlagGeneral8'] = $key->FlagGeneral8;

        $body['ScoreQuestion9'] = $key->ScoreQuestion9;
        $body['ScoreQuestionValue9'] = $key->ScoreQuestionValue9;
        $body['Note9'] = $key->Note9;
        $body['Turn9'] = $key->Turn9;
        $body['FlagGeneral9'] = $key->FlagGeneral9;

        $body['ScoreQuestion10'] = $key->ScoreQuestion10;
        $body['ScoreQuestionValue10'] = $key->ScoreQuestionValue10;
        $body['Note10'] = $key->Note10;
        $body['Turn10'] = $key->Turn10;
        $body['FlagGeneral10'] = $key->FlagGeneral10;

        $body['ScoreQuestion11'] = $key->ScoreQuestion11;
        $body['ScoreQuestionValue11'] = $key->ScoreQuestionValue11;
        $body['Note11'] = $key->Note11;
        $body['Turn11'] = $key->Turn11;
        $body['FlagGeneral11'] = $key->FlagGeneral11;

        $body['ScoreQuestion12'] = $key->ScoreQuestion12;
        $body['ScoreQuestionValue12'] = $key->ScoreQuestionValue12;
        $body['Note12'] = $key->Note12;
        $body['Turn12'] = $key->Turn12;
        $body['FlagGeneral12'] = $key->FlagGeneral12;
    $importazione[] = $body;
    unset($body);
}
$log = array();
$count=0;
foreach ($importazione as $corpo) {


    $log[] = insert2("flatacof", $client, $corpo, "acof", NULL);
    $count++;
    ob_flush();
    echo $count."<br>";
    flush();
}
file_put_contents('log.json', json_encode($log));

function insert2($index, $client, $body, $type, $id) {
    $params = array();
    if ($id != NULL) {
        $params['id'] = $id;
    }
    $params['index'] = $index;
    $params['type'] = $type;
    $params['body'] = $body;
    $ret = $client->index($params);
    return $ret;
}
