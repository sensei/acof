<?php

session_start();
require("class/config.php");
require("class/db.php");
$config = Config::get_instance();
$db = Database::get_instance();
$db->connect();
$json = file_get_contents('./extrinsicimport/DECODA/output.asr.json');

$obj = json_decode($json);
foreach ($obj as $value) {
    $synopsis = $value->features->synopsis;
    $filename = $value->id;
    $ret = $db->update('extrinsic_conversation', array('synopsis' => utf8_decode($synopsis)), 'filename = \'' . $filename . '\'');

    ob_flush();
    echo $ret . " : " . $filename . ": " . utf8_decode($synopsis). "<br>";
    flush();
}