<?php
session_start();

//ini_set('display_errors','On');
//error_reporting(E_ALL);

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/authentication.php");

$_SESSION["username"] = '';
$_SESSION['ipaddress'] = getenv('HTTP_CLIENT_IP')?:
						getenv('HTTP_X_FORWARDED_FOR')?:
						getenv('HTTP_X_FORWARDED')?:
						getenv('HTTP_FORWARDED_FOR')?:
						getenv('HTTP_FORWARDED')?:
						getenv('REMOTE_ADDR');

$config = Config::get_instance();

$db = Database::get_instance();
$db -> connect();

$acof = Acof::get_instance();
$auth = Authentication::get_instance();

if ($_SERVER["REQUEST_METHOD"] == "POST"){
	if ($auth->check_login($_POST['user'],$_POST['pass'])) header("Location: home.php");
	else $msg = "<div class='alert alert-danger' role='alert'><strong>Error! </strong>Login failed. Wrong input data.</div>";
}
else $msg = '';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
		<?php echo $acof -> head_tag('index'); ?>	
	</head>

	<body>
		<div class="container">
			<?php $auth->show_login($msg); ?>
		</div> <!-- /container -->
		
	</body>
</html>

<?php
$db -> close();
?>