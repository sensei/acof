<?php
session_start();

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if (!$_SESSION["username"])
    header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/annotationform.php");
require("class/extrinsic.php");

//implementing ElasticSearch
require("class/elastic.php");

$config = Config::get_instance();
$extrinsic = extrinsic::get_instance();
$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('Listen Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$annotationform = Annotationform::get_instance();
unset($_SESSION['idsconversation']);


$msgerr = "";
$nerr = 0;
$str = "*";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $lang->translate($_POST['lang']);

    //if postback is true by save button
   
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <?php echo $acof->head_tag(); ?>
        <script src="js/extrinsic.js"></script>
        <script language="javascript">
            var strSt = "<?php echo $lang->get_language($_SESSION["username"], 'LabSpeechListen', $_SESSION["language"]); ?>";
            //take the value of each checkbox 
            function ConCheckbox(idsubitem) {
                if (document.getElementById('ckGen' + idsubitem).checked == true) {
                    document.getElementById('FlagGen' + idsubitem).value = "Y";
                    document.getElementById('turn' + idsubitem).value = "";
                    document.getElementById('turn' + idsubitem).className = "nodroppable";
                } else {
                    document.getElementById('FlagGen' + idsubitem).value = "N";
                    document.getElementById('turn' + idsubitem).className = "droppable";
                    document.getElementById('turn' + idsubitem).value = strSt;
                }
            }
        </script>
        
    </head>
    <body>
        <?php $acof->navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']); ?>
        <div class="container">
            <div class="col-md-12 pre-scrollable" style="min-height: 100%;">
                <form action="" method="post" id="form1" name="form1">
                    <input type="hidden" name="lang" id="lang" value="<?php echo $_SESSION['language']; ?>"/>
                    <input type="hidden" name="flagsave" id="flagsave" value="0"/>
                    <input type="hidden" name="startdate" id="startdate"  value="<?php echo $_POST['startdate']; ?>" />
                    <input type="hidden" name="enddate" id="enddate" value="<?php echo $_POST['enddate']; ?>"/>
                    <?php
//show the confirm/alert message
                    if ($msgerr != '') {
                        echo $msgerr;
                    } else {
                        if ($confirm != '') {
                            $btn = $lang->get_language($_SESSION["username"], 'LabRefrBtn', $_SESSION['language']);
                            echo $confirm . "<p align=left><a href='listen.php'><button type='button' class='btn btn-primary'>" . $btn . "</button></a></p>";
                            exit;
                        }
                    }
                    $LabInstrListen = ($lang->get_language($_SESSION["username"], 'LabInstrListen', $_SESSION['language']));
                    $LabInfoListen = ($lang->get_language($_SESSION["username"], 'LabInfoListen', $_SESSION['language']));
                    echo '<br />';
                    $doc = $config->get_ini_value("DOC", "GUIDE");
                    //echo $acof->show_info($doc, $LabInstrListen, $LabInfoListen, $case = 'listen');
                    /*                     * *** */
                    $LabServListen = ($lang->get_language($_SESSION["username"], 'LabServListen', $_SESSION['language']));
                    $LabFileListen = ($lang->get_language($_SESSION["username"], 'LabFileListen', $_SESSION['language']));
                    $LabLoadFBtn = ($lang->get_language($_SESSION["username"], 'LabLoadFBtn', $_SESSION['language']));
                    $service = 'LUNA';
                    echo ($annotationform->show_file_select_extr($LabServListen, $LabFileListen, $LabLoadFBtn, $service, $_POST["filtfile"], $_REQUEST['filename'], "insert"));
                    /*                     * *** */
                    if ($_POST["service"] != '')
                        $FlagService = $_POST["service"];
                    else
                        $FlagService = 'DECODA';

                    echo '<br />';
                    //echo ($annotationform->show_questionnaire($_SESSION['language'], $_SESSION['username'], $FlagService, $_POST, $_GET, "insert"));
                    //echo "pippo";
                    echo "</form>";
                    $service = "";
                    $scenario = "";
                    $collection = "1";
                    $condition = "1";
                    if (isset($_REQUEST['service']) and isset($_REQUEST['scenario']) and isset($_REQUEST['collection'])and isset($_REQUEST['condition'])) {
                        $service = $_REQUEST['service'];
                        $collection = $_REQUEST['collection'];
                        $scenario = $_REQUEST['scenario'];
                        $condition = $_REQUEST['condition'];
                        echo $extrinsic->show_extr_eval_questionnaire($collection, $service, $scenario, null);
                    }
                    ?>
            </div>
           
        </div>	
        <?php echo $acof->foot_page(); ?>
        <script language="javascript">
            $(function () {
                var strSt = "<?php echo $lang->get_language($_SESSION["username"], 'LabSpeechListen', $_SESSION["language"]); ?>";
                $(".droppable").droppable({
                    drop: function (event, ui) {
                        var $this = $(this);

                        if ($this.val() == '' || $this.val() == strSt) {
                            $this.focus().val(ui.draggable.text().replace(/(\r\n|\n|\r)/gm, " ").replace(/ +(?= )/g, ''));
                        } else {
                            $this.focus().val($this.val() + "|" + ui.draggable.text().replace(/(\r\n|\n|\r)/gm, " ").replace(/ +(?= )/g, ''));
                        }
                    }
                });

                $("#synopsisnote").keyup(function () {
                    var lenSys = $(this).val().length;
                    var lenXml = $("#lenxml").val();
                    //7% of the text xml
                    var lenFromPerc = Math.round((lenXml * 7) / 100).toFixed(0);
                    var diff = parseInt(lenFromPerc) - (parseInt(lenFromPerc) - parseInt(lenSys));
                    var str = '<?php echo $lang->get_language($_SESSION["username"], 'LabCharsSyn', $_SESSION["language"]); ?> ' + diff + ' su ' + lenFromPerc + ' [max 1000]';
                    //put the value of number into badge
                    $("#lenSys").text(str);

                    if (lenSys > lenFromPerc)
                        $("#lenSys").css("background-color", "red");
                    else
                        $("#lenSys").css("background-color", "grey");
                }
                );
            });
        </script>	
    </body>
</html>

<?php
$db->close();
?>