function ChangeLan(language) {
    document.form1.lang.value = language;
    document.form1.submit();
}

function Save() {
    if (ValidateForm(document.form1) == true) {
        getTime('e');
        document.form1.flagsave.value = "1";
        document.form1.submit();
    }
    else {

        return false;
    }
}

function ValidateForm(form) {
    var count =$("[id^=turn]").length;
    $('td').removeAttr("style");
    var toReturn = true;
    for (var i = 1; i <= count; i++) {
        var radiobutton = 'input[name=radio' + i + ']';
        var turn = '#turn' + i;
        var general = '#ckGen' + i;
        if (!($(radiobutton).is(':checked'))) {
            toReturn = false;
            $(radiobutton).parent().css("background-color", "red");
        }
        if (!($(general).prop("checked"))) {
            if ($(turn).val() == "Intervento Vocale Effettivo" || $(turn).val() == "Effective Speech Turn" || $(turn).val() == "Voix d'intervention efficace") {
                $(turn).parent().css("background-color", "red");
                $(general).parent().css("background-color", "red");
                toReturn = false;
            }
        }
    }
    if (($('#finalnote').val() == "")) {
        $('#finalnote').parent().css("background-color", "red");
        toReturn = false;
    }

    if ($('#synopsisnote').val() == "") {
        $('#synopsisnote').parent().css("background-color", "red");
        toReturn = false;

    }
    if (!toReturn) {
        $('.col-md-8').animate({ scrollTop: 0 }, 0);
    }
    return toReturn;
}

function getTime(type) {
    var currentTime = new Date();
    var day = currentTime.getDate();
    var month = currentTime.getMonth() + 1;
    var year = currentTime.getFullYear();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }

    //start date
    if (type == 's') {
        document.form1.startdate.value = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
    }
    else { //end date
        document.form1.enddate.value = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
    }
}

function printExcel() {
    $.post('printexcel.php', function (result) {
        newpage = result;
        window.open('printexcel.php', 'popUpWindow', 'height=1, width=1');
    });
}

function loadTranscription(type, fileselected, curr_url, action) {
    $.ajax({
        type: 'POST',
        //dataType: 'json',
        //url: 'readjeremyjson.php',
        url: 'search_synopsis.php',
        data: {
            filename: fileselected
        },
        success: function (responseData) {
            if (responseData.length != 0 & action != null) {
                var predicted_synopsis = responseData;
                $('.synopsis-tooltip').css('display', 'block');
                $('.synopsis-tooltip').html("<h4>Synopsis predicted</h4>" + predicted_synopsis);
                if (action == 'insert')
                    $('#synopsisnote').val(predicted_synopsis);
            } else {
                $('.synopsis-tooltip').css('display', 'none');
                $('.synopsis-tooltip').text("");
                $('#synopsisnote').val("");
            }
        }
    });

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'predict_questions.php',
        data: {
            filename: fileselected
        },
        success: function (responseData) {
            if (responseData != null & action == 'insert') {
                for (var i = 0; i < responseData.length; i++) {
                    var question_id = responseData[i].question_id;
                    var predicted_label = responseData[i].predicted_label;
                    var label;
                    switch (predicted_label) {
                        case "PASS":
                            label = "PASS";
                            break;
                        case "NOPASS":
                            label = "FAIL";
                            break;
                        default:
                            label = "NA";
                    }
                    $('#radio' + question_id + '[value=' + label + ']').click();
                }
            }
        }
    });

    $.ajax(
            {
                type: "POST",
                url: "ajax.php",
                dataType: 'html',
                data: {
                    action: 'loadTranscription',
                    file: fileselected, //take the value of ID object "filename"
                    service: type //LUNA O DECODA				
                },
                success: function (content) {
                    var check;
                    check = content.indexOf('audio');

                    //spli the posted text
                    var data = content;
                    var arr = data.split('|');
                    var len = arr[1];

                    //put the length into input obj
                    $("#lenxml").val(len);

                    //put the result of loadTranscription function
                    $('#transcription-area').html(arr[0] + arr[2]);

                    //if it's ok to evaluation
                    if (check > '-1') {
                        //$('#filename').html('');

                        $(".turn").draggable({
                            cursor: 'move', // sets the cursor apperance
                            opacity: 0.75, // opacity fo the element while it's dragged
                            appendTo: "body",
                            zIndex: 10000,
                            containment: 'body',
                            scroll: false,
                            helper: function () {
                                return $(this).clone();
                            }
                        });
                    }
                    else
                    {
                        //clear the filename obj
                        document.form1.filename.value = '';
                    }

                }
            }
    )
}

function checkShowColumn(element, checkID) {
    var elementID = $(element).attr('id');
    var valore = $('#' + elementID).val();

    if (valore.length > 1) {

        $('#' + checkID).attr('checked', 'true');

    } else {
        $('#' + checkID).removeAttr('checked');
    }

}

function checkShowSubItem(element) {


    var elementID = $(element).attr('id');
    var valore = $('#' + elementID).val();
    if (valore != "") {


        $('#showsubitem\\[' + elementID.replace('ScoreQuestion', '') + '\\]').attr('checked', 'true');
    }
    else
    {
        $('#showsubitem\\[' + elementID.replace('ScoreQuestion', '') + '\\]').removeAttr('checked');
    }



}

function resetEsForm(form) {

    $("form#" + form + " :input").each(function () {
        var input = $(this);
        input.val("");// This is the jquery object of the input, do what you will
    });
    $("form#" + form + "  :checkbox").each(function () {
        var input = $(this);
        input.removeAttr('checked');// This is the jquery object of the input, do what you will
    });

    $("form#" + form + "  :select").each(function () {
        var input = $(this);
        input.removeAttr('selected');// This is the jquery object of the input, do what you will
    });
}

function goBack() {
    document.form1.action = "report.php";
    document.form1.submit();
}

function goEdit(id, type) {
    document.form1.action = "listen_" + type + "_prefilled.php?id=" + id;
    document.form1.submit();
}

function changeSubItem(valuearr, checkcount) {
    var value;
    value = valuearr.split("|");

    var i;
    for (i = 1; i <= checkcount; i++) {
        document.getElementById("comparewith[" + i + "]").checked = false;
    }

    if (value[0] < 0) {
        for (i = 1; i <= checkcount; i++) {
            document.getElementById("comparewith[" + i + "]").checked = true;
        }
    }
    else
        document.getElementById("comparewith[" + value[0] + "]").checked = true;
}