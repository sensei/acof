$(function () {


});

function loadEvaluation(service) {
    startCounter();
    var scenario = $('#scenario').val();
    var condition = $('#condition').val();
    $('#extrinsictableconv').empty().load('html/' + service + 's' + scenario + 'c' + condition + '.html');
}

function savecollectionevaluation() {
    stopCounter();
    collectionarray = "";


    var compilationtime = $('#endtime').val() - $('#starttime').val();

    for (var i = 1; i < 100; i++) {
        if ($("textarea[name='" + i + "']").val() != "" && document.getElementById("coll|" + i) != null) {
            collectionarray += i + "|" + $("textarea[name='" + i + "']").val() + ";";
        }
    }

    var collections = collectionarray.slice(0, -1);

    var service = $("[name='service']").val();
    var condition = $("[name='condition']").val();
    var task = $("[name='task']").val();
    var scenario = $("[name='scenario']").val();
    $.ajax({
        type: "POST",
        url: "savecollectioneval.php",
        dataType: 'html',
        data: {
            compilationtime: compilationtime,
            service: service,
            scenario: scenario,
            condition: condition,
            task: task,
            collections: collections
        },
        success: function (content) {
            if (content == "OK") {
                alert("Evaluation saved!");
                window.open('listen_extrinsic.php', '_self');
                return true;

            } else {
                alert("Error during evaluation saving. Please retry!");
                return false;
            }
        }
    });

}

function SaveUserExperience() {
    var compiled = true;

    for (var i = 1; i <= 5; i++) {
        var label = "exp" + i;

        if ($("input:radio[name=" + label + "]:checked").val() != null)
        {
        } else {
            compiled = false;
        }
    }
    var exp6val = $('textarea#exp6').val();
    var exp7val = $('textarea#exp7').val();
    if (exp6val.trim() == "" || exp7val.trim() == "") {
        compiled = false;
    }

    if (compiled == true) {
        var experiencearray = "";

        for (var i = 1; i <= 5; i++) {
            if ($("input[name='exp" + i + "']:checked").val() != "" && document.getElementById("exp" + i) != null) {
                experiencearray += i + "|" + $("input[name='exp" + i + "']:checked").val() + ";";
            }
        }
        experiencearray +="6"+ "|" + $('textarea#exp6').val() + ";" +"7"+ "|" + $('textarea#exp7').val();


        var experience = experiencearray;
        $.ajax({
            type: "POST",
            url: "saveuserexperience.php",
            dataType: 'html',
            data: {
                experience: experience
               
            },
            success: function (content) {
                if (content == 'OK') {
                    alert("Questionnaire correctly saved!");

                    window.open('listen_extrinsic.php', '_self');
                    return true;

                } else {
                    alert("Error during evaluation saving. Please retry!");
                    return false;
                }
            }
        });


    } else
    {
        alert("Please, answer all questions!");
    }
}


function loadnextconv(conv, quest) {
    var inserted = 0;

    if ($(':radio:checked').length == $(':radio').length / 5) {
        inserted = 1;
    } else {
        inserted = 0;

    }
    if (inserted == 1) {

        var conversationarray = "";
        for (var j = 1; j < conv; j++) {
            for (var i = 1; i <= quest; i++) {
                if ($("input[name='conv" + i + "|" + j + "']:checked").val() != null && document.getElementById('conv' + i + '|' + j) != null) {
                    conversationarray += i + "|" + j + "|" + $("input[name='conv" + i + "|" + j + "']:checked").val() + ";";
                }
            }
        }
        var conversations = conversationarray.slice(0, -1);

        var service = $("[name='service']").val();
        var condition = $("[name='condition']").val();
        var task = $("[name='task']").val();
        var scenario = $("[name='scenario']").val();

        $.ajax({
            type: "POST",
            url: "savequestion.php",
            dataType: 'html',
            data: {
                service: service,
                scenario: scenario,
                condition: condition,
                task: task,
                conversations: conversations
            },
            success: function (content) {
                if (content == 'OK') {

                    $('#task1save').submit();
                    return true;

                } else {
                    alert("Error during evaluation saving. Please retry!");
                    return false;
                }
            }
        });

    } else {
        alert("Please answer all questions!");
        return false;
    }
}
function loadExtrEvaluation() {
    if ($('#task').val() == 1) {
        $('#form1').attr('action', 'task1.php');
    } else
    {
        $('#form1').attr('action', 'task2.php');
    }

    $('#form1').submit();
}

function startCounter() {
    $('#starttime').val(Math.round(jQuery.now() / 1000));
}

function stopCounter() {
    $('#endtime').val(Math.round(jQuery.now() / 1000));
}

function getCounterTime() {
    var start = parseInt($('#starttime').val());
    var end = parseInt($('#endtime').val());
    return end - start;
}

function SaveEvaluation() {
    stopCounter();
    alert("Tempo di compilazione del form: " + getCounterTime() + " secondi");
}

function SaveExtrEvaluation() {
    if (checkEmptyValues()) {
        var collections = "";
        for (var i = 1; i < 13; i++) {
            if ($("textarea[name='coll" + i + "|" + 0 + "']").val() != "" && document.getElementById("coll" + i + "|" + 0) != null) {
                collections += i + "|" + 0 + "|" + $("textarea[name='coll" + i + "|" + 0 + "']").val() + ";";
            }
        }
        $('#postcollections').val((collections.slice(0, -1)));
        var conversations = "";
        for (var j = 1; j < 3; j++) {
            for (var i = 1; i < 11; i++) {
                if ($("input[name='conv" + i + "|" + j + "']:checked").val() != null && document.getElementById('conv' + i + '|' + j) != null) {
                    conversations += i + "|" + j + "|" + $("input[name='conv" + i + "|" + j + "']:checked").val() + ";";
                }
            }
        }
        $('#postconversations').val((conversations.slice(0, -1)));
        stopCounter();
        alert("Salvataggio effettuato. Tempo di compilazione del form: " + getCounterTime() + " secondi");
        window.location.replace("listen_extrinsicluna.php");
    } else
        alert("Inserisci tutti i valori");
}

function checkEmptyValues() {
    var toreturn = true;
    if ($(':radio:checked').length != $(':radio').length / 5) {
        toreturn = false;
    }
    $('textarea').each(function () {
        if ($(this).val() == "") {
            toreturn = false;
        }
    });
    return toreturn;
}

function loadtask2results(service, condition, searchvalues, topic, polarity, caller_polarity, politness, conversation_temper, acof, agent_polarity) {
    $('#results').empty().html(' <img src="./img/loader.gif" />');
    $.ajax({
        type: "POST",
        url: "searchtask2.php",
        dataType: 'html',
        data: {
            service: service,
            agent_polarity: agent_polarity,
            condition: condition,
            searchtext: searchvalues,
            topic: topic,
            polarity: polarity,
            caller_polarity: caller_polarity,
            politness: politness,
            conversation_temper: conversation_temper,
            acof: acof
        },
        success: function (content) {

            $('#results').empty().html(content);

        }
    });

}


function searchtask2conv(service, condition) {
    var text = $('#searchfield').val();
    var topic = $('#topic').val();
    var polarity = $('#polarity').val();
    var caller_polarity = $('#caller_polarity').val();
    var agent_polarity = $('#agent_polarity').val();
    var politness = $('#politness').val();
    var conversation_temper = $('#conversation_temper').val();
    var acof = $('#acof').val();
    loadtask2results(service, condition, text, topic, polarity, caller_polarity, politness, conversation_temper, acof, agent_polarity);
}
function loadExtrTrans(transcription, audiofile) {
    $('#conversation').empty().html(' <img src="./img/loader.gif" />');
    $.ajax({
        type: "POST",
        url: "http://cassandra.disi.unitn.it/sensei-y2/conversation/audiotranscription.php",
        dataType: 'html',
        data: {
            transcription: transcription,
            audiofile: audiofile, //take the value of ID object "filename"
        },
        success: function (content) {
            $('#transcriptionarea').empty().html(content);
            $('#conversation').empty().html(content);

        }
    });
}