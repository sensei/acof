<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require("./class/config.php");
require("./class/db.php");

$config = Config::get_instance();
$db = Database::get_instance();
$db->connect();
$json = file_get_contents('./extrinsicimport/DECODA/output.asr.json');

$obj = json_decode($json);
//$errore = json_last_error_msg();
//echo $errore;
$importazione = array();

foreach ($obj as $value) {
    $item = array();
    $item['filename'] = $value->id;
    $item['topic_true'] = $value->topic_true;
    $item['synopsis'] = $value->features->synopsis;
    $item['criteria'] = $value->criteria;
    $item['templates'] = $value->features->templates;
    $importazione[] = $item;
}
foreach ($importazione as $elem) {
    $lastinsid = $db->insert('extrinsic_conversation', array(
        'filename' => $elem['filename'],
        'topic_true' => $elem['topic_true'],
        'service' => 'DECODA',
        'synopsis' => utf8_decode($elem['synopsis'])
    ));
    if ($lastinsid != '') {
        ob_flush();
        echo "Inserted row: " . $lastinsid . "<br>";
        flush();
        $criteria = array();
        $criteria['id'] = $lastinsid;
        $criteria['filename'] = $elem['filename'];
        $criteria['polarity'] = $elem['criteria']->polarity;
        $criteria['conversation_temper'] = $elem['criteria']->conversation_temper;
        $criteria['topic_pred'] = $elem['criteria']->topic_pred;
        $criteria['politness'] = $elem['criteria']->politness;
        $criteria['acof'] = $elem['criteria']->acof;
        $criteria['caller_polarity'] = $elem['criteria']->caller_polarity;
        $criteria['caller_polarity_percent'] = $elem['criteria']->caller_polarity_percent;
        $criteria['politness_percent'] = $elem['criteria']->politness_percent;
        $criteria['polarity_percent'] = $elem['criteria']->polarity_percent;

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'conversation_temper',
            'value' => $criteria['conversation_temper'],
            'type' => 'criteria'
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'topic_pred',
            'value' => $criteria['topic_pred'],
            'type' => 'criteria'
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'politness',
            'value' => $criteria['politness'],
            'type' => 'criteria'
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'caller_polarity',
            'value' => $criteria['caller_polarity'],
            'type' => 'criteria'
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'polarity',
            'value' => $criteria['polarity'],
            'type' => 'criteria'
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'acof',
            'value' => $criteria['acof'],
            'type' => 'criteria'
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'caller_polarity_percent',
            'value' => $criteria['caller_polarity_percent'],
            'type' => 'criteria'
        ));
        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'politness_percent',
            'value' => $criteria['politness_percent'],
            'type' => 'criteria'
        ));
        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'polarity_percent',
            'value' => $criteria['polarity_percent'],
            'type' => 'criteria'
        ));

        if (count($item['templates']) > 0) {
            foreach ($item['templates'] as $templateitem) {
                $db->insert('extrinsic_template', array(
                    'idextrinsic_conversation' => $criteria['id'],
                    'filename' => $criteria['filename'],
                    'field_0' => $templateitem['0'],
                    'field_1' => $templateitem['1'],
                    'field_2' => $templateitem['2']
                ));
            }
        }
    } else {
        ob_flush();
        echo mysql_errno($db) . ": " . mysql_error($db) . "<br>";
        flush();
    }
}
importtranscription($db);

var_dump($importazione);

function importtranscription($db) {

    $out = $db->get_all_conversations();

    foreach ($out as $value) {
        if ($value['service'] == 'DECODA') {
            $filename = searchtranscription($value['filename'] . ".trs");
            if (file_exists($filename)) {
                $lastinsid = $db->update('extrinsic_conversation', array(
                    'transcription' => file_get_contents($filename)
                        ), "filename='" . $value['filename'] . "'");
                if ($lastinsid != '0') {
                    ob_flush();
                    echo $lastinsid . " " . $value['filename'] . " " . "<br>";
                    flush();
                } else {
                    ob_flush();
                    echo mysql_errno($db) . ": " . mysql_error($db) . "<br>";
                    flush();
                }
            }
        }
    }
    $sql = " select idextrinsic_conversation,filename,transcription from extrinsic_conversation ";
    $out = $db->fetch_array($sql);
    foreach ($out as $value) {
        $filename = $value['filename'];
        $xml = simplexml_load_string($value['transcription']);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        $html = "";
        foreach ($xml->Episode->Section as $article) {
            foreach ($article->Turn as $turn) {
                if (trim($turn) != "") {
                    $html.= trim($turn) . " ";
                }
            }
        }
        $ret = $db->update('extrinsic_conversation', array(
            'transcription_text' => $html), 'filename = \'' . $filename . '\'');
    }
}

function searchtranscription($filename) {

    $percorso = './trs/';
    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($percorso));
    $files = iterator_to_array($iterator, true);
// iterate over the directory
// add each file found to the archive
    foreach ($files as $key => $value) {
        try {
            if ($value->getFilename() != '.' && $value->getFilename() != '..') {
                if (trim($value->getFilename()) == trim($filename)) {
                    return $value->getRealPath();
                }
            }
        } catch (Exception $e) {
            echo "ERROR: Could not add the file '$key': $e\n";
        }
    }
    return "0";
}
