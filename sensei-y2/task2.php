<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if (!$_SESSION["username"])
    header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/annotationform.php");
require("class/extrinsic.php");

//implementing ElasticSearch
require("class/elastic.php");

$config = Config::get_instance();
$extrinsic = extrinsic::get_instance();
$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('Listen Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$annotationform = Annotationform::get_instance();

//Get post values
$service = $_POST['service'];
$scenario = $_POST['scenario'];
$task = $_POST['task'];

$condition = $_POST['condition'];
$questions = $db->get_extrinsic_questionnaire($service, $scenario);
$questionstask1 = find_items($questions, "task", "2");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $lang->translate($_POST['lang']);

    //if postback is true by save button
}

$topic = array('ETFC' => ($lang->get_language($_SESSION["username"], 'ETFC', $_SESSION['language'])),
    'ITNR' => ($lang->get_language($_SESSION["username"], 'ITNR', $_SESSION['language'])),
    'OBJT' => ($lang->get_language($_SESSION["username"], 'OBJT', $_SESSION['language'])),
    'NVGO' => ($lang->get_language($_SESSION["username"], 'NVGO', $_SESSION['language'])),
    'HORR' => 'Schedules',
    'AAPL' => ($lang->get_language($_SESSION["username"], 'AAPL', $_SESSION['language'])),
    'PV' => ($lang->get_language($_SESSION["username"], 'PV', $_SESSION['language'])),
    //  'NULL' => 'None',
    'VGC' => ($lang->get_language($_SESSION["username"], 'VGC', $_SESSION['language'])),
    // 'TARF' => 'Fares',
    // 'OFTP' => 'Transportation offers',
    'RETT' => ($lang->get_language($_SESSION["username"], 'RETT', $_SESSION['language'])),
    'CPAG' => ($lang->get_language($_SESSION["username"], 'CPAG', $_SESSION['language'])),
    // 'ACDT' => 'Accident',
    // 'JSTF' => 'Justification',
    'SVDO' => ($lang->get_language($_SESSION["username"], 'SVDO', $_SESSION['language'])),
        //  'RGLM' => 'Payment',
        // 'DDOC' => 'Query for documents',
        // 'AESP' => 'Passenger bahviour',
        // 'ACCS' => 'Accessibility',
        //'SRTP' => 'Website / mobile'
);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <?php echo $acof->head_tag(); ?>
        <style>
            .Hot{color:red;}
			.more{color:green;}
			.less{color:red;}
            .Medium {color:orange;}
            .Cold{color:blue;}
            .PASS{color:green;}
            .FAIL{color:red;}
			.neutral {
    color: orange;
}
        </style>
        <script src="js/extrinsic.js"></script>
        <script language="javascript">
            var strSt = "<?php echo $lang->get_language($_SESSION["username"], 'LabSpeechListen', $_SESSION["language"]); ?>";
            //take the value of each checkbox 
            function ConCheckbox(idsubitem) {
                if (document.getElementById('ckGen' + idsubitem).checked == true) {
                    document.getElementById('FlagGen' + idsubitem).value = "Y";
                    document.getElementById('turn' + idsubitem).value = "";
                    document.getElementById('turn' + idsubitem).className = "nodroppable";
                } else {
                    document.getElementById('FlagGen' + idsubitem).value = "N";
                    document.getElementById('turn' + idsubitem).className = "droppable";
                    document.getElementById('turn' + idsubitem).value = strSt;
                }
            }
        </script>

        <style>
            h1 {
                font-size: 120%;
            }
            .block-1 {
                margin: 5px;
                padding: 15px;
                background: #eeeeee;
            }
            .block-2 {
                margin: 5px;
                padding: 15px;
                background: #eeeeee;
                display: inline-block;
                width: 94%;
            }

            .columns {
                column-count: 2;
                -moz-column-count: 2;
                -webkit-column-count: 2;
            }
            #player {
                margin: 5px;
                text-align: center;
            }
            .agent {
                text-align: right;
            }

            .agent div {
                border-radius: 10px;
                background: skyblue;
                padding: 10px;
                display: inline-block;
                text-align: right;
            }
            .caller div {
                border-radius: 10px;
                background: yellowgreen;
                padding: 10px;
                display: inline-block;
            }
            .hot {
                color: red;
            }
            .negative { color: red; }
           
            .polite { color: green; }
			.positive { color: green; }
            #search {
                text-align: center;
            }
            #criteria li {
                list-style-type: none;
                display: inline-block;
                padding-right: 1em;
            }
        </style>
        <script>
            $(function () {
                startCounter();
            });
        </script>
    </head>
    <body>
        <?php $acof->navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']); ?>
        <div class="container">


            <div class="col-md-8 pre-scrollable" style="min-height: 100%;">
                <form action="task2.php" method="post" id ="task1save" name="form1">
                    <?php
                    foreach ($_REQUEST as $key => $item) {
                        $convpost = strpos($key, "conv", 0);
                        if ($convpost === FALSE && $key != "lang") {
                            echo '<input type="hidden" name="' . $key . '" value="' . $item . '">';
                        }
                    }
                    ?>
                    <input type="hidden" name="lang" id="lang" value="<?php echo $_POST['lang']; ?>"/>
                </form>
                <div class="block-1" id="instructions">
                    <h1><?php echo $lang->get_language($_SESSION["username"], 'LabIstrValut', $_SESSION["language"]) ?></h1>
                    <?php
                    echo $service . " ";
                    $scenariolabel = "";
                    if ($service == "DECODA") {
                        $scenariolabel = $scenario == '1' ? "Scenario 1: " . $lang->get_language($_SESSION["username"], 'LabAgainAfterD', $_SESSION["language"]) : "Scenario 2 : " . $lang->get_language($_SESSION["username"], 'LabBallHideD', $_SESSION["language"]);
                    } else {
                        $scenariolabel = $scenario == '1' ? "Scenario 1: " . $lang->get_language($_SESSION["username"], 'LabSidePartL', $_SESSION["language"]) : "Scenario 2 : " . $lang->get_language($_SESSION["username"], 'LabWhiteBlackL', $_SESSION["language"]);
                    }

                    echo $scenariolabel . "<br>";

                    echo $lang->get_language($_SESSION["username"], 'LabTask', $_SESSION['language']) . " 2:" . $lang->get_language($_SESSION["username"], 'LabMouseYellowD', $_SESSION["language"]) . "<br>";
                    ?>
                    <?php
                    echo "C" . $condition;
                    ?>

                </div>
                <div class="block-1" id="evaluation">

                    <div id="questions">
                        <table width="100%">
<?php
foreach ($questionstask1 as $question) {
    echo '<tr>';
    echo '<td>' . $lang->get_language($_SESSION["username"], $question['label'], $_SESSION["language"]) . '</td>';


    echo '<td><textarea rows="4" cols="50" id="coll|' . $question['idextrinsic_question'] . '" name="' . $question['idextrinsic_question'] . '" class="' . $question['idextrinsic_question'] . '"> </textarea> </td>';
    echo '</tr>';
}
?>
                            <tr> 
                                <td colspan="2" >
<?php
echo '<input type="hidden" name="starttime" id="starttime" value="">';
echo '<input type="hidden" name="endtime" id="endtime" value="">';
?>
                                    <p align="center"> <br> <br><button onclick="savecollectionevaluation(); return false;"><?php echo $lang->get_language($_SESSION["username"], 'LabFinishBtn', $_SESSION['language']) ?></button></p>
                                                </td>
                                                </tr>
                                                </table>

                                                </p>
                                                </div>
                                                </div>
                                                <div class="block-1" id="search">
                                                    <p>
                                                        <?php echo $lang->get_language($_SESSION["username"], 'LabSearchEngine', $_SESSION['language']); ?>: <input name="searchfield"  id="searchfield" type="text" size="100" /> <button id="searchtask2" onclick="searchtask2conv(<?php echo "'" . $service . "'" . "," . $condition ?>);"><?php echo $lang->get_language($_SESSION["username"], 'LabSearchBtn', $_SESSION['language']) ?></button>
                                                    </p>
<?php
if ($condition == '2') {
    if ($service == 'DECODA') {
        echo $lang->get_language($_SESSION["username"], 'LabFilterBy', $_SESSION['language']) . ": ";
        echo $lang->get_language($_SESSION["username"], 'topic_pred', $_SESSION['language']) . ":  <select id=\"topic\"  name=\"topic\" >
                                                                                                    ";
        echo "<option value=\"\"></option>";
        foreach ($topic as $key => $item) {
            echo "<option value=\"" . $key . "\">" . $item . "</option>";
        }

        echo "</select>";
        echo $lang->get_language($_SESSION["username"], 'polarity', $_SESSION['language']) . ": <select id=\"polarity\"  name=\"polarity\">
                                                                                                          <option value=\"\"></option> 
                                                                                                           <option value=\"positive\">" . $lang->get_language($_SESSION["username"], 'Positive', $_SESSION['language']) . "</option> 
                                                                                                            <option value=\"negative\">" . $lang->get_language($_SESSION["username"], 'Negative', $_SESSION['language']) . "</option> 
                                                                                                            </select>
                                                                                                    ";
        echo $lang->get_language($_SESSION["username"], 'caller_polarity', $_SESSION['language']) . ": <select id=\"caller_polarity\"  name=\"caller_polarity\">
                                                                                                          <option value=\"\"></option> 
                                                                                                           <option value=\"positive\">" . $lang->get_language($_SESSION["username"], 'Positive', $_SESSION['language']) . "</option> 
                                                                                                            <option value=\"negative\">" . $lang->get_language($_SESSION["username"], 'Negative', $_SESSION['language']) . "</option> 
                                                                                                            </select>
                                                                                                    ";
        echo "<br>" . $lang->get_language($_SESSION["username"], 'politness', $_SESSION['language']) . " <select id=\"politness\"  name=\"politness\">
                                                                                                          <option value=\"\"></option> 
                                                                                                           <option value=\"More\">" . $lang->get_language($_SESSION["username"], 'More', $_SESSION['language']) . "</option> 
                                                                                                            <option value=\"Less\">" . $lang->get_language($_SESSION["username"], 'Less', $_SESSION['language']) . "</option> 
                                                                                                            </select>
                                                                                                    ";
        echo $lang->get_language($_SESSION["username"], 'conversation_temper', $_SESSION['language']) . " <select id=\"conversation_temper\"  name=\"conversation_temper\">
                                                                                                          <option value=\"\"></option> 
                                                                                                           <option value=\"Hot\">" . $lang->get_language($_SESSION["username"], 'Hot', $_SESSION['language']) . "</option> 
                                                                                                             <option value=\"Medium\">" . $lang->get_language($_SESSION["username"], 'Medium', $_SESSION['language']) . "</option>
                                                                                                              <option value=\"Cold\">" . $lang->get_language($_SESSION["username"], 'Cold', $_SESSION['language']) . "</option>
                                                                                                            </select>
                                                                                                    ";
        echo $lang->get_language($_SESSION["username"], 'acof', $_SESSION['language']) . ": <select id=\"acof\"  name=\"acof\">
                                                                                                          <option value=\"\"></option> 
                                                                                                           <option value=\"PASS\">".$lang->get_language($_SESSION["username"], 'PASS', $_SESSION['language'])."</option> 
                                                                                                             <option value=\"FAIL\">".$lang->get_language($_SESSION["username"], 'FAIL', $_SESSION['language'])."</option>
                                                                                                             </select>
                                                                                                    ";

        echo "</p>";
    } else {
        echo $lang->get_language($_SESSION["username"], 'LabFilterBy', $_SESSION['language']) . ": ";

        echo $lang->get_language($_SESSION["username"], 'polarity', $_SESSION['language']) . ": <select id=\"polarity\"  name=\"polarity\">
                                                                                                          <option value=\"\"></option> 
                                                                                                           <option value=\"positive\">Positive</option> 
                                                                                                           <option value=\"neutral\">Neutral</option> 
                                                                                                            <option value=\"negative\">Negative</option> 
                                                                                                            </select>
                                                                                                    ";
        echo "Caller polarity <select id=\"caller_polarity\"  name=\"caller_polarity\">
                                                                                                          <option value=\"\"></option> 
                                                                                                           <option value=\"positive\">Positive</option> 
                                                                                                           <option value=\"neutral\">Neutral</option> 
                                                                                                            <option value=\"negative\">Negative</option> 
                                                                                                            </select>
                                                                                                    ";
        echo "<br>Agent polarity <select id=\"agent_polarity\"  name=\"agent_polarity\">
                                                                                                           <option value=\"\"></option> 
                                                                                                           <option value=\"positive\">Positive</option> 
                                                                                                           <option value=\"neutral\">Neutral</option> 
                                                                                                            <option value=\"negative\">Negative</option> 
                                                                                                            </select>
                                                                                                    ";


        echo "</p>";
    }
}
?>
                                                </div>
                                                <div id="results">





                                                </div>


                                                </div>
                                                <div class = "col-md-4 pre-scrollable" style = "min-height: 100%;" >
                                                    <div class = "block-2" id = "conversation" >
                                                    </div>

                                                </div>
                                                </div>
                                                </div>
                                                </body>
                                                </html>
<?php

function find_items($array, $findwhat, $value, $found = array()) {
    foreach ($array as $k => $v) {
        if (is_array($v)) {
            $result = find_items($v, $findwhat, $value, $found);
            if ($result === true) {
                $found[] = $v;
            } else {
                $found = $result;
            }
        } else {
            if ($k == $findwhat && $v == $value) {
                return TRUE;
            }
        }
    }
    return $found;
}
?>