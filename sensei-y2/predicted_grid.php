<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
$base_url = 'json/';
$files = scandir($base_url);
$toReturn = array();
$count=0;
foreach ($files as $file) {
    if ($file != '.' && $file != '..') {
        $full_path = $base_url . $file;
        $json = file_get_contents($full_path);
        $obj = json_decode($json);
        $toReturn[$count]['filename'] = $obj[0]->name . ".trs";
        $toReturn[$count]['question_id0'] = $obj[0]->question_id;
        $toReturn[$count]['question_description0'] = $obj[0]->question_en_description;
        $toReturn[$count]['predicted_label0'] = $obj[0]->predicted_label;
        $toReturn[$count]['question_id1'] = $obj[1]->question_id;
        $toReturn[$count]['question_description1'] = $obj[1]->question_en_description;
        $toReturn[$count]['predicted_label1'] = $obj[1]->predicted_label;
        $toReturn[$count]['question_id2'] = $obj[2]->question_id;
        $toReturn[$count]['question_description2'] = $obj[2]->question_en_description;
        $toReturn[$count]['predicted_label2'] = $obj[2]->predicted_label;
        $count++;
      
    }
}
echo "{\"data\":" .json_encode($toReturn). "}";



//$firstNames = array("Andrew", "Nancy", "Shelley", "Regina", "Yoshi", "Antoni", "Mayumi", "Ian","Peter", "Lars", "Petra", "Martin", "Sven", "Elio", "Beate", "Cheryl", "Michael", "Guylene");
//
//	$lastNames = array("Fuller", "Davolio", "Burke", "Murphy", "Nagase", "Saavedra", "Ohno", "Devling","Wilson", "Peterson", "Winkler", "Bein", "Petersen", "Rossi", "Vileid", "Saylor", "Bjorn", "Nodier");
//
//	$productNames = array("Black Tea", "Green Tea", "Caffe Espresso", "Doubleshot Espresso", "Caffe Latte", "White Chocolate Mocha", "Cramel Latte", "Caffe Americano", "Cappuccino", "Espresso Truffle", "Espressocon Panna", "Peppermint Mocha Twist", "Black Tea", "Green Tea", "Caffe Espresso", "Doubleshot Espresso", "Caffe Latte", "White Chocolate Mocha");
//
//	$priceValues = array("2.25", "1.5", "3.0", "3.3", "4.5", "3.6", "3.8", "2.5", "5.0","1.75","3.25","4.0", "2.25", "1.5", "3.0", "3.3", "4.5", "3.6");
//
//	$data = array();
//	$i=0;
//	while($i < count($productNames))
//	{    
//	  $row = array();
//	  $productindex = $i;
//	  $price = $priceValues[$productindex];
//	  $quantity = rand(1, 10);
//	  $row["firstname"] = $firstNames[$i];
//	  $row["lastname"] = $lastNames[$i];
//	  $row["productname"] = $productNames[$productindex];
//	  $row["price"] = $price;
//	  $row["quantity"] = $quantity;
//	  $row["total"] = $price * $quantity;
//
//	  $data[$i] = $row;
//	  $i++;
//	}
//	 
//	header("Content-type: application/json"); 
//	echo "{\"data\":" .json_encode($data). "}";
?>