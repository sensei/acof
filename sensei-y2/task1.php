<?php
session_start();

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if (!$_SESSION["username"])
    header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/annotationform.php");
require("class/extrinsic.php");

//implementing ElasticSearch
require("class/elastic.php");

$config = Config::get_instance();
$extrinsic = extrinsic::get_instance();
$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('Listen Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$annotationform = Annotationform::get_instance();
$starttime = 0;
//Get post values
$service = $_POST['service'];
$scenario = $_POST['scenario'];
$task = $_POST['task'];
$condition = $_POST['condition'];
//Se è la prima conversazione parte il timer
$referer = end(split('/', $_SERVER['HTTP_REFERER']));
$sqlmaxconvid = "SELECT max(idextrinsic_conversation) as maxval FROM extrinsic_conversation";
$maxconvid = $db->fetch_array($sqlmaxconvid)[0]['maxval'];
$sqlmaxquestid = "SELECT max( idextrinsic_question ) AS maxval FROM extrinsic_question";
$maxquestid = $db->fetch_array($sqlmaxquestid)[0]['maxval'];
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $lang->translate($_POST['lang']);

    //if postback is true by save button
}
if ($referer == 'task1.php') {
    $starttime = $_SESSION['starttime'];
} else {
    $starttime = time();
    $_SESSION['starttime'] = $starttime;
    $lastinsid = $db->insert('extrinsic_evaluation', array(
        'service' => $service,
        'condition' => $condition,
        'scenario' => $scenario,
        'task' => $task,
        'sysuser' => $_SESSION["username"]
    ));
    $_SESSION['idevaluation'] = $lastinsid;
}

$topic = array('ETFC' => ($lang->get_language($_SESSION["username"], 'ETFC', $_SESSION['language'])),
    'ITNR' => ($lang->get_language($_SESSION["username"], 'ITNR', $_SESSION['language'])),
    'OBJT' => ($lang->get_language($_SESSION["username"], 'OBJT', $_SESSION['language'])),
    'NVGO' => ($lang->get_language($_SESSION["username"], 'NVGO', $_SESSION['language'])),
     'HORR' => 'Schedules',
    'AAPL' => ($lang->get_language($_SESSION["username"], 'AAPL', $_SESSION['language'])),
    'PV' => ($lang->get_language($_SESSION["username"], 'PV', $_SESSION['language'])),
    //  'NULL' => 'None',
    'VGC' => ($lang->get_language($_SESSION["username"], 'VGC', $_SESSION['language'])),
    // 'TARF' => 'Fares',
    // 'OFTP' => 'Transportation offers',
    'RETT' => ($lang->get_language($_SESSION["username"], 'RETT', $_SESSION['language'])),
    'CPAG' => ($lang->get_language($_SESSION["username"], 'CPAG', $_SESSION['language'])),
    // 'ACDT' => 'Accident',
    // 'JSTF' => 'Justification',
    'SVDO' => ($lang->get_language($_SESSION["username"], 'SVDO', $_SESSION['language'])),
        //  'RGLM' => 'Payment',
        // 'DDOC' => 'Query for documents',
        // 'AESP' => 'Passenger bahviour',
        // 'ACCS' => 'Accessibility',
        //'SRTP' => 'Website / mobile'
);

if (!isset($_SESSION['idsconversation'])) {
    $_SESSION['idsconversation'] = $db->getidsconversationtask1($service, $scenario);
    $ids = $_SESSION['idsconversation'];
} else {
    $ids = $_SESSION['idsconversation'];
}
if (count($ids) > 0) {
    $currentid = end($ids)['idextrinsic_conversation'];

    $_SESSION['idsconversation'] = $ids;
} else {
    $compilationtime = time() - $starttime;
    unset($_SESSION['starttime']);

    $timetoshow = gmdate("H:i:s", $compilationtime);
    $lastinsid = $db->update('extrinsic_evaluation', array(
        'compilation_time' => $compilationtime
            ), "idextrinsic_evaluation=" . $_SESSION['idevaluation']);
    unset($_SESSION['idevaluation']);
    echo "<script> alert('Task 1 ended in " . $timetoshow . " ');
window.open('listen_extrinsic.php','_self');
</script>";

    exit();
}
$conversation = $db->get_extrinsic_conversation($currentid);

//echo $pippo['topic_true']. $topic[$pippo['topic_true']];

$questions = $db->get_extrinsic_questionnaire($service, $scenario);
$questionstask1 = find_items($questions, "task", "1");
$msgerr = "";
$nerr = 0;
$str = "*";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $lang->translate($_POST['lang']);

    //if postback is true by save button
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <?php echo $acof->head_tag(); ?>
        <script src="js/extrinsic.js"></script>
        <script language="javascript">
            var strSt = "<?php echo $lang->get_language($_SESSION["username"], 'LabSpeechListen', $_SESSION["language"]); ?>";
            //take the value of each checkbox 
            function ConCheckbox(idsubitem) {
                if (document.getElementById('ckGen' + idsubitem).checked == true) {
                    document.getElementById('FlagGen' + idsubitem).value = "Y";
                    document.getElementById('turn' + idsubitem).value = "";
                    document.getElementById('turn' + idsubitem).className = "nodroppable";
                } else {
                    document.getElementById('FlagGen' + idsubitem).value = "N";
                    document.getElementById('turn' + idsubitem).className = "droppable";
                    document.getElementById('turn' + idsubitem).value = strSt;
                }
            }
        </script>

    </head>
    <body>
        <?php $acof->navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']); ?>
        <div class="container">


            <div class="col-md-12 pre-scrollable" style="min-height: 100%;">


                <style>
                    h1 {
                        font-size: 120%;
                    }
                    .block-1 {
                        margin: 5px;
                        padding: 15px;
                        background: #eeeeee;
                    }
                    .block-2 {
                        margin: 5px;
                        padding: 15px;
                        background: #eeeeee;
                        display: inline-block;
                        width: 94%;
                    }

                    .columns {
                        column-count: 2;
                        -moz-column-count: 2;
                        -webkit-column-count: 2;
                    }
                    #player {
                        margin: 5px;
                        text-align: center;
                    }
                    .agent {
                        text-align: right;
                    }

                    .agent div {
                        border-radius: 10px;
                        background: skyblue;
                        padding: 10px;
                        display: inline-block;
                        text-align: right;
                    }
                    .caller div {
                        border-radius: 10px;
                        background: yellowgreen;
                        padding: 10px;
                        display: inline-block;
                    }
                    .hot {
                        color: red;
                    }
                    .negative { color: red; }
                    
                    .polite { color: green; }
					.Medium {color:orange;}
            .Cold{color:blue;}
            .PASS{color:green;}
            .FAIL{color:red;}
			.more{color:green;}
			.less{color:red;}
			.positive { color: green; }
			.neutral { color: orange; }
                </style>


                <form action="task1.php" method="post" id ="task1save" name="form1">
                    <input type="hidden" name="lang" id="lang" value="<?php echo $_POST['lang']; ?>"/>
                    <div class="block-1" id="instructions">

                        <h1><?php echo $lang->get_language($_SESSION["username"], 'LabIstrValut', $_SESSION["language"]) ?></h1>
                        <?php
                        echo $service . " ";
                        $scenariolabel = "";
                        if ($service == "DECODA") {
                            $scenariolabel = $scenario == '1' ? "Scenario 1: " . $lang->get_language($_SESSION["username"], 'LabAgainAfterD', $_SESSION["language"]) : "Scenario 2 : " . $lang->get_language($_SESSION["username"], 'LabBallHideD', $_SESSION["language"]);
                        } else {
                            $scenariolabel = $scenario == '1' ? "Scenario 1: " . $lang->get_language($_SESSION["username"], 'LabSidePartL', $_SESSION["language"]) : "Scenario 2 : " . $lang->get_language($_SESSION["username"], 'LabWhiteBlackL', $_SESSION["language"]);
                        }

                        echo $scenariolabel . "<br>";

                        echo  $lang->get_language($_SESSION["username"], 'LabTask', $_SESSION['language']). " 1:" . $lang->get_language($_SESSION["username"], 'LabMouseYellowD', $_SESSION["language"]) . "<br>";
                        ?>
                        <?php
                        
                            echo "C" . $condition;
                        
                        ?>
                    </div>
                    <div class="columns">
                        <div class="block-2" id="evaluation">

                            <div id="questions">
                                <?php
                                $counter = 1;
                                foreach ($questionstask1 as $question) {
                                    echo "<p>";
                                    echo $counter . ": <strong>" . $lang->get_language($_SESSION["username"], $question['label'], $_SESSION["language"]) . "</strong>";
                                    echo "</p><p>";
                                    for ($i = 1; $i <= 5; $i++) {
                                        echo $i . '&nbsp;<input name="conv' . $question['idextrinsic_question'] . "|" . $conversation[0]['idextrinsic_conversation'] . '" id="conv' . $question['idextrinsic_question'] . "|" . $conversation[0]['idextrinsic_conversation'] . '" class="' . substr($conversation[0]['filename']) . ' conv' . $question['idextrinsic_question'] . "|" . $conversation[0]['idextrinsic_conversation'] . '" type="radio" value="' . $i . '"></input>';
                                    }
                                    echo "</p>";
                                    $counter ++;
                                }
                                foreach ($_REQUEST as $key => $item) {
                                    $convpost = strpos($key, "conv", 0);
                                    if ($convpost === FALSE && $key != "lang") {
                                        echo '<input type="hidden" name="' . $key . '" value="' . $item . '">';
                                    }
                                }
                                echo '<input type="hidden" name="starttime|' . $conversation[0]['idextrinsic_conversation'] . '" value="' . time() . '">';
                                ?>

                                <p>

                                    <button id="nextconv" value=">>" onclick=" loadnextconv(<?php echo $maxconvid . "," . $maxquestid; ?>); return false;" ><?php echo $lang->get_language($_SESSION["username"], 'LabNextConv', $_SESSION['language']) ?></button>
                                </p>
                            </div>
                        </div>
                        <div class="block-2" id="sensei">
                            <?php
                            if ($condition == "2") {
                                echo '<h1>' . ($lang->get_language($_SESSION["username"], 'LabFeatSen', $_SESSION['language'])) . '</h1>
                            <div id="criteria"> <ul>';
                                if ($service == 'DECODA') {
                                    echo "<li>";
                                    echo $lang->get_language($_SESSION["username"], 'topic_pred', $_SESSION['language']).": <strong>" . $topic[$conversation[0]['topic_true']] . "</span></strong>";
                                    echo "</li>";
                                }
                                $conversationcriteria = $db->get_extrinsic_criteria($currentid);
                                foreach ($conversationcriteria as $crit) {
                                    if ($crit['name'] != "topic_pred") {
                                        echo "<li>";
                                        echo $lang->get_criteria_translation($_SESSION["username"], $crit['name'], $_SESSION['language']) . ": <strong>". "<span class='" . $crit['value'] . "' >"  .$lang->get_criteria_translation($_SESSION["username"], $crit['value'], $_SESSION['language']) . "</strong>";
                                        echo "</li>";
                                    }
                                }
                                echo "</ul></div>";
                            }
                            if ($service == 'DECODA') {
                                $extrinsictemplate = $db->get_extrinsic_template($currentid);
                                foreach ($extrinsictemplate as $templateitem) {
                                    //Aggiungere gestione template
                                }
                            }
                            ?>


                            <div id="features">


                                <div id="synopsis">
                                    <?php
                                    if ($condition == "2") {
                                        echo "
                                    <p><b>Synopsis:</b> ";
                                        echo utf8_encode( $conversation[0]['synopsis']);
                                    }
                                    ?>
                                    </p>
                                </div>
                                <div id="templates" style="display:none;"><b>Detected concepts:</b>
                                    <ul>
                                        <li>Lost item: "sac" (jump to turn <a href="#">16</a>, <a href="#">23</a>)
                                            <li>Location: "Gare Saint Lazare" (jump to turn <a href="#">16</a>)
                                                </ul>
                                                </div>
                                                </div>
                                                </div>                                                        
                                                <script>
                                                    $(function () {
                                                        loadExtrTrans(' <?php echo $conversation[0]['filename'] . ".trs" ?>', '<?php echo $conversation[0]['filename'] . ".wav" ?>');
                                                    });
                                                </script>
                                                <div class="block-2" id="conversation">
                                                </div>
                                                </div>
                                                </form>
                                                </div>
                                                </div>
                                                </body>
                                                </html>
                                                <?php

                                                function find_items($array, $findwhat, $value, $found = array()) {
                                                    foreach ($array as $k => $v) {
                                                        if (is_array($v)) {
                                                            $result = find_items($v, $findwhat, $value, $found);
                                                            if ($result === true) {
                                                                $found[] = $v;
                                                            } else {
                                                                $found = $result;
                                                            }
                                                        } else {
                                                            if ($k == $findwhat && $v == $value) {
                                                                return TRUE;
                                                            }
                                                        }
                                                    }
                                                    return $found;
                                                }
                                                ?>