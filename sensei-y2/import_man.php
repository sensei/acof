<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require("./class/config.php");
require("./class/db.php");

$config = Config::get_instance();
$db = Database::get_instance();
$db->connect();
$json = file_get_contents('./extrinsicimport/extrinsic.asr.json');

$obj = json_decode($json);
$errore = json_last_error_msg();
//echo $errore;
$importazione = array();

foreach ($obj as $value) {
    $item = array();
    $item['filename'] = $value->id;
    $item['topic_true'] = $value->topic_true;
    $item['synopsis'] = $value->features->synopsis[0];
    $item['criteria'] = $value->criteria;
    $importazione[] = $item;
}
foreach ($importazione as $elem) {
    $lastinsid = $db->insert('extrinsic_conversation', array(
        'filename' => $elem['filename'],
        'topic_true' => $elem['topic_true'],
        'synopsis' => utf8_decode($elem['synopsis'])
    ));
    if ($lastinsid != '') {
        ob_flush();
        echo "Inserted row: " . $lastinsid . "<br>";
        flush();
        $criteria = array();
        $criteria['id'] = $lastinsid;
        $criteria['filename'] = $elem['filename'];
        $criteria['polarity'] = $elem['criteria']->polarity;
        $criteria['conversation_temper'] = $elem['criteria']->conversation_temper;
        $criteria['topic_pred'] = $elem['criteria']->topic_pred;
        $criteria['politness'] = $elem['criteria']->politness;
        $criteria['acof'] = $elem['criteria']->acof;
        $criteria['caller_polarity'] = $elem['criteria']->caller_polarity;
        $criteria['caller_polarity_percent'] = $elem['criteria']->caller_polarity_percent;
        $criteria['politness_percent'] = $elem['criteria']->politness_percent;
        $criteria['polarity_percent'] = $elem['criteria']->polarity_percent;



        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'conversation_temper',
            'value' => $criteria['conversation_temper']
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'topic_pred',
            'value' => $criteria['topic_pred']
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'politness',
            'value' => $criteria['politness']
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'caller_polarity',
            'value' => $criteria['caller_polarity']
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'polarity',
            'value' => $criteria['polarity']
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'acof',
            'value' => $criteria['acof']
        ));

        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'caller_polarity_percent',
            'value' => $criteria['caller_polarity_percent']
        ));
        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'politness_percent',
            'value' => $criteria['politness_percent']
        ));
        $db->insert('extrinsic_conversation_criteria', array(
            'idextrinsic_conversation' => $criteria['id'],
            'filename' => $criteria['filename'],
            'name' => 'polarity_percent',
            'value' => $criteria['polarity_percent']
        ));
    } else {
        ob_flush();
        echo mysql_errno($db) . ": " . mysql_error($db) . "<br>";
        flush();
    }
}

var_dump($importazione);
