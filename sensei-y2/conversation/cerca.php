

<?php

function searchtranscription($filename) {
   
    $percorso='/var/www/sensei/trs/';
    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($percorso));
    $files = iterator_to_array($iterator, true);
// iterate over the directory
// add each file found to the archive
    foreach ($files as $key => $value) {
        try {
            if ($value->getFilename() != '.' && $value->getFilename() != '..') {
                    if(trim($value->getFilename())==trim($filename)) {
					return $value->getRealPath();
                    }
               
            }
        } catch (Exception $e) {
            echo "ERROR: Could not add the file '$key': $e\n";
        }
    }
	return "0";
}
