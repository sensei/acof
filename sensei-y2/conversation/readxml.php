<?php
session_start();
//error_reporting(E_ALL);ini_set("display_errors", 1);
if (isset($_REQUEST['filename'])) {
    $_SESSION['file'] = $_REQUEST['filename'];
}

$filename = $_SESSION['file'];
require 'cerca.php';

 $actual_link = "http://$_SERVER[HTTP_HOST]".substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/') + 1);

$transcription = searchtranscription($filename);
if ($transcription=="0"){
	echo "Conversation file: " .$filename . " not exists on this server";
	exit();
}
$xml = simplexml_load_file($transcription);
$json = json_encode($xml);
$array = json_decode($json, TRUE);
$style = explode(" ", $xml->Episode->Section[0]->Turn['speaker']);
$style = $style[0];
$html="";
$html.= '<link rel="stylesheet" href="'.$actual_link.'/css/style.css" type="text/css"/>';
$html.= '<div class="' . $style . '">';
foreach ($xml->Episode->Section as $article) {
    foreach ($article->Turn as $turn) {
        if (trim($turn) != "") {

            $arraystyle = explode(" ", $turn['speaker']);

            $appliedstyle = $arraystyle[0];
            if (trim($style) == trim($appliedstyle)) {
                $html.= utf8_decode(trim($turn));
                $html.= "<br><span style=\"font-size:10px; text-align:right; display:block; font-weight:bold;\">[" . $turn['startTime'] . " - " . $turn['endTime'] . "]</span><br>";
            } else {
                $html.= "</div>";
                $html.= '<div class="' . $appliedstyle . '">';
                $html.= str_replace("+", " ", utf8_decode(trim($turn)));
                $html.= "<br><span style=\"font-size:10px; text-align:right; display:block; font-weight:bold;\">[" . $turn['startTime'] . " - " . $turn['endTime'] . "]</span>";
            }
            $style = $appliedstyle;
        }
    }
}
$html.= "</div>";
echo $html;
