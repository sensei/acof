
<?php

session_start();

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if (!$_SESSION["username"])
    header("Location: index.php"); // User not logged in, redirect to login page
echo '  <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />';
require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/annotationform.php");
require("class/extrinsic.php");

//implementing ElasticSearch
require("class/elastic.php");

$lang = Lang::get_instance();

$config = Config::get_instance();
$extrinsic = extrinsic::get_instance();
$db = Database::get_instance();
$db->connect();
$topic = array('ETFC' => ($lang->get_language($_SESSION["username"], 'ETFC', $_SESSION['language'])),
    'ITNR' => ($lang->get_language($_SESSION["username"], 'ITNR', $_SESSION['language'])),
    'OBJT' => ($lang->get_language($_SESSION["username"], 'OBJT', $_SESSION['language'])),
    'NVGO' => ($lang->get_language($_SESSION["username"], 'NVGO', $_SESSION['language'])),
    //  'HORR' => 'Schedules',
    'AAPL' => ($lang->get_language($_SESSION["username"], 'AAPL', $_SESSION['language'])),
    'PV' => ($lang->get_language($_SESSION["username"], 'PV', $_SESSION['language'])),
    //  'NULL' => 'None',
    'VGC' => ($lang->get_language($_SESSION["username"], 'VGC', $_SESSION['language'])),
    // 'TARF' => 'Fares',
    // 'OFTP' => 'Transportation offers',
    'RETT' => ($lang->get_language($_SESSION["username"], 'RETT', $_SESSION['language'])),
    'CPAG' => ($lang->get_language($_SESSION["username"], 'CPAG', $_SESSION['language'])),
    // 'ACDT' => 'Accident',
    // 'JSTF' => 'Justification',
    'SVDO' => ($lang->get_language($_SESSION["username"], 'SVDO', $_SESSION['language'])),
        //  'RGLM' => 'Payment',
        // 'DDOC' => 'Query for documents',
        // 'AESP' => 'Passenger bahviour',
        // 'ACCS' => 'Accessibility',
        //'SRTP' => 'Website / mobile'
);
$condition = $_REQUEST['condition'];
$service = $_REQUEST['service'];
if ($condition == '2') {
    if ($service == "DECODA") {
        $topic = $_REQUEST['topic'];
        $polarity = $_REQUEST['polarity'];
        $caller_polarity = $_REQUEST['caller_polarity'];
        $politness = $_REQUEST['politness'];
        $conversation_temper = $_REQUEST['conversation_temper'];
        $acof = $_REQUEST['acof'];

        $criteriasensei = "";
        if ($topic != "" || $topic != null) {
            $criteriasensei.=" and  a.topic_true='" . $topic . "' ";
        }
        if ($polarity != "" || $polarity != null) {
            $criteriasensei .= " and c.polarity = '" . $polarity . "'";
        }

        if ($caller_polarity != "" || $caller_polarity != null) {
            $criteriasensei .= " and c.caller_polarity = '" . $caller_polarity . "'";
        }

        if ($politness != "" || $politness != null) {
            $criteriasensei .= " and c.politness = '" . $politness . "'";
        }
        if ($conversation_temper != "" || $conversation_temper != null) {
            $criteriasensei .= "  and c.conversation_temper = '" . $conversation_temper . "'";
        }

        if ($acof != "" || $acof != null) {
            $criteriasensei .= " and c.acof = '" . $acof . "'";
        }
        $texttosearch = $_REQUEST['searchtext'];
        // $textarray = explode(" ", $texttosearch);
        $where = " 1 ";
        if ($texttosearch != "") {
            $where .=" and MATCH(transcription_text) AGAINST('" . $texttosearch . "' IN BOOLEAN MODE) ";
        }
        $where .= $criteriasensei;

        $results = $db->get_all_conversation_criteria($service, $where);
    } else {

        $polarity = $_REQUEST['polarity'];
        $caller_polarity = $_REQUEST['caller_polarity'];
        $agent_polarity = $_REQUEST['agent_polarity'];

        $criteriasensei = "";

        if ($polarity != "" || $polarity != null) {
            $criteriasensei .= " and c.polarity = '" . $polarity . "'";
        }

        if ($caller_polarity != "" || $caller_polarity != null) {
            $criteriasensei .= " and c.caller_polarity = '" . $caller_polarity . "'";
        }

        if ($agent_polarity != "" || $agent_polarity != null) {
            $criteriasensei .= " and c.agent_polarity = '" . $agent_polarity . "'";
        }

        $texttosearch = $_REQUEST['searchtext'];
        // $textarray = explode(" ", $texttosearch);
        $where = " where a.service= '" . $service . "' ";
        if ($texttosearch != "") {
            $where .=" and MATCH(transcription_text) AGAINST('" . $texttosearch . "' IN BOOLEAN MODE) ";
        }
        $where .= $criteriasensei;

        $results = $db->get_all_conversation_criteria($service, $where);
    }
} else {
    $texttosearch = $_REQUEST['searchtext'];
    //$textarray = explode(" ", $texttosearch);
    $where = " where a.service= '" . $service . "' ";
    if ($texttosearch != "") {
        $where .=" and MATCH(transcription_text) AGAINST('" . $texttosearch . "' IN BOOLEAN MODE) ";
    }
    $sql = " SELECT DISTINCT a.*
FROM   view_all_conversation_collection a  " . $where;
    $results = $db->fetch_array($sql);
}

echo "<ol>";
foreach ($results as $value) {
    $transcriptionresult = $value['transcription_text'];
    echo '
                    <li class="block-1">
                        <h1><a href="#" onclick="loadExtrTrans(\'' . $value['filename'] . '.trs' . '\',\'' . $value['filename'] . '.wav' . '\')">Conversation: ' . $value['filename'] . '</a></h1>';
    echo '<div id="snippets">
                        <p>
                        <b>'.$lang->get_language($_SESSION["username"], 'LabSnippets', $_SESSION['language']).':</b>' . utf8_decode(getsnippet($transcriptionresult, $texttosearch)) . ' 
                        </p>
                    </div>';

    if ($condition == '2') {
        echo '<div id="sensei">
                            <div id="criteria">
                                <b>'.$lang->get_language($_SESSION["username"], 'LabCriteria', $_SESSION['language']).':</b>
                                <ul>';
        if ($service == "DECODA") {
            $topicval = $value['topic_true'];
            echo "<li>";
            echo $lang->get_language($_SESSION["username"], 'topic_pred', $_SESSION['language']) . ": <strong>" . ($lang->get_language($_SESSION["username"], $value['topic_true'], $_SESSION['language'])) . "</strong>";
            echo "</li>";
        }
        $conversationcriteria = $db->get_extrinsic_criteria($value['idextrinsic_conversation']);
        foreach ($conversationcriteria as $crit) {
            if ($crit['name'] != "topic_pred") {
                echo "<li>";

                echo $lang->get_criteria_translation($_SESSION["username"], $crit['name'], $_SESSION['language']) . ": <strong>" . "<div style='float:right;' class='" . $crit['value'] . "' >" . $lang->get_criteria_translation($_SESSION["username"], $crit['value'], $_SESSION['language']) . "</div></strong>";
                echo "</li>";
            }
        }

        echo '   </ul>
                           </div>
                            <div id="features">
                                <div id="synopsis"><p><b>Synopsis:</b>
                                      <p>' . utf8_encode($value['synopsis']) . '  
                                    </p>
                                </div>
                               
                            </div>
                        </div>';
    }

    echo '</li>';
}
echo "</ol>";

function getsnippet($transcription, $text) {
    $toreturn = "";
    $textarray = split(" ", $text);

    $result = "";
    foreach ($textarray as $item) {
        if (strlen($item) > 2) {
            $result.= get_surrounding_text($item, $transcription, 30);
        }
    }
    $result = boldify($result, $textarray);
    if ($result != "") {
        $toreturn = $result;
        $toreturn.= '<br>';
    }
    return $toreturn;
}

function boldify($text, $value) {
    $toreturn = $text;
    foreach ($value as $item) {
        $toreturn = str_replace($item, "<strong>$item</strong>", $toreturn);
    }
    return utf8_encode($toreturn);
}

function get_surrounding_text($keyword, $content, $padding) {
    $position = strpos($content, $keyword);
    // starting at (where keyword was found - padding), retrieve
    // (padding + keyword length + padding) characters from the content
    if ($position) {
        if ($position < 30) {
            $padding = $position - 1;
        }
        
        $snippet = substr($content, $position - $padding, (strlen($keyword) + $padding * 2));
        $firstspace=  strpos($snippet, ' ');
        $snippet=  substr($snippet, $firstspace);
        $lastspace=strrpos ($snippet, ' ');
         $snippet=  substr($snippet, 0,$lastspace);
        return '...' . $snippet . '...';
    } else {
        $snippet = substr($content, 0, 30);
        $snippet=  str_replace('+', ' ', $snippet);
        return '...' . $snippet . '...';
    }
}

?>