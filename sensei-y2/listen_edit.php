<?php
session_start();

if (!$_SESSION["username"]) header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/annotationform.php");

$config = Config::get_instance();

$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('Listen Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$annotationform = Annotationform::get_instance();

$msgerr = "";
$nerr=0;
$str = "*";
$confirm="";


if ($_SERVER["REQUEST_METHOD"] == "POST"){
	//storage all post variables into session variables
	if (basename($_SERVER["HTTP_REFERER"])=='report.php' ){
		foreach ($_POST as $key => $value) {
			//${$key} = $value;
			$_SESSION[$key] = $value;
		}
	} 

	$lang -> translate($_POST['lang']);
	
	//if postback is true by save button
	if ($_POST["flagsave"]==="1"){
		if (empty($_POST["filename"])){
			$flagerrfile = "<span class='error'>".$str."</span>";
		}
		else{
			$filename = $acof-> checkinput($_POST["filename"]);		
			$flagerrfile='';
		}	
		
		if ($_POST["finalnote"]===''){
			$flagerrcons = "<span class='error'>".$str."</span>";
		}
		else{
			$finalnote = $acof-> checkinput($_POST["finalnote"]);		
			$flagerrcons='';
		}

		if ($_POST["synopsisnote"]===''){
			$flagerrsyn = "<span class='error'>".$str."</span>";
		}
		else{
			$synopsisnote = $acof-> checkinput($_POST["synopsisnote"]);
			$flagerrsyn='';
		}
		
		if ($flagerrfile==='' && $flagerrcons==='' && $flagerrsyn==='' && $nerr ===0){
			
			if (!$_GET['id']) {
				echo 'ERROR on getting ID. Please contact admin.';
				exit;
			}
			
			$lastinsid = $db->update('tblListen', 
				array(
					'FileName' => $filename,
					'Comment' => $finalnote,
					'SynopsisNew' => $synopsisnote,
					'LenSynopsis' => $_POST["lenxml"],									
					'IpAddress' => $_SESSION['ipaddress'],		
					'sysuser' => $_SESSION["username"],
					'sysdatemod' => 'NOW()'
					), 
				"idListen = '".$_GET['id']."'");

			if ($lastinsid != 0){
				$confirm = "<div class='alert alert-success' role='alert'>".$lang -> get_language($_SESSION["username"],'LabSavedListen',$_SESSION['language'])."</div><br>";
				$log->ins_log('Update Listen OK', $_SESSION["username"] );
			}
			else
			{
				$msgerr = "<div class='alert alert-danger' role='alert'>".$lastinsid." - ".$lang -> get_language($_SESSION["username"],'LabErrSaveListen',$_SESSION['language'])."</div>";		 
				$log->ins_log('Update Listen KO', $_SESSION["username"] );
			}
		}
		else $msgerr = "<div class='alert alert-danger' role='alert'><strong>* ".$lang -> get_language($_SESSION["username"],'LabReqFieldsListen',$_SESSION['language'])."</strong></div>";	
	}
}

$listen = $db->fetch_array('SELECT * FROM tblListen WHERE idListen = '.$_GET['id'].' LIMIT 1');
$listen = $listen[0];

$_POST['filename'] = $listen['FileName'];
$_POST["finalnote"] = html_entity_decode($listen['Comment']);
$_POST["synopsisnote"] = html_entity_decode($listen['SynopsisNew']);
$_POST["service"] = $listen['Service'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
	<?php echo $acof -> head_tag(); ?>
	<script language="javascript">
		var strSt = "<?php echo $lang -> get_language($_SESSION["username"],'LabSpeechListen',$_SESSION['language']);?>"; 

		function ConCheckbox(idsubitem){
			if (document.getElementById('ckGen'+idsubitem).checked == true){
				document.getElementById('turn'+idsubitem).value = "";
				document.getElementById('turn'+idsubitem).className = "nodroppable";
			}
			else{
				document.getElementById('turn'+idsubitem).className = "droppable";
				document.getElementById('turn'+idsubitem).value = strSt;
			}
		}
	</script>
  </head>

  <body onload="document.form1.btnload.click()";>
	<?php $acof -> navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']); ?>
	<div class="container">
		<div class="col-md-8 pre-scrollable" style="min-height: 100%;">
			<form action="" method="post" id="form1" name="form1">
				<input type="hidden" name="lang" id="lang" value="<?php echo $_POST['lang'];?>"/>
				<input type="hidden" name="flagsave" id="flagsave" value="0"/>
				<input type="hidden" name="sysdate" id="sysdate" value="<?php echo $_POST['sysdate'];?>"/>
				<input type="hidden" name="startdate" id="startdate"  value="" />
				<input type="hidden" name="enddate" id="enddate" value=""/>
				
				<?php
					if($msgerr!='') echo $msgerr;		
					else if ($confirm!=''){	
						$btn = $lang -> get_language($_SESSION["username"],'LabBackBtn',$_SESSION['language']);
						echo $confirm."<p align=left><a href='report.php'><button type='button' class='btn btn-primary'>".$btn."</button></a></p>";						
						exit;
					}	
		
					echo '<br />';
					echo $acof->show_info('', $lang -> get_language($_SESSION["username"],'LabInstrListen',$_SESSION['language']), '', $case='view');
					
					/******/
					
					$LabServListen = ($lang -> get_language($_SESSION["username"],'LabServListen',$_SESSION['language']));
					$LabFileListen =($lang -> get_language($_SESSION["username"],'LabFileListen',$_SESSION['language']));
					$LabLoadFBtn =($lang -> get_language($_SESSION["username"],'LabLoadFBtn',$_SESSION['language']));
										
					if ($_POST["service"]=='') $service = 'LUNA';
					else $service = $_POST["service"];
					
					echo '<br />';
					echo ($annotationform->show_file_select($LabServListen, $LabFileListen, $LabLoadFBtn, $service, '', $_POST['filename'], "update"));
				
					/******/
					
					echo '<br />';
					echo ($annotationform->show_questionnaire($_SESSION['language'], $_SESSION['username'], $service, $_POST, $_GET, "update"));
				?>
			</form>
		</div>
		<div id="transcription-area" class="col-md-4 pre-scrollable" style="min-height: 100%;">
			
		</div>
	</div>	
	<?php echo $acof -> foot_page(); ?>
	<script language="javascript">		
		$(function() {
			var strSt = "<?php echo $lang -> get_language($_SESSION["username"],'LabSpeechListen',$_SESSION['language']);?>"; 
			$('.general').change(function() {
				var val;
				if ($(this).is(':checked')) {
					val = 'Y';
					$(this).parent().prev().find('textarea').addClass('nodroppable');
				} else {
					val = 'N';
					$(this).parent().prev().find('textarea').removeClass('nodroppable');
				}
				var $this = $(this);
				$.ajax({
				  type: "POST",
				  url: "ajax.php",
				  data: { action: 'saveFlgGeneral', id: $(this).attr('data-id'), value: val }
				})
				  .done(function( msg ) {
					var bg = $this.parent().css('background-color');
					$this.parent().css({backgroundColor: '#00FF00'});
					setTimeout(function() { $this.parent().css({backgroundColor: bg});}, 1500);
				  });
				$(this).parent().prev().find('textarea').val('');
			});
			$( ".droppable" ).droppable({
				
				drop: function( event, ui ) {
					var $this = $(this);
					
					if ($this.val() == '' || $this.val() == strSt) {
						$this.focus().val(ui.draggable.text().replace(/(\r\n|\n|\r)/gm," ").replace(/ +(?= )/g,''));
					} 
					else {
						$this.focus().val($this.val() + "|" + ui.draggable.text().replace(/(\r\n|\n|\r)/gm," ").replace(/ +(?= )/g,''));
					}
					$.ajax({
					  type: "POST",
					  url: "ajax.php",
					  data: { action: 'saveTurn', id: $this.attr('data-id'), value: $this.val() }
					})
					  .done(function( msg ) {
						var bg = $this.css('background-color');
						$this.css({backgroundColor: '#00FF00'});
						setTimeout(function() { $this.css({backgroundColor: bg});}, 1500);
					  });
				}				
			}).change(function() {
				var $this = $(this);
				$.ajax({
				  type: "POST",
				  url: "ajax.php",
				  data: { action: 'saveTurn', id: $this.attr('data-id'), value: $this.val() }
				})
				  .done(function( msg ) {
					var bg = $this.css('background-color');
					$this.css({backgroundColor: '#00FF00'});
					setTimeout(function() { $this.css({backgroundColor: bg});}, 1500);
				  });
			});
			
			$("#synopsisnote").keyup(function(){
					var lenSys = $(this).val().length;
					var lenXml = $("#lenxml").val();
					//7% of the text xml
					var lenFromPerc = Math.round((lenXml*7)/100).toFixed(0);
					var diff = parseInt(lenFromPerc)-(parseInt(lenFromPerc)-parseInt(lenSys));
					var str = '<?php echo $lang -> get_language($_SESSION["username"],'LabCharsSyn',$_SESSION['language']);?> '+diff+' su '+lenFromPerc+' [max 1000]';
					//put the value of number into badge
					$("#lenSys").text(str);		
					
					if (lenSys>lenFromPerc)					
						$("#lenSys").css("background-color", "red");
					else
						$("#lenSys").css("background-color", "#a5acb1");					
				}
			);			
		});		
	</script>
  </body>
</html>