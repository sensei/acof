<?php

session_start();
require("class/config.php");
require("class/db.php");
$config = Config::get_instance();
$db = Database::get_instance();
$db->connect();
$service = $_REQUEST['service'];
$condition = $_REQUEST['condition'];
$scenario = $_REQUEST['scenario'];
$collections = $_REQUEST['collections'];
$compilationtime = $_REQUEST['compilationtime'];
$task = $_REQUEST['task'];
$idevaluation = $db->insert('extrinsic_evaluation', array(
    'service' => $service,
    'condition' => $condition,
    'scenario' => $scenario,
    'task' => $task,
    'sysuser' => $_SESSION["username"],
    'compilation_time' => $compilationtime
        ));
$singlecollectionarray = explode(";", $collections);
$toreturn = "OK";
foreach ($singlecollectionarray as $value) {
    $coll = explode("|", $value);
    $lastinsid = $db->insert('extrinsic_evaluation_item', array(
        'value' => mysql_escape_string($coll[1]),
        'idextrinsic_question' => $coll[0],
        'service' => $service,
        'condition' => $condition,
        'scenario' => $scenario,
        'task' => $task,
        'sysuser' => $_SESSION["username"],
        'idextrinsic_evaluation' => $idevaluation
    ));
    if ($lastinsid < 1) {
        $toreturn = "KO";
    }
}
echo $toreturn;

