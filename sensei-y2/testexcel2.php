<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/Rome');


/** Include PHPExcel */
require_once './lib/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Teleperformance")
        ->setLastModifiedBy("Pierluigi Buongiorno")
        ->setTitle("Esempio xls")
        ->setSubject("Esempio xls")
        ->setDescription("Esempio xls")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Esempio xls");

// Set Column width
//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);

// Set Column label
$colLabel = array();
$colLabel[]="ID";
$colLabel[]="Transcription File";

// Set [showcol] label
//$showcol=$post['showcol'];
$showcol=array("x1", "x2", "x3");
foreach($showcol as $k => $v) $colLabel[]=$v;

$colLabel[]="Service";
$colLabel[]="Score";
$colLabel[]="Insert Date";
$colLabel[]="Modify Date";
$colLabel[]="Inserted By";

// Set [showsubitem] label
//$showsubitem=$post['showsubitem'];
$showsubitem=array("sub1", "sub2", "sub3");
foreach($showsubitem as $k => $v) $colLabel[]=$v;


for ($i = 0; $i < count($colLabel); $i++) {
    $colString = PHPExcel_Cell::stringFromColumnIndex($i);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.'1', $colLabel[$i]);
}

for ($i = 0; $i < 10; $i++) {
    $colString = PHPExcel_Cell::stringFromColumnIndex($i);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.'2', $i);
}

$objPHPExcel->getActiveSheet()->setTitle('Elastic Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="report.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;