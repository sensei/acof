<?php

session_start();
require("class/config.php");
require("class/db.php");
$config = Config::get_instance();
$db = Database::get_instance();
$db->connect();
$text = "appelle madame";
$sql = " SELECT idextrinsic_conversation,transcription_text
FROM extrinsic_conversation
WHERE MATCH(transcription_text) AGAINST('$text' IN BOOLEAN MODE)";
$out = $db->fetch_array($sql);
echo "<table>";
foreach ($out as $value) {
    echo "<tr>";
    echo "<td>" . $value[idextrinsic_conversation] . "</td>";
    echo "<td>" . boldify(get_surrounding_text("appelle", $value['transcription_text'], 20),"appelle") . "<br>";
    echo  boldify(get_surrounding_text("madame", $value['transcription_text'], 20),"madame") . "</td>";
    echo "</tr>";
}
echo "</table>";

function boldify($text, $value) {
   return utf8_decode(str_replace($value, "<strong>$value</strong>", $text));
    
}

function get_surrounding_text($keyword, $content, $padding) {
    $position = strpos($content, $keyword);
    // starting at (where keyword was found - padding), retrieve
    // (padding + keyword length + padding) characters from the content
    $snippet = substr($content, $position - $padding, (strlen($keyword) + $padding * 2));
    return '...' . $snippet . '...';
}
