<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="utf-8" indent="yes"/>


<xsl:template match="/">

	<div class="audio_filename">
		Audio Filename:<xsl:value-of select="*/@audio_filename"/>
	</div>
	
	<xsl:for-each select="//Turn">		

		<div class="turn"><xsl:if test="normalize-space(.)">[<xsl:value-of select="@startTime"/>]<xsl:value-of select="."/>[<xsl:value-of select="@endTime"/>]</xsl:if></div>
		
	</xsl:for-each>

</xsl:template>
</xsl:stylesheet>