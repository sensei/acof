<?php
error_reporting(E_ALL);ini_set("display_errors", 1);
require '../lib/vendor/autoload.php';
$pluto=date("Y-m-d\TH:i:s\Z",1333699439);
$parame = array();
$parame['hosts'] = array(
    'http://es_admin:admin123@192.168.202.65:9200' // IP + Port
);
$client = new Elasticsearch\Client($parame);

$json = file_get_contents('http://localhost/senseinew/import/TP_full.json');
$obj = json_decode($json);

$importazione = array();

echo "pippo";
foreach ($obj as $key) {
    $body = array();
    $body['idListen'] = $key->idListen;
    $body['FileName'] = $key->FileName;
    $body['Comment'] = $key->Comment;
    $body['SynopsisNew'] = $key->SynopsisNew;
    $body['LenSynopsis'] = $key->LenSynopsis;
    $body['Service'] = $key->Service;
    $body['Score'] = $key->Score;
    $body['IpAddress'] = $key->IpAddress;
    $body['tbllistensysdate'] = date( 'Y-m-d\TH:i:s\Z', strtotime($key->tbllistensysdate) );
    $body['tbllistensysdatemod'] = date( 'Y-m-d\TH:i:s\Z', strtotime($key->tbllistensysdatemod) );
    $body['StartDate'] = date( 'Y-m-d\TH:i:s\Z', strtotime($key->StartDate) );
    $body['EndDate'] = date( 'Y-m-d\TH:i:s\Z', strtotime($key->EndDate) );
    $body['idScore'] = $key->idScore;
    $body['idSubItem'] = $key->idSubItem;
    $body['tbllistenscoreScore'] = $key->tbllistenscoreScore;
    $body['tbllistenscoreScoreValue'] = $key->tbllistenscoreScoreValue;
    $body['Note'] = $key->Note;
    $body['Turn'] = $key->Turn;
    $body['FlagGeneral'] = $key->FlagGeneral;
    $body['tbllistenscoresysdate'] = date( 'Y-m-d\TH:i:s\Z', strtotime($key->tbllistenscoresysdate) );
    $body['listenscoresysdatemod'] = date( 'Y-m-d\TH:i:s\Z', strtotime($key->listenscoresysdatemod) );
    $body['idUser'] = $key->idUser;
    $importazione[] = $body;
    unset($body);
}
$log = array();
foreach ($importazione as $corpo) {

    $log[] = insert2("acofnew", $client, $corpo, "acof", NULL);
}
file_put_contents('log.json',json_encode($log));


function insert2($index, $client, $body, $type, $id) {
    $params = array();
    if ($id != NULL) {
        $params['id'] = $id;
    }
    $params['index'] = $index;
    $params['type'] = $type;
    $params['body'] = $body;
    $ret = $client->index($params);
    return $ret;
}
