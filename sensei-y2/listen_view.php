<?php
session_start();

if (!$_SESSION["username"]) header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/annotationform.php");

$config = Config::get_instance();

$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('Listen Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$annotationform = Annotationform::get_instance();

$msgerr = "";
$nerr=0;
$str = "*";
$confirm="";


if ($_SERVER["REQUEST_METHOD"] == "POST"){
	//storage all post variables into session variables
	if (basename($_SERVER["HTTP_REFERER"])=='report.php' ){
		foreach ($_POST as $key => $value) {
			//${$key} = $value;
			$_SESSION[$key] = $value;
		}
	}
	$lang -> translate($_POST['lang']);	
}

$listen = $db->fetch_array('SELECT * FROM tblListen WHERE idListen = '.$_GET['id'].' LIMIT 1');
$listen = $listen[0];

$_POST['filename'] = $listen['FileName'];
$_POST["finalnote"] = html_entity_decode($listen['Comment']);
$_POST["synopsisnote"] = html_entity_decode($listen['SynopsisNew']);
$_POST["service"] = $listen['Service'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
	<?php echo $acof -> head_tag(); ?>
  </head>
	
  <body onload="document.form1.btnload.click()";>
	<?php $acof -> navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']); ?>
	<div class="container">
		<div class="col-md-8 pre-scrollable" style="min-height: 100%;">
			<form action="" method="post" id="form1" name="form1">
				<input type="hidden" name="lang" id="lang" value="<?php echo $_POST['lang'];?>"/>
				<input type="hidden" name="flagsave" id="flagsave" value="0"/>
				<input type="hidden" name="sysdate" id="sysdate" value="<?php echo $_POST['sysdate'];?>"/>
				<input type="hidden" name="startdate" id="startdate"  value="" />
				<input type="hidden" name="enddate" id="enddate" value=""/>
				
				<?php
					//show the confirm/alert message
					if($msgerr!='') echo $msgerr;		
					else if ($confirm!=''){	
						$btn = $lang -> get_language($_SESSION["username"],'LabBackBtn',$_SESSION['language']);
						//echo $confirm."<p align=left><a href='report.php'><button type='button' class='btn btn-primary'>".$btn."</button></a></p>";						
						exit;
					}	
					
					echo '<br />';
					echo $acof->show_info('', $lang -> get_language($_SESSION["username"],'LabInstrListen',$_SESSION['language']), '', $case='view');
					
					/******/
					
					$LabServListen = ($lang -> get_language($_SESSION["username"],'LabServListen',$_SESSION['language']));
					$LabFileListen =($lang -> get_language($_SESSION["username"],'LabFileListen',$_SESSION['language']));
					$LabLoadFBtn =($lang -> get_language($_SESSION["username"],'LabLoadFBtn',$_SESSION['language']));
										
					if ($_POST["service"]=='') $service = 'LUNA';
					else $service = $_POST["service"];
					
					echo '<br />';
					echo ($annotationform->show_file_select($LabServListen, $LabFileListen, $LabLoadFBtn, $service, '', $_POST['filename'], "view"));
					
					/******/
					
					echo '<br />';
					echo ($annotationform->show_questionnaire($_SESSION['language'], $_SESSION['username'], $FlagService, $_POST, $_GET, "view"));
				?>				
			</form>
		</div>
		<div id="transcription-area" class="col-md-4 pre-scrollable" style="min-height: 100%;"></div>
	</div>	
	<?php echo $acof -> foot_page(); ?>	
  </body>
</html>

<?php
	$db->close();
?>