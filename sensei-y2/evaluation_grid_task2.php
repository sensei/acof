<?php
session_start();

ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

if (!$_SESSION["username"])
    header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/lang.php");
require("class/acof.php");
require("class/report.php");
require("class/elastic.php");

$config = Config::get_instance();

$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('E Search Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$report = Report::get_instance();

if ($_SERVER["REQUEST_METHOD"] == "POST" || $_SESSION['language'] == '') {
    //load the default language
    $lang->translate($_POST['lang']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <?php echo $acof->head_tag('predicted'); ?>	
        <script type="text/javascript">
            $(document).ready(function () {
                var url = "evaluation_report_task2.php";

                // prepare the data
                var source =
                        {
                            datatype: "json",
                            datafields: [
                                {name: 'idextrinsic_evaluation'},
                                {name: 'service'},
                                {name: 'scenario'},
                                {name: 'condition'},
                                {name: 'task', },
                                {name: 'sysuser'},
                                {name: 'compilation_time'},
                                {name: 'sysdate'},
                                {name: 'question'},
                                {name: 'value'}
                            ],
                           // id: 'idextrinsic_evaluation',
                            url: url,
                            root: 'data',
                            pager: function (pagenum, pagesize, oldpagenum) {
                                // callback called when a page or page size is changed.
                            }
                        };
                var addfilter = function () {
                    var filtergroup = new $.jqx.filter();

                    var filter_or_operator = 1;
                    var filtervalue = 'Andrew';
                    var filtercondition = 'equal';
                    var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);

                    filtergroup.addfilter(filter_or_operator, filter1);
                    // add the filters.
                    $("#jqxgrid").jqxGrid('addfilter', 'firstname', filtergroup);
                    // apply the filters.
                    $("#jqxgrid").jqxGrid('applyfilters');
                }

                var dataAdapter = new $.jqx.dataAdapter(source);
                var larghezza = 0.9 * $(window).width();

                $("#jqxgrid").jqxGrid(
                        {
                            width: larghezza,
                            source: dataAdapter,
                            filterable: true,
                            sortable: true,
                            pageable: true,
                            ready: function () {
                                addfilter();
                            },
                            autoshowfiltericon: true,
                            columnsresize: true,
                            columns: [
                                {text: 'Id', dataField: 'idextrinsic_evaluation'},
                                {text: 'Service', dataField: 'service'},
                                {text: 'Scenario', dataField: 'scenario'},
                                {text: 'Condition', dataField: 'condition'},
                                {text: 'Task', dataField: 'task'},
                                {text: 'User', dataField: 'sysuser'},
                                {text: 'Compilation time', dataField: 'compilation_time', cellsalign: 'right', type: 'int', width: 100},
                                {text: 'Date', dataField: 'sysdate', width: 100, cellsalign: 'right', type: 'date'},
                                {text: 'Question', dataField: 'question'},
                                {text: 'Answer', dataField: 'value'}


                            ]
                        });

               
            });
        </script>
    </head>

    <body>
        <?php
        $acof->navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']);
        $consideration = $lang->get_language($_SESSION["username"], 'LabConsListen', $_SESSION["language"]);
        ?>
        <div class="container">
            <div id="predicted"><h3>Evaluation Report Task 2</h3></div>
            <div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left;">
                <div id="jqxgrid"></div>
                 <a href="task2export.php"><img alt="Export to excel" src="./img/excel.png"></img></a>

            </div>
        </div>
    </body>
</html>