<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/Rome');


/** Include PHPExcel */
require_once './lib/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Teleperformance")
        ->setLastModifiedBy("Pierluigi Buongiorno")
        ->setTitle("Esempio xls")
        ->setSubject("Esempio xls")
        ->setDescription("Esempio xls")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Esempio xls");
$objPHPExcel->getActiveSheet ()->getColumnDimension ( 'A' )->setWidth ( 10 );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( 'B' )->setWidth ( 52 );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( 'C' )->setWidth ( 52 );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( 'D' )->setWidth ( 52 );
                $objPHPExcel->getActiveSheet ()->getColumnDimension ( 'E' )->setWidth ( 52 );
                $objPHPExcel->getActiveSheet ()->getColumnDimension ( 'F' )->setWidth ( 52 );
                $objPHPExcel->getActiveSheet ()->getColumnDimension ( 'G' )->setWidth ( 52 );
                $objPHPExcel->getActiveSheet ()->getColumnDimension ( 'H' )->setWidth ( 52 );
$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'idListen')
        ->setCellValue('B1', 'FileName')
        ->setCellValue('C1', 'Comment')
        ->setCellValue('D1', 'SynopsisNew')
        ->setCellValue('E1', 'sysdate')
        ->setCellValue('F1', 'idSubItem')
        ->setCellValue('G1', 'Score')
        ->setCellValue('H1', 'Note')
        ;
	
$rows= interrogadb();
for($i=0;$i<count($rows);$i++){
    $j=$i+2;
    $objPHPExcel->setActiveSheetIndex (0)->setCellValue ( 'A' . $j, '' . $rows[$i]['idListen'] );
    $objPHPExcel->setActiveSheetIndex (0)->setCellValue ( 'B' . $j, '' . $rows[$i]['FileName'] );
    $objPHPExcel->setActiveSheetIndex (0)->setCellValue ( 'C' . $j, '' . $rows[$i]['Comment'] );
    $objPHPExcel->setActiveSheetIndex (0)->setCellValue ( 'D' . $j, '' . $rows[$i]['SynopsisNew'] );
    $objPHPExcel->setActiveSheetIndex (0)->setCellValue ( 'E' . $j, '' . $rows[$i]['sysdate'] );
    $objPHPExcel->setActiveSheetIndex (0)->setCellValue ( 'F' . $j, '' . $rows[$i]['idSubItem'] );
    $objPHPExcel->setActiveSheetIndex (0)->setCellValue ( 'G' . $j, '' . $rows[$i]['Score'] );
    $objPHPExcel->setActiveSheetIndex (0)->setCellValue ( 'H' . $j, '' . $rows[$i]['Note'] );
}

$objPHPExcel->getActiveSheet()->setTitle('Elastic Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="report.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

function interrogadb() {


    require 'include/functions.php';
    error_reporting(0);
    $servername = "localhost";
    $username = "root";
    $password = "sensei";
    $dbname = "sensei";


    $conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sth = mysqli_query($conn, "SELECT
tbllisten.idListen,
tbllisten.FileName,
tbllisten.`Comment`,
tbllisten.SynopsisNew,
tbllisten.LenSynopsis,
tbllisten.Service,
tbllisten.Score,
tbllisten.IpAddress,
tbllisten.sysdate as tbllistensysdate,
tbllisten.sysdatemod as tbllistensysdatemod,
tbllisten.StartDate,
tbllisten.EndDate,
tbllistenscore.idScore,
tbllistenscore.idSubItem,
tbllistenscore.Score as tbllistenscoreScore,
tbllistenscore.ScoreValue as tbllistenscoreScoreValue,
tbllistenscore.Note,
tbllistenscore.Turn,
tbllistenscore.FlagGeneral,
tbllistenscore.sysdate as tbllistenscoresysdate,
tbllistenscore.sysdatemod as listenscoresysdatemod,
tbluser.idUser
FROM
tbllisten
INNER JOIN tbllistenscore ON tbllistenscore.idListen = tbllisten.idListen
INNER JOIN tbluser ON tbllisten.sysuser = tbluser.Login");
    $rows = array();
    while ($r = mysqli_fetch_assoc($sth)) {
        $rows[] = $r;
    }
    return $rows;
}
