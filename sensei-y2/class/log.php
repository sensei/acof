<?php
/**
     
    USAGE: $log = Log::get_instance();
	NEED: db.php
     
**/
class Log {
	
    // Store the single instance of the object
    private static $instance ;
	
	/**
        Constructor
    **/
    private function __construct() {
		$this->db = Database::get_instance();
    }// END CONSTRUCTOR
	
	/**
        Singleton Declaration
    **/
    public static function get_instance() {
        if( !self::$instance) {
            self::$instance = new Log () ;    
        }
        
        return self::$instance ;
    }// END SINGLETON DECARATION
	
	//log insert function for each user action
	public function ins_log($action,$user) {	
		$db = $this->db;
		
		$ret = $db->insert('tblLog', array(
			'Action' => $action,
			'sysuser' => $user,
			'SessionID' => session_id(),
			'IpAddress' => $_SESSION['ipaddress'],
		));
		
		return $ret;
	}
}

?>