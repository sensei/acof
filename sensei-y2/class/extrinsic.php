<style>
#extrinsictableconv td {
    height: 50px;
}
</style>
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of extrinsic
 *
 * @author buongiorno.10
 */
class extrinsic {

    private static $instance;

    private function __construct() {
        $this->config = Config::get_instance();
        $this->db = Database::get_instance();
        $this->lang = Lang::get_instance();
    }

    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new extrinsic ();
        }

        return self::$instance;
    }

    public function show_extr_eval_questionnaire($collection, $service, $scenario, $condition) {
        $db = $this->db;
        $questions = $db->get_extrinsic_questionnaire($service, $scenario, $condition);
        $conversations = $db->get_extrinsic_conversations($collection, $service, $scenario, $condition);
        $task1 = $this->find_items($questions, "task", "1");
        $task2 = $this->find_items($questions, "task", "2");
        if (count($questions) < 1 or count($conversations) < 1) {
            exit("There are no conversations");
        }
        /*
         * Inizio tabella livello conversation
         * */
        $starttime = time();
        $toreturn = '
            <input type="hidden" name="starttime" id ="starttime" value="' . $starttime . '">
							<input type="hidden" name="endtime" id ="endtime" value="">
							<h3 style="text-align:center !important;"> Task 1 - Conversation level</h3>
            <table class= "table-bordered" id="extrinsictableconv"  width="100%">
    <thead>
        <tr>
            <th rowspan="2" style="width:20%;" class="tdcenter">
                Conversation
            </th>
            <th  colspan="2" style="width:40%;" class="tdcenter">
                Questions
            </th>

        </tr>
        <tr>';
        foreach ($task1 as $value) {
            $toreturn.='<td class="tdcenter" style="width:20%;">';
            $toreturn.= $value['question'];
            $toreturn.=' </td>';
        }



        $toreturn.='
        </tr>
    </thead>
    <tr>
        <td>
            &nbsp;
        </td>';
        foreach ($task1 as $value) {
            $toreturn.=' <td>
            <table border="0" width="100%">
                <tr>
                    <td class="tdcenter">
                        1
                    </td>
                    <td class="tdcenter">
                        2
                    </td>
                    <td class="tdcenter">
                        3
                    </td>
                    <td class="tdcenter">
                        4
                    </td>
                    <td class="tdcenter">
                        5
                    </td>
                </tr>
            </table>
        </td>';
        }


        foreach ($conversations as $value) {
            $toreturn.='<tr border="0">
        <td>
		<p align="center">
		
		<button onclick="javascript:loadExtrTrans(\''.$value['transcription_file'].'\',\''.$value['filename'].'\')" class="btn btn-sm btn-primary" style="" id="" type="button">Load '.substr($value['filename'],0,-4).'</button>
		</p>
        </td>';

            foreach ($task1 as $question) {
                $toreturn.=' <td class="tdcenter">
            <table border="0" width="100%">
                <tr>';
                for ($cell = 1; $cell <= 5; $cell++) {
                    $toreturn.='  <td class="tdcenter">
                        <input name="conv' . $question['idextrinsic_question'] . "|" . $value['idextrinsic_conversation'] . '" id="conv' . $question['idextrinsic_question'] . "|" . $value['idextrinsic_conversation'] . '" type="radio" value="' . $cell . '"></input>
                    </td>';
                }
                $toreturn.='  </tr>
            </table>
        </td>';
            }
            $toreturn.='</tr>';
        }
        $toreturn.='</table>';

        /*
         * Fine tabella livello conversation
         * */
        $toreturn.='<br>';
        /*
         * Inizio tabella collection
         * */
        $toreturn.=' <h3 style="text-align:center !important;"> Task 2 - Collection level</h3>
                    <table class="table-bordered" id="collectiontable" width="100%">
                        <tr>
                       ';
        foreach ($task2 as $collquest) {
            $toreturn.='<tr  border="0">';
            $toreturn.=' <td class="tdcenter" style="width:50%;">' . $collquest['question'] . '</td>';
            $toreturn.=' <td class="tdcenter"><textarea rows="4" cols="50" id="coll' . $collquest['idextrinsic_question'] . '|0" name="coll' . $collquest['idextrinsic_question'] . '|0"></textarea></td>';
            $toreturn.='</tr>';
        }
        $toreturn.='<tr><td colspan="2"><p align="center"><button onclick="SaveExtrEvaluation();" class="btn btn-lg btn-success" style="" id="" type="button">Salva</button></p></td></tr>';
        $toreturn.='</table>
               <form id="extrinsic_eval" method="post" name="extrinsic_eval" action="getquest.php">
               <input type="hidden" name="postconversations" id="postconversations">
               <input type="hidden" name="postcollections" id="postcollections">
               <input type="hidden" name="postservice" id="postservice">
               <input type="hidden" name="postscenario" id="postscenario">
               <input type="hidden" name="postcollection" id="postcollection">
               <input type="hidden" name="postcondition" id="postcondition">
               <input type="hidden" name="postcompilation_time" id="postcompilation_time">
                </form>';
        return $toreturn;
    }

    public function find_items($array, $findwhat, $value, $found = array()) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $result = $this->find_items($v, $findwhat, $value, $found);
                if ($result === true) {
                    $found[] = $v;
                } else {
                    $found = $result;
                }
            } else {
                if ($k == $findwhat && $v == $value) {
                    return TRUE;
                }
            }
        }
        return $found;
    }

}
