<?php
/**
     
    USAGE: $authentication = Authentication::get_instance();
	NEED: db.php - log.php
     
**/

class Authentication {

	// Store the single instance of the object
    private static $instance ;
	
	/**
        Constructor
    **/
    private function __construct() {
		$this->db = Database::get_instance();
		$this->log = Log::get_instance();
    }// END CONSTRUCTOR
	
	/**
        Singleton Declaration
    **/
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new Authentication () ;    
        }
		
        return self::$instance ;
    }// END SINGLETON DECARATION
	
	//echo the login form
	public function show_login ($msg) {
		echo '<form class="form-signin" role="form" method="post" id="form1" name="form1" action="index.php">		

				<img width="101%" height="101%" src="img/sensei.png"/>
				<h2 class="form-signin-heading">Login</h2>
				<input type="user" class="form-control" id="user" name="user" required>
				<input type="password" class="form-control" id="pass" name="pass" required>
	
				<button class="btn btn-lg btn-primary btn-block" type="submit">Entra</button>
				<br>
				'.$msg.'
			</form>';
	}
	
	//check the login data
	public function check_login ($user, $pass) {
		$db = $this->db;
		$log = $this->log;
		
		if ($user!='' && $pass!=''){
			//check the user in database
			$arrReturn = $db->fetch_array( "SELECT Login, Nome, Cognome, FlagWrite, FlagRead, FlagAdmin FROM tblUser WHERE login = '".$user."' and password = md5('".$pass."')") ;
			//$db -> close();
			
			if (count($arrReturn)==0){
				$log->ins_log('Login Failed', $user);
				return false;
			} else {
				//save in the session data
				$_SESSION['username'] = $arrReturn[0]['Login'];
				$_SESSION['name'] 	  = $arrReturn[0]['Nome'];
				$_SESSION['surname']  = $arrReturn[0]['Cognome'];
				$_SESSION['flagW'] 	  = $arrReturn[0]['FlagWrite'];
				$_SESSION['flagR'] 	  = $arrReturn[0]['FlagRead'];
				$_SESSION['flagA'] 	  = $arrReturn[0]['FlagAdmin'];

				$log->ins_log('Login Success', $arrReturn[0]['Login'] );
				return true;
			}	
		}
	}
}

?>