<?php

/**

  USAGE: $db = Database::get_instance();
  NEED: config.php

 * */
class Database {

    // Debug flag for showing error messages
    public $debug = true;
    // Store the single instance of the Database
    private static $instance;
    // Server connection parameters
    private $server = "";
    private $user = "";
    private $pass = "";
    private $database = "";
    // Database Error
    private $error = "";
    // Number of rows affected by SQL query
    public $affected_rows = 0;
    // Link and Query resource variables
    private $link_id = 0;
    private $query_id = 0;

    /**
      Constructor
     * */
    private function __construct() {
        $this->config = Config::get_instance();
        $config = $this->config;

        $this->server = $config->get_ini_value("DB", "HOST");
        $this->user = $config->get_ini_value("DB", "USERNAME");
        $this->pass = $config->get_ini_value("DB", "PASSWORD");
        $this->database = $config->get_ini_value("DB", "DATABASE");
    }

// END CONSTRUCTOR

    /**
      Singleton Declaration
     * */
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new Database();
        }

        return self::$instance;
    }

// END SINGLETON DECARATION

    /**
      Connect and select database
     * */
    public function connect($new_link = false) {
        $this->link_id = @mysql_connect($this->server, $this->user, $this->pass, $new_link);

        if (!$this->link_id) {
            $this->oops("Could not connect to the MySQL Database Server: <b>$this->server</b>.");
        }

        if (!@mysql_select_db($this->database, $this->link_id)) {
            $this->oops("Could not open database: <b>$this->database</b>.");
        }

        // Reset connection data so it cannot be dumped
        $this->server = "";
        $this->user = "";
        $this->pass = "";
        $this->database = "";
    }

// END CONNECTION

    /**
      Close the connection to the database
     * */
    public function close() {
        if (!@mysql_close($this->link_id)) {
            $this->oops("Connection close failed!");
        }
    }

// END CLOSE

    /**
      Escapes characters to be mysql ready
     * */
    public function escape($string) {
        if (get_magic_quotes_runtime())
            $string = stripslashes($string);
        return @mysql_real_escape_string($string, $this->link_id);
    }

// END ESCAPE

    /**
      Executes a SQL query to an open connection
     * */
    public function query($sql) {
        $this->query_id = @mysql_query($sql, $this->link_id);

        if (!$this->query_id) {
            $this->oops("<b>MySQL Query Failed:</b> $sql");
            return 0;
        }

        $this->affected_rows = @mysql_affected_rows($this->link_id);

        return $this->query_id;
    }

// END QUERY

    public function insert_id() {
        return mysql_insert_id($this->link_id);
    }

    /**
      Execute a SQL Query and returns only the first row, frees resultset
     * */
    public function query_first($sql) {
        $query_id = $this->query($sql);
        $out = $this->fetch($query_id);
        $this->free_result($query_id);
        return $out;
    }

// END FETCH SINGLE

    /**
      Execute a SQL Query and returns results one line at a time
     * */
    public function fetch($query_id = -1) {
        if ($query_id != -1) {
            $this->query_id = $query_id;
        }

        if (isset($this->query_id)) {
            $record = @mysql_fetch_assoc($this->query_id);
        } else {
            $this->oops("Invalid query_id: <b>$this->query_id</b>. Records could not be fetched.");
        }

        return $record;
    }

// END FETCH

    /**
      Return all results in array format
     * */
    public function fetch_array($sql) {
        $query_id = $this->query($sql);
        $out = array();

        while ($row = $this->fetch($query_id)) {
            $out [] = $row;
        }

        $this->free_result($query_id);
        return $out;
    }

// END FETCH ARRAY

    /**
      Execute an update query with an array
     * */
    public function update($table, $data, $where = '1') {
        $q = "UPDATE `$table` SET ";

        foreach ($data as $key => $val) {
            if (strtolower($val) == 'null')
                $q .= "`$key` = NULL, ";
            elseif (strtolower($val) == 'now()')
                $q .= "`$key` = NOW(), ";
            elseif (preg_match("/^increment\((\-?\d+)\)$/i", $val, $m))
                $q.= "`$key` = `$key` + $m[1], ";
            else
                $q.= "`$key`='" . $this->escape($val) . "', ";
        }

        $q = rtrim($q, ', ') . ' WHERE ' . $where . ';';
        return $this->query($q);
    }

// END UPDATE

    /**
      Execute an insert query with an array
     * */
    public function insert($table, $data) {
        $q = "INSERT INTO `$table` ";
        $v = '';
        $n = '';

        foreach ($data as $key => $val) {
            $n.="`$key`, ";
            if (strtolower($val) == 'null')
                $v.="NULL, ";
            elseif (strtolower($val) == 'now()')
                $v.="NOW(), ";
            else
                $v.= "'" . $this->escape($val) . "', ";
        }

        $q .= "(" . rtrim($n, ', ') . ") VALUES (" . rtrim($v, ', ') . ");";

        if ($this->query($q)) {
            return mysql_insert_id($this->link_id);
        } else
            return false;
    }

// END INSERT

    public function replace($table, $fields, $data) {
        $v = '';
        $n = '';

        foreach ($data as $key => $val) {
            $v.='(';

            foreach ($val as $chiave => $valore) {

                if (strtolower($valore) == 'null')
                    $v.="NULL,";
                elseif (strtolower($valore) == 'now()')
                    $v.="NOW(),";
                else
                    $v.= "'" . $this->escape($valore) . "',";
            }
            $v = substr($v, 0, -1); //remove last comma - $v too long to use rtrim
            $v.='),';
        }

        $v = substr($v, 0, -1); //remove last commma - $v too long to use rtrim

        foreach ($fields as $field) {
            $n.="`$field`,";
        }

        $q .= "REPLACE INTO `$table` (" . rtrim($n, ',') . ") VALUES " . $v . ";";

        if ($this->query($q)) {
            return mysql_insert_id($this->link_id);
        } else
            return false;
    }

    public function getUserID($username) {
        $sql = "SELECT
                tblUser.idUser
                FROM tblUser
                WHERE tblUser.Login='" . $username . "'";
        $out = $this->fetch_array($sql);
        return $out[0]['idUser'];
    }

    public function getSynopsisPredicted($filename) {
        $sql = "SELECT tblSynopsis.synopsis_predicted
                FROM tblSynopsis
                WHERE tblSynopsis.transcriptionFilename ='" . $filename . "'";
        $out = $this->fetch_array($sql);
        return $out[0]['synopsis_predicted'];
    }

    public function get_extrinsic_questionnaire($service, $scenario, $condition = null) {
        $sql = "SELECT * 
                FROM extrinsic_question
                WHERE scenario='" . $scenario . "' 
                    and service='" . $service . "'";
        $out = $this->fetch_array($sql);
        return $out;
    }

    public function get_extrinsic_conversations($collection, $service, $scenario, $condition) {
        $sql = "select * from view_conversation_collection
                where idextrinsic_collection=" . $collection . " 
                and service='" . $service . "' and scenario='" . $scenario . "'";
        if ($condition != null) {
            $sql.= " and extr_condition='" . $condition . "'";
        }
        $out = $this->fetch_array($sql);
        return $out;
    }

    public function get_extrinsic_conversation($id) {
        $sql = "SELECT * FROM view_conversation_collection
            where idextrinsic_conversation=" . $id;
        $out = $this->fetch_array($sql);
        return $out;
    }

    public function get_extrinsic_criteria($id) {
        $sql = "SELECT * FROM extrinsic_conversation_criteria
                where type='criteria' and idextrinsic_conversation=" . $id;
        $out = $this->fetch_array($sql);
        return $out;
    }

    public function get_extrinsic_template($id) {
        $sql = "SELECT * FROM extrinsic_template
                where  idextrinsic_conversation=" . $id;
        $out = $this->fetch_array($sql);
        return $out;
    }

    public function get_all_conversations() {
        $sql = "select * FROM extrinsic_conversation";

        $out = $this->fetch_array($sql);
        return $out;
    }

    /*
      public function get_conversation_criteria($sql) {
      $query = " SELECT DISTINCT a.* FROM   view_conversation_collection a
      left outer JOIN (SELECT a.idextrinsic_conversation,
      Max(caller_polarity)     caller_polarity,
      Max(polarity)     polarity,
      Max(politness)           politness,
      Max(conversation_temper) conversation_temper,
      Max(acof)                acof
      FROM   (SELECT b.idextrinsic_conversation,
      CASE
      WHEN b.name = 'caller_polarity' THEN b.value
      ELSE ''
      end caller_polarity,
      CASE
      WHEN b.name = 'polarity' THEN b.value
      ELSE ''
      end polarity,
      CASE
      WHEN b.name = 'politness' THEN b.value
      ELSE ''
      end politness,
      CASE
      WHEN b.name = 'conversation_temper' THEN
      b.value
      ELSE ''
      end conversation_temper,
      CASE
      WHEN b.name = 'acof' THEN b.value
      ELSE ''
      end acof
      FROM   extrinsic_conversation_criteria b
      WHERE  b.name = 'caller_polarity'
      OR b.name = 'polarity'
      OR b.name = 'politness'
      OR b.name = 'conversation_temper'
      OR b.name = 'acof') a
      GROUP  BY a.idextrinsic_conversation) c
      ON a.idextrinsic_conversation = c.idextrinsic_conversation where ";
      $query.=$sql;
      $out = $this->fetch_array($query);
      return $out;
      } */

    public function get_all_conversation_criteria($service, $sql) {
        $query = "";
        if ($service == 'DECODA') {
            $query = " 
                        SELECT DISTINCT a.* FROM   view_all_conversation_collection a
       left outer JOIN (SELECT a.idextrinsic_conversation,
                          Max(caller_polarity)     caller_polarity,
                          Max(polarity)     polarity,
                          Max(politness)           politness,
                          Max(conversation_temper) conversation_temper,
                          Max(acof)                acof
                   FROM   (SELECT b.idextrinsic_conversation,
                                  CASE
                                    WHEN b.name = 'caller_polarity' THEN b.value
                                    ELSE ''
                                  end caller_polarity,
                     CASE
                                    WHEN b.name = 'polarity' THEN b.value
                                    ELSE ''
                                  end polarity,
                                  CASE
                                    WHEN b.name = 'politness' THEN b.value
                                    ELSE ''
                                  end politness,
                                  CASE
                                    WHEN b.name = 'conversation_temper' THEN
                                    b.value
                                    ELSE ''
                                  end conversation_temper,
                                  CASE
                                    WHEN b.name = 'acof' THEN b.value
                                    ELSE ''
                                  end acof
                           FROM   extrinsic_conversation_criteria b
                           WHERE  b.name = 'caller_polarity'
                                   OR b.name = 'polarity'
                                   OR b.name = 'politness'
                                   OR b.name = 'conversation_temper'
                                   OR b.name = 'acof') a
                   GROUP  BY a.idextrinsic_conversation) c
               ON a.idextrinsic_conversation = c.idextrinsic_conversation
    where a.service='" . $service . "' and ";
        } else {
            $query = " 
                        SELECT DISTINCT a.*
                        FROM view_all_conversation_collection a
                        LEFT OUTER JOIN
                          (SELECT a.idextrinsic_conversation,
                                  Max(agent_empathy) agent_empathy,
                                  Max(polarity) polarity,
                                  Max(client_satisfaction) client_satisfaction,
                                  Max(dialog_polarity_percent) dialog_polarity_percent,
                                  Max(agent_polarity_percent) agent_polarity_percent,
                                  Max(agent_polarity) agent_polarity,
                                  Max(caller_polarity_percent) caller_polarity_percent,
                                  Max(caller_polarity) caller_polarity
                           FROM
                             (SELECT b.idextrinsic_conversation,
                                     CASE WHEN b.name = 'agent_empathy' THEN b.value ELSE '' END agent_empathy,
                                     CASE WHEN b.name = 'polarity' THEN b.value ELSE '' END polarity,
                                     CASE WHEN b.name = 'client_satisfaction' THEN b.value ELSE '' END client_satisfaction,
                                     CASE WHEN b.name = 'dialog_polarity_percent' THEN b.value ELSE '' END dialog_polarity_percent,
                                     CASE WHEN b.name = 'agent_polarity_percent' THEN b.value ELSE '' END agent_polarity_percent,
                                     CASE WHEN b.name = 'agent_polarity' THEN b.value ELSE '' END agent_polarity,
                                     CASE WHEN b.name = 'caller_polarity_percent' THEN b.value ELSE '' END caller_polarity_percent,
                                     CASE WHEN b.name = 'caller_polarity' THEN b.value ELSE '' END caller_polarity
                              FROM extrinsic_conversation_criteria b
                        WHERE b.name = 'agent_empathy'
                        OR b.name = 'polarity'
                        OR b.name = 'client_satisfaction'
                        OR b.name = 'dialog_polarity_percent'
                        OR b.name = 'agent_polarity_percent'
                        OR b.name = 'agent_polarity'
                        OR b.name = 'caller_polarity_percent'
                        OR b.name = 'caller_polarity') a
                           GROUP BY a.idextrinsic_conversation) c ON a.idextrinsic_conversation = c.idextrinsic_conversation ";
        }
        $query.=$sql;
        $out = $this->fetch_array($query);
        return $out;
    }

    public function getidsconversationtask1(
    $service, $scenario) {
        $sql = "";
        if ($service == 'DECODA') {
            $sql = "select distinct idextrinsic_conversation FROM view_conversation_collection
  where filename in ";
            if ($scenario == '1') {
                $sql = $sql . " ('20091112_RATP_SCD_0393',
                                '20091112_RATP_SCD_1310',
                                '20101206_RATP_SCD_0163',
                                '20091112_RATP_SCD_0148',
                                '20091112_RATP_SCD_1150',
                                '20101206_RATP_SCD_0031',
                                '20091112_RATP_SCD_0582',
                                '20101206_RATP_SCD_0410',
                                '20091112_RATP_SCD_1297',
                                '20091112_RATP_SCD_0060')";
            } else {
                $sql = $sql . " ('20091112_RATP_SCD_0010',
                                '20091112_RATP_SCD_0061',
                                '20091112_RATP_SCD_0177',
                                '20091112_RATP_SCD_0165',
                                '20091112_RATP_SCD_0378',
                                '20091112_RATP_SCD_0379',
                                '20091112_RATP_SCD_0277',
                                '20091112_RATP_SCD_0453',
                                '20091112_RATP_SCD_0116',
                                '20091112_RATP_SCD_0217') ";
            }
            $sql=$sql." order by filename ";
        } else {
            $sql = "  select idextrinsic_conversation FROM view_conversation_collection
                where service = '" . $service . "' and scenario = '" . $scenario . "' 
                  
                                    limit 10";
        }
        $out = $this->fetch_array($sql);
        return $out;
    }

    /**
      Frees Resultset
     * */
    private function free_result(
    $query_id = -1) {
        if ($query_id != -1) {
            $this->query_id = $query_id;
        }
        if ($this->query_id != 0 && !@mysql_free_result($this->query_id)) {
            $this->oops("Result ID: <b>$this->query_id</b> could not be freed."
            );
        }
    }

// END FREE RESULTSET

    /**
      Throws an Error Message only if debug flag is true
     * */
    private function oops($msg = '') {
        if (!empty($this->link_id)) {
            $this->error = mysql_error($this->link_id);
        } else {
            $this->error = mysql_error();
            $msg = "<b>WARNING:</b> No link_id found. Likely not be connected to database.<br />$msg";
        }

        // if no debug, done here
        if (!$this->debug)
            return;
        ?>
        <table align="center" border="1" cellspacing="0" style="background:white;
               color:black;
               width:80%;
               ">
            <tr><th colspan=2>Database Error</th></tr>
            <tr><td align="right" valign="top">Message:</td><td><?php echo $msg; ?></td></tr>
            <?php
            if (!empty($this->error))
                echo '<tr><td align="right" valign="top" nowrap>MySQL Error:</td><td>' . $this->error . '</td></tr>';
            ?>
            <tr><td align="right">Date:</td><td><?php echo date("l, F j, Y \a\\t g:i:s A"); ?></td></tr>
            <?php
            if (!empty($_SERVER['REQUEST_URI']))
                echo '<tr><td align="right">Script:</td><td><a href="' . $_SERVER['REQUEST_URI'] . '">' . $_SERVER['REQUEST_URI'] . '</a></td></tr>';
            ?>
            <?php
            if (!empty($_SERVER['HTTP_REFERER']))
                echo '<tr><td align="right">Referer:</td><td><a href="' . $_SERVER['HTTP_REFERER'] . '">' . $_SERVER['HTTP_REFERER'] . '</a></td></tr>';
            ?>
        </table>
        <?php
    }

// END OOPS
}
?>