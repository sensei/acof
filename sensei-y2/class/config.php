<?php
	
/**
     
    USAGE: $config = Config::get_instance();
	NEED: db.php - log.php
     
**/

class Config {

	// Store the single instance of the object
    private static $instance ;
	
	/**
        Constructor
    **/
    private function __construct() {
    }// END CONSTRUCTOR
	
	/**
        Singleton Declaration
    **/
    public static function get_instance() {
        if( !self::$instance) {
            self::$instance = new Config () ;    
        }
		
        return self::$instance ;
    }// END SINGLETON DECARATION
	
	public static function get_ini_value($section, $param) {
        $ini=parse_ini_file("./include/config.ini", true);
        return $ini[$section][$param];;
    }	
}

?>