<?php

/**

  USAGE: $report = Report::get_instance();
  NEED: db.php - config.php - lang.php - acof.php - elastic.php

 * */
class Report {

    // Store the single instance of the object
    private static $instance;

    /**
      Constructor
     * */
    private function __construct() {
        $this->config = Config::get_instance();
        $this->db = Database::get_instance();
        $this->lang = Lang::get_instance();
        $this->acof = Acof::get_instance();
        $this->elastic = Elastic::get_instance();
    }

// END CONSTRUCTOR

    /**
      Singleton Declaration
     * */
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new Report ();
        }

        return self::$instance;
    }

// END SINGLETON DECARATION

    public function show_search_form($post, $session) {
        $db = $this->db;
        $config = $this->config;
        $lang = $this->lang;

        $selected = array("sysdate" => "", "sysdatemod" => "", "PASS" => "", "FAIL" => "", "NA" => "");
        if ($post["typedate"] == "sysdate")
            $selected["sysdate"] = "selected";
        else if ($post["typedate"] == "sysdatemod")
            $selected["sysdatemod"] = "selected";

        $option = array();
        /*         * * */
        $query = "SELECT distinct sysuser FROM tblListen order by sysuser";
        $arrTemp = array();
        $arrReturn = array();

        $arrTemp = $db->fetch_array($query);
        foreach ($arrTemp as $k => $v) {
            $string = '';
            foreach ($v as $a => $b)
                $string .= $b;
            $arrReturn[$k] = $string;
        }

        if (count($arrReturn) > 0) {
            reset($arrReturn);
            while (list($key, $value) = each($arrReturn)) {
                $option["sysuser"].= '<option value="' . $value . '"' . ((isset($post['sysuser']) && $post['sysuser'] == $value) ? ' selected="selected"' : '') . '>' . $value . '</option>';
            }
        }

        /*         * * */
        $query = "SELECT distinct service FROM tblListen";
        $arrTemp = array();
        $arrReturn = array();

        $arrTemp = $db->fetch_array($query);
        foreach ($arrTemp as $k => $v) {
            $string = '';
            foreach ($v as $a => $b)
                $string .= $b;
            $arrReturn[$k] = $string;
        }

        if (count($arrReturn) > 0) {
            reset($arrReturn);
            while (list($key, $value) = each($arrReturn)) {
                $option["service"].= '<option value="' . $value . '"' . ((isset($post['service']) && $post['service'] == $value) ? ' selected="selected"' : '') . '>' . $value . '</option>';
            }
        }

        /*         * * */
        $query = "SELECT idsubitem, Description" . $session['language'] . " as SubItem FROM tblSubItem ";
        if ($post['service'] != '') {
            $query .= " where Flag" . $post['service'] . " = 1";
        }
        $query .= " order by idsubitem";
        $arrTemp = array();
        $arrReturn = array();

        $arrTemp = $db->fetch_array($query);
        foreach ($arrTemp as $k => $v) {
            $string = '';
            foreach ($v as $a => $b)
                $string .= $b . '|';
            $arrReturn[$k] = $string;
        }

        if (count($arrReturn) > 0) {
            reset($arrReturn);
            while (list($key, $value) = each($arrReturn)) {
                $arrVal = explode("|", $value);
                $arrPost = explode("|", $post['subitem']);
                $option["subitem"].= '<option value="' . $value . '"' . ((isset($post['subitem']) && $arrPost[0] == $arrVal[0]) ? ' selected="selected"' : '') . '>' . $arrVal[0] . ' - ' . $arrVal[1] . '</option>';
            }
        }

        /*         * * */
        $selected[$post["scoresubitem"]] = "selected";

        $score = "Score";
        $score.='<select class="form-control" style="width: 100%;"  id="scoresubitem" name="scoresubitem">
                <option value=""/>
                <option value="PASS" ' . $selected["PASS"] . '>PASS</option>
                <option value="FAIL" ' . $selected["FAIL"] . '>FAIL</option>
                <option value="NA" ' . $selected["NA"] . '>NA</option>
        </select>';

        //set checked on Show columns chechboxes
        $showcol_checked = array("Comment" => "", "Synopsis" => "", "Monit. Start Date" => "", "Monit. End Date" => "");
        foreach ($post['showcol'] as $k => $v)
            $showcol_checked[$k] = 'checked="checked"';

        //show compair with chechboxes
        $arrTemp = $db->fetch_array("SELECT IdSubItem FROM `tblSubItem` ORDER BY IdSubItem ASC");
        foreach ($arrTemp as $k => $v) {
            foreach ($v as $a => $b) {
                if (isset($post['comparewith' + $b]))
                    $checked = 'checked="checked"';
                else
                    $checked = "";
                $comparewith.='<input ' . $checked . ' type="checkbox" id="comparewith[' . $b . ']" name="comparewith[' . $b . ']" value="' . $b . '"/> SubItem ' . $b . '</input><br/>';
            }
        }
        $comparewith_count = $b;

        $show_search_form = '<table class="table table-responsive " style="width: 100%;">
            <tr>
                <td>' . $lang->get_language($session["username"], 'LabDateReport', $session["language"]) . '
                    <div align="left" style="border-style: none;">                    
                        <input style="width: 70%;" class="form-control" value="' . $post["datesel"] . '" ID="datesel" name="datesel" ReadOnly="False" BorderStyle="None" BackColor="#F4F4F4" Font-Bold="True" Font-Size="Small"/>									
                    </div>
                </td>
                <td>' . str_replace(' ', '&nbsp;', $lang->get_language($session["username"], 'LabTypeDate', $session["language"])) . '
                    <select class="form-control" style="width: 100%;" id="typedate" name="typedate" onchange="javascript:submit();">
                        <option value="sysdate" ' . $selected["sysdate"] . '>Insert Date</option>
                        <option value="sysdatemod" ' . $selected["sysdatemod"] . '>Modify Date</option>
                    </select>							
                </td>	
                <td></td><td></td>
            </tr> 
            <tr>
                <td>
                    User
                    <select class="form-control" style="width: 100%;" id="sysuser" name="sysuser" class="form-action" onchange="javascript:submit();">
                        <option value=""/></option>
                        ' . $option["sysuser"] . '
                    </select>
                </td>
                <td>	
                    ' . $lang->get_language($session["username"], 'LabTypeService', $session["language"]) . '
                    <select class="form-control" style="width: 100%;" id="service" name="service" onchange="javascript:submit();">
                        <option value=""></option>
                        ' . $option["service"] . '
                    </select>
                </td>
                <td></td><td></td>
            </tr>
            <tr>
                <td>SubItem
                    <select class="form-control" style="width: 100%;" id="subitem" name="subitem" class="form-action" onchange="javascript:changeSubItem(this.value, ' . $comparewith_count . ');">
                        <option value=""></option>
                        <option value="-1" ' . ((isset($post['subitem']) && $post['subitem'] < 0) ? ' selected="selected"' : '') . '>' . $lang->get_language($session["username"], 'LabSubItemAll', $session["language"]) . '</option>
                        ' . $option["subitem"] . '
                    </select>								
                </td>
                <td>' . $score . '</td>
                <td></td><td></td>
            </tr>						
            <tr>
                <td>Conversation Name
                    <br />
                    <input class="form-control" style="width: 100%;" BorderStyle="None" BackColor="#F4F4F4" Font-Bold="True" Font-Size="Small" type="text" id="txtFile" name="txtFile" value="' . $post['txtFile'] . '"/>
                </td>
                <td></td><td></td><td></td>
            </tr>
            <tr>
                <td nowrap>
                    ' . $lang->get_language($session["username"], 'LabShowCol', $session["language"]) . '<br />
                    <input ' . $showcol_checked["Comment"] . ' type="checkbox" name="showcol[Comment]" value=", list.Comment"/> 
                            ' . $lang->get_language($session["username"], 'LabConsListen', $session["language"]) . '
                    </input><br/>
                    <input  ' . $showcol_checked["Synopsis"] . ' type="checkbox" name="showcol[Synopsis]" value=", list.SynopsisNew"/> Synopsis</input><br/>
                    <input ' . $showcol_checked["Monit. Start Date"] . ' type="checkbox" name="showcol[Monit. Start Date]" value=", DATE_FORMAT(list.StartDate, \'%d/%m/%Y %H:%i\')StartDate"/> Monit.&nbsp;Start&nbsp;Date</input><br/>
                    <input ' . $showcol_checked["Monit. End Date"] . ' type="checkbox" name="showcol[Monit. End Date]" value=", DATE_FORMAT(list.EndDate, \'%d/%m/%Y %H:%i\')EndDate"/> Monit.&nbsp;End&nbsp;Date</input>
                </td>
                <td nowrap>
                    <br />' . $comparewith . '
                </td>
                <td></td><td></td>
            </tr>
            <tr>
                <td>
                    <button type="button" onclick="javascript:document.form1.flagxls.value=0;submit();" class="btn btn-sm btn-info">
                        ' . $lang->get_language($session["username"], 'LabSearchReport', $session["language"]) . '
                    </button>
                </td>
                <td>
                    <button type="button" onclick="javascript:document.form1.flagxls.value=1;submit();" class="btn btn-sm btn-success">
                        ' . $lang->get_language($session["username"], 'LabExcelReport', $session["language"]) . '
                    </button>						
                </td>
                <td></td><td></td>
            </tr>
        </table>';
        return $show_search_form;
    }

    public function show_es_search_form($post, $session) {
        $db = $this->db;
        $config = $this->config;
        $lang = $this->lang;

        $elastic = $this->elastic;
        $client = $elastic->connect();
        $type = $elastic->type;
        $count = $elastic->count;

        $selected = array("sysdate" => "", "sysdatemod" => "");
        if ($post["typedate"] == "sysdate")
            $selected["sysdate"] = "selected";
        else if ($post["typedate"] == "sysdatemod")
            $selected["sysdatemod"] = "selected";

        $option = array();

        /** create User combobox * */
        $bodysearch['size'] = 0;
        $bodysearch['aggs']['Annotator']['terms']['field'] = "Annotator";
        $dataset = $elastic->search($client, $bodysearch, $type, $count);
        $idUser = $dataset['aggregations']['Annotator']['buckets'];
        if (count($idUser) > 0) {
            $where = "WHERE ";
            foreach ($idUser as $k => $v)
                $where.="Login='" . $v['key'] . "' OR ";
        } else
            $where = "";
        $query = "SELECT * FROM tblUser " . substr($where, 0, strlen($where) - 3) . " ORDER BY Cognome ASC";
        $arrTemp = array();
        $arrReturn = array();

        $arrReturn = $db->fetch_array($query);

        foreach ($arrReturn as $k => $v) {
            $option["sysuser"].= '<option value="' . $arrReturn[$k]['Login'] . '"' . ((isset($post['sysuser']) && $post['sysuser'] == $arrReturn[$k]['Login']) ? ' selected="selected"' : '') . '>' . $arrReturn[$k]['Cognome'] . ' ' . $arrReturn[$k]['Nome'] . '</option>';
        }

        /** create Service combobox * */
        $bodysearch['size'] = 0;
        $bodysearch['aggs']['service']['terms']['field'] = "Service";
        $dataset = $elastic->search($client, $bodysearch, $type, $count);
        $service = $dataset['aggregations']['service']['buckets'];
        foreach ($service as $k => $v)
            $option["service"].= '<option value="' . $v['key'] . '" ' . ((isset($post['service']) && $post['service'] == $v['key']) ? ' selected="selected"' : '') . '>' . $v['key'] . '</option>';

        /** create SubItem comboboxes* */
        $query = "SELECT idsubitem, Description" . $session['language'] . " as SubItem FROM tblSubItem ";
        if ($post['service'] != '')
            $query .= " where Flag" . $post['service'] . " = 1";
        $query .= " order by idsubitem";
        $arrTemp = array();
        $arrReturn = array();

        $arrTemp = $db->fetch_array($query);
        foreach ($arrTemp as $k => $v) {
            $string = '';
            foreach ($v as $a => $b)
                $string .= $b . '|';
            $arrReturn[$k] = $string;
        }

        if (count($arrReturn) > 0) {
            reset($arrReturn);
            while (list($key, $value) = each($arrReturn)) {
                $arrVal = explode("|", $value);
                $arrPost = explode("|", $post['subitem']);
                //$option["subitem"].= '<option value="'.$value.'"'.((isset($post['subitem']) && $arrPost[0]==$arrVal[0])?' selected="selected"':'').'>'.$arrVal[1].'</option>';
                $subitem.='<tr>
                        <td>' . $arrVal[0] . ' - ' . $arrVal[1] . '</td>
                        <td>
                            <select onchange="checkShowSubItem(this)" class="form-control" style="width: 100%;"  id="ScoreQuestion' . $arrVal[0] . '" name="ScoreQuestion' . $arrVal[0] . '">
                                <option value=""/>
                                <option value="PASS" ' . (($post['ScoreQuestion' . $arrVal[0]] == "PASS") ? ' selected="selected"' : '') . '>PASS</option>
                                <option value="FAIL" ' . (($post['ScoreQuestion' . $arrVal[0]] == "FAIL") ? ' selected="selected"' : '') . '>FAIL</option>
                                <option value="NA" ' . (($post['ScoreQuestion' . $arrVal[0]] == "NA") ? ' selected="selected"' : '') . '>NA</option>
                            </select>
                        </td>
                        <td align="center">
                            <input ' . ((isset($post['showsubitem'][$arrVal[0]])) ? ' checked="checked"' : '') . ' type="checkbox" id="showsubitem[' . $arrVal[0] . ']" name="showsubitem[' . $arrVal[0] . ']" value="SubItem ' . $arrVal[0] . '"/></input>
                        </td>
                    </tr>';
            }
        }

        $LabConsListen = $lang->get_language($session['username'], 'LabConsListen', $session['language']);

        //set checked on Show columns chechboxes
        $showcol_checked = array("Comment" => "", "Synopsis" => "", "Synopsis_Predicted" => "", "Monit. Start Date" => "", "Monit. End Date" => "");
        foreach ($post['showcol'] as $k => $v)
            $showcol_checked[$k] = 'checked="checked"';

        $show_search_form = '<table class="table table-responsive" style="width: 100%;">
            <tr>
                <td width="50%">' . $lang->get_language($session["username"], 'LabDateReport', $session["language"]) . '
                    <div align="left" style="border-style: none;">                    
                        <input style="width: 70%;" class="form-control" value="' . $post["datesel"] . '" ID="datesel" name="datesel" ReadOnly="False" BorderStyle="None" BackColor="#F4F4F4" Font-Bold="True" Font-Size="Small"/>									
                    </div>
                </td>
                <td  width="50%">' . str_replace(' ', '&nbsp;', $lang->get_language($session["username"], 'LabTypeDate', $session["language"])) . '
                    <select class="form-control" style="width: 100%;" id="typedate" name="typedate">
                        <option value="sysdate" ' . $selected["sysdate"] . '>Insert Date</option>
                        <option value="sysdatemod" ' . $selected["sysdatemod"] . '>Modify Date</option>
                    </select>							
                </td>  
            </tr>
            <tr>
                <td>
                    User
                    <select class="form-control" style="width: 100%;" id="sysuser" name="sysuser" class="form-action">
                        <option value=""/></option>
                        ' . $option["sysuser"] . '
                    </select>
                </td>
                <td>	
                    ' . $lang->get_language($session["username"], 'LabTypeService', $session["language"]) . '
                    <select class="form-control" style="width: 100%;" id="service" name="service">
                        <option value=""></option>
                        ' . $option["service"] . '
                    </select>
                </td>
            </tr>
            <tr>
                <td>Conversation Name
                    <br />
                    <input class="form-control" style="width: 100%;" BorderStyle="None" BackColor="#F4F4F4" Font-Bold="True" Font-Size="Small" type="text" id="txtFile" name="txtFile" value="' . $post['txtFile'] . '"/>
                </td>
                <td>Note
                    <br />
                    <input class="form-control" style="width: 100%;" BorderStyle="None" BackColor="#F4F4F4" Font-Bold="True" Font-Size="Small" type="text" id="note" name="note" value="' . $post['note'] . '"/>
                </td>
            </tr>
            <tr>
                <td>' . $LabConsListen . '
                    <br />
                    <input onkeyup="checkShowColumn(this,\'showcol\\\\[Comment\\\\]\')" class="form-control" style="width: 100%;" BorderStyle="None" BackColor="#F4F4F4" Font-Bold="True" Font-Size="Small" type="text" id="Comment" name="Comment" value="' . $post['Comment'] . '"/>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Synopsis
                    <br />
                    <input onkeyup="checkShowColumn(this,\'showcol\\\\[Synopsis\\\\]\')" class="form-control" style="width: 100%;" BorderStyle="None" BackColor="#F4F4F4" Font-Bold="True" Font-Size="Small" type="text" id="Synopsis" name="Synopsis" value="' . $post['Synopsis'] . '"/>
                </td>
                <td>Synopsis Predicted
                    <br />
                    <input onkeyup="checkShowColumn(this,\'showcol\\\\[Synopsis_Predicted\\\\]\')" class="form-control" style="width: 100%;" BorderStyle="None" BackColor="#F4F4F4" Font-Bold="True" Font-Size="Small" type="text" id="Synopsis_Predicted" name="Synopsis_Predicted" value="' . $post['Synopsis_Predicted'] . '"/>
                </td>
            </tr>
            <tr>
                <td nowrap colspan="4">
                    <input ' . $showcol_checked["Comment"] . ' type="checkbox" name="showcol[Comment]" id="showcol[Comment]" value="Comment"/> 
                            ' . $lang->get_language($session["username"], 'LabConsListen', $session["language"]) . '
                    </input>&nbsp;
                    <input ' . $showcol_checked["Synopsis"] . ' type="checkbox" name="showcol[Synopsis]" id="showcol[Synopsis]" value="Synopsis"/> Synopsis</input>&nbsp;
                        <input ' . $showcol_checked["Synopsis_Predicted"] . ' type="checkbox" name="showcol[Synopsis_Predicted]" id="showcol[Synopsis_Predicted]" value="Synopsis_Predicted"/> Synopsis Predicted</input>&nbsp;
                    <input ' . $showcol_checked["Monit. Start Date"] . ' type="checkbox" name="showcol[Monit. Start Date]" id="showcol[Monit. Start Date]" value="StartDate"/> Monit.&nbsp;Start&nbsp;Date</input>&nbsp;
                    <input ' . $showcol_checked["Monit. End Date"] . ' type="checkbox" name="showcol[Monit. End Date]" id="showcol[Monit. End Date]" value="EndDate"/> Monit.&nbsp;End&nbsp;Date</input>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table class="table-responsive" style="width: 100%;">
                        <tr>
                            <td>SubItem</td>
                            <td>Score</td>
                            <td align="center">' . $lang->get_language($session["username"], 'LabShowCol', $session["language"]) . '</td>
                        </tr>
                        ' . $subitem . '
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="button" onclick="javascript:document.form1.flagxls.value=0;submit();" class="btn btn-sm btn-info">
                        ' . $lang->get_language($session["username"], 'LabSearchReport', $session["language"]) . '
                    </button>
                    &nbsp;<input ' . ((isset($post['show_es_query'])) ? ' checked="checked"' : '') . ' type="checkbox" name="show_es_query" id="show_es_query"/> Elasticsearch query</input>
                </td>
                <td align="right">
                <button type="button" onclick="resetEsForm(\'form1\');" class="btn btn-sm btn-warning">
                        Reset
                    </button>		
                    					
                </td>
            </tr>
        </table>';
        return $show_search_form;
    }

    public function show_result($post, $session) {
        $db = $this->db;
        $config = $this->config;
        $lang = $this->lang;
        $acof = $this->acof;

        $show_search_form = '';

        $query = "SELECT list.idListen,list.FileName";
        foreach ($post['showcol'] as $k => $v)
            $query .= $v;

        $query .= " ,list.Service,list.Score,list.sysuser,DATE_FORMAT(list.sysdate, '%d/%m/%Y&nbsp;%H:%i')sysdate, DATE_FORMAT(list.sysdatemod, '%d/%m/%Y&nbsp;%H:%i')sysdatemod ";
        $query .= " FROM tblListen list";

        if ($post["subitem"] != '')
            $query .= " inner join tblListenScore score on list.idListen=score.idListen";
        $query .= "  where 1=1";

        if ($post["subitem"] != '') {
            $arrPost = explode("|", $post['subitem']);
            if ($post["subitem"] > 0)
                $query .= " and score.idsubitem = " . $arrPost[0];
            if ($post["scoresubitem"] != '')
                $query .= " and score.Score = '" . $post["scoresubitem"] . "'";
        }

        if ($post["datesel"] != '') {
            //if the date is range
            if (strpos($post["datesel"], "-") > 0) {
                $arrDate = explode(' - ', $post["datesel"]);
                $from = $arrDate[0];
                $to = $arrDate[1];
                $dateselFrom = $acof->conv_date($from);
                $dateselTo = $acof->conv_date($to);
                $query .= " and DATE_FORMAT(list." . $post["typedate"] . ", '%Y%m%d') >= '" . $dateselFrom . "'";
                $query .= " and DATE_FORMAT(list." . $post["typedate"] . ", '%Y%m%d') <= '" . $dateselTo . "'";
            } else { //if date is a single date
                $datesel = $acof->conv_date($post["datesel"]);
                $query .= " and DATE_FORMAT(list." . $post["typedate"] . ", '%Y%m%d') = '" . $datesel . "'";
            }
        }

        if ($post['txtFile'] != '')
            $query .= " and list.filename like '%" . $post['txtFile'] . "%'";
        if ($post['sysuser'] != '')
            $query .= " and list.sysuser='" . $post['sysuser'] . "'";
        if ($post['service'] != '')
            $query .= " and list.service='" . $post['service'] . "'";
        $query .= " order by list.filename ";

        $arrTemp = array();
        $arrReturn = array();
        //call the query function
        $arrTemp = $db->fetch_array($query);
        foreach ($arrTemp as $k => $v) {
            $string = '';
            foreach ($v as $a => $b)
                $string .= $b . '|';
            $arrReturn[$k] = $string;
        }

        if (count($arrReturn) > 0) {
            reset($arrReturn);
            $i = 1;

            $showcol = "";
            foreach ($post['showcol'] as $k => $v)
                $showcol.='<td>' . $k . '</td>';
            foreach ($post['comparewith'] as $k => $v)
                $comparewithcol.='<td> SubItem ' . $k . '</td>';

            $show_search_form.=$query . '<table class="table table-bordered" style="font-size:9pt;">
                <thead>
                    <tr>
                        <td><span id="nrow" class="badge">' . $lang->get_language($session["username"], 'LabNumRow', $session["language"]) . ' ' . count($arrReturn) . '</span></td>
                        <td>ID</td>
                        <td>Transcription&nbsp;file</td>
                        ' . $showcol . '
                        <td>Service</td>
                        <td>Score&nbsp;%</td>
                        <td>Inserted&nbsp;By</td>                
                        <td>Insert&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Modify&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        ' . $comparewithcol . '
                    </tr>
                </thead>
            <tbody>';

            while (list($key, $value) = each($arrReturn)) {
                $arrRecordValue = explode("|", $value);
                $id = $arrRecordValue[0];

                $row = '<tr onclick="View(\'' . $id . '\');" onmouseover="javascript:this.className=\'highlight\'" onmouseout="javascript:this.className=\'\'">';

                if (($session["username"] == $post['sysuser']) || ($session['flagA'] == 1))
                    $row .='<td align="center"><a href="javascript: goEdit(' . $id . ',\'edit\')">Edit</a></td>';
                else
                    $row .='<td align="center"><a href="javascript: goEdit(' . $id . ',\'view\')">View</a></td>';

                for ($i = 0; $i < count($arrRecordValue) - 1; $i++)
                    $row .= '<td>' . $arrRecordValue[$i] . '</td>';

                foreach ($post['comparewith'] as $k => $v) {
                    $arrTemp = $db->fetch_array("SELECT Score FROM tblListenScore WHERE idSubItem=" . $k . " AND idListen=" . $id);
                    foreach ($arrTemp as $k => $v) {
                        foreach ($v as $a => $b)
                            $row.='<td>' . $b . '</td>';
                    }
                }
                $row.='</tr>';
                $show_search_form.=$row;
            }
            $show_search_form.='</tbody>';
        }
        return $show_search_form;
    }

    public function show_es_result($post, $session) {
        $db = $this->db;
        $lang = $this->lang;

        $elastic = $this->elastic;
        $client = $elastic->connect();
        $type = $elastic->type;
        $count = $elastic->count;

        //foreach ($post as $k => $v)  $show_result.=$k.' = '.$v.'<br/>';
        //prepare $filter
        $i = 0;
        //service filter
        if ($post['service'] != "")
            $filter['bool']['must'][$i++]['term']['Service'] = $post['service'];

        //date filter
        if ($post['datesel'] != "") {
            $datesel = explode("-", $post['datesel']);
            $date_from = $datesel[0];
            $date_from = str_replace('/', '-', $date_from);
            $date_from = date('Y-m-d H:i:s', strtotime($date_from));
            $filter['bool']['must'][$i]['range']['sysdate']['gte'] = $date_from;

            if ($datesel[1] != "") {
                $date_to = $datesel[1];
                $date_to = str_replace('/', '-', $date_to);
                $date_to = date('Y-m-d', strtotime($date_to));
                $filter['bool']['must'][$i]['range'][$post['typedate']]['lte'] = $date_to . " 23:59:59";
            }

            $filter['bool']['must'][$i]['range'][$post['typedate']]['format'] = "yyyy-MM-dd HH:mm:ss";
        }

        //prepare $query
        $i = 0;
        if ($post['sysuser'] != "")
            $query['bool']['must'][$i++]['term']['Annotator'] = $post['sysuser'];
        if ($post['txtFile'] != "")
            $query['bool']['must'][$i++]['term']['Conversation_Name'] = $post['txtFile'];

        //count subitem for following query in note e subitem;
        $subitem_count = $db->query_first("SELECT count(*) as tot FROM tblSubItem");

        //search in every question note
        if ($post['note'] != "") {
            for ($j = 1; $j <= $subitem_count['tot']; $j++) {
                $query['bool']['should'][$j - 1]['match']['Note' . $j] = $post['note'];
            }

            $query['bool']['minimum_should_match'] = 1;
        }

        //query in every question subitem value
        for ($j = 1; $j <= $subitem_count['tot']; $j++) {
            if ($post['ScoreQuestion' . $j] != "")
                $query['bool']['must'][$i++]['term']['ScoreQuestion' . $j] = $post['ScoreQuestion' . $j];
        }

        if ($post['Comment'] != "")
            $query['bool']['must'][$i++]['term']['Comment'] = $post['Comment'];
        if ($post['Synopsis'] != "")
            $query['bool']['must'][$i++]['term']['Synopsis'] = $post['Synopsis'];
        if ($post['Synopsis_Predicted'] != "")
            $query['bool']['must'][$i++]['term']['Synopsis_Predicted'] = $post['Synopsis_Predicted'];

        $bodysearch['query']['filtered'] = array("query" => $query, "filter" => $filter);
        if (isset($post['show_es_query']))
            $elastic->show_es_query($bodysearch);

        //call search method
        $dataset = $elastic->search($client, $bodysearch, $type, $count);
        $count = $dataset['hits']['total'] . ' - ' . $dataset['took'];

        //** load $post and $dataset in $_SESSION for printExcel()**//
        $_SESSION['postdata'] = $post;
        $_SESSION['dataset'] = $dataset;

        //** create showcol th **//
        $showcol = "";
        foreach ($post['showcol'] as $k => $v)
            $showcol.='<td>' . $v . '</td>';

        $showsubitem = "";
        foreach ($post['showsubitem'] as $k => $v)
            $showsubitem.='<td>' . $v . '</td>';

        $show_result.='<table class="table table-bordered" style="font-size:9pt;">
            <thead>
                <tr>
                    <td align="center">
                        <span id="nrow" class="badge">' . $lang->get_language($session["username"], 'LabNumRow', $session["language"]) . ' ' . $count . '</span>
                        <button type="button" onclick="printExcel()" class="btn btn-sm btn-success">' . $lang->get_language($session["username"], 'LabExcelReport', $session["language"]) . '</button>	
                    </td>
                    <td>ID</td>
                    <td>Conversation Name</td>
                    ' . $showcol . '
                    <td>Service</td>
                    <td>Score&nbsp;%</td>
                    <td>Insert&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>Modify&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>Inserted&nbsp;By</td>
                    ' . $showsubitem . '
                </tr>
            </thead>
            <tbody>';

        foreach ($dataset['hits']['hits'] as $k => $v) {
            $show_result.= '<tr>
                <td>&nbsp;</td>
                <td>' . $v['_source']['idListen'] . '</td>
                <td>' . $v['_source']['Conversation_Name'] . '</td>';

            //showcol
            foreach ($post['showcol'] as $a => $b) {
                if (($b === 'StartDate') || ($b === 'EndDate'))
                    $show_result.='<td>' . str_replace(array("T", "Z"), " ", $v['_source'][$b]) . '</td>';
                else {
                    $dest = str_replace($post[$a], '<span class="highlight">' . $post[$a] . '</span>', $v['_source'][$b]);
                    $show_result.='<td>' . $dest . '</td>';
                }
            }

            $show_result.= '<td>' . $v['_source']['Service'] . '</td>
                <td>' . $v['_source']['Scorelisten'] . '</td>
                <td>' . str_replace(array("T", "Z"), " ", $v['_source']['sysdate']) . '</td>
                <td>' . str_replace(array("T", "Z"), " ", $v['_source']['sysdatemod']) . '</td>';

            //show insert by
          
            
            
            $show_result.= '<td>' . $v['_source']['Annotator'] . '</td>';

            //show sunbitem
            foreach ($post['showsubitem'] as $a => $b)
                $show_result.='<td>' . $v['_source']['ScoreQuestion' . $a] . '</td>';

            $show_result.='</tr>';
        }

        $show_result.='</tbody></table>';

        return $show_result;
    }

}

?>