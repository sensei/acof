<?php

/**

  USAGE: $lang = Lang::get_instance();
  NEED: db.php - log.php - acof.php

 * */
class Lang {

    // Store the single instance of the object
    private static $instance;

    /**
      Constructor
     * */
    private function __construct() {
        $this->db = Database::get_instance();
        $this->log = Log::get_instance();
        $this->acof = Acof::get_instance();
    }

// END CONSTRUCTOR

    /**
      Singleton Declaration
     * */
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new Lang ();
        }

        return self::$instance;
    }

// END SINGLETON DECARATION

    public function get_language($username, $LabelName, $FlagLang) {
        $db = $this->db;
        $log = $this->log;
        $acof = $this->acof;

        $msg = '';
        $strLabel = '';

        if ($LabelName != '' && $FlagLang != '') {
            $arrReturn = array();
            $query = "SELECT " . $FlagLang . " FROM tblLanguage WHERE label = '" . $LabelName . "'";
            $arrReturn = $db->fetch_array($query);
            if (count($arrReturn) == 0) {
                $log->ins_log('getLanguage Failed', $username);
                $msg = "<div class='alert alert-danger' role='alert'><strong>Error! </strong>Login failed. Wrong input data.</div>";
                return $msg;
            } else {
                //current language label
                $strLabel = $arrReturn[0][$FlagLang];
                return utf8_encode($acof->checkinput($strLabel));
            }
        }
    }

    public function get_criteria_translation($username, $LabelName, $FlagLang) {
        $db = $this->db;
        $log = $this->log;
        $acof = $this->acof;

        $msg = '';
        $strLabel = '';

        if ($LabelName != '' && $FlagLang != '') {
            $arrReturn = array();
            $query = "SELECT " . $FlagLang . " FROM tblLanguage WHERE label = '" . $LabelName . "'";
            $arrReturn = $db->fetch_array($query);
            if (count($arrReturn) == 0) {

                return $LabelName;
            } else {
                //current language label
                $strLabel = $arrReturn[0][$FlagLang];
                return utf8_encode($acof->checkinput($strLabel));
            }
        }
    }

    public function translate($lang) {
        if ($lang == '')
            $_SESSION['language'] = 'Ita';
        else
            $_SESSION['language'] = $lang;
    }

}

?>