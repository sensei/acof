<?php

/**

  USAGE: $acof = Acof::get_instance();

 * */
class Acof {

    // Store the single instance of the object
    private static $instance;

    /**
      Constructor
     * */
    private function __construct() {
        
    }

// END CONSTRUCTOR

    /**
      Singleton Declaration
     * */
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new Acof ();
        }

        return self::$instance;
    }

// END SINGLETON DECARATION

    public function navbar($lang, $name, $surname) {
        switch ($lang) {
            case 'Ita':
                $changelan = array("Ita" => "", "Eng" => "ChangeLan('Eng')", "Fra" => "ChangeLan('Fra')");
                $opacity = array("Ita" => 0.25, "Eng" => 1, "Fra" => 1);
                $alpha = array("Ita" => 25, "Eng" => 100, "Fra" => 100);
                break;

            case 'Eng':
                $changelan = array("Ita" => "ChangeLan('Ita')", "Eng" => "", "Fra" => "ChangeLan('Fra')");
                $opacity = array("Ita" => 1, "Eng" => 0.25, "Fra" => 1);
                $alpha = array("Ita" => 100, "Eng" => 25, "Fra" => 100);
                break;

            case 'Fra':
                $changelan = array("Ita" => "ChangeLan('Ita')", "Eng" => "ChangeLan('Eng')", "Fra" => "");
                $opacity = array("Ita" => 1, "Eng" => 1, "Fra" => 0.25);
                $alpha = array("Ita" => 100, "Eng" => 100, "Fra" => 25);
                break;

            default:
                $changelan = array("Ita" => "", "Eng" => "", "Fra" => "");
                $opacity = array("Ita" => "", "Eng" => "", "Fra" => "");
                $alpha = array("Ita" => "", "Eng" => "", "Fra" => "");
                break;
        }

        echo '<div class="menu">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="home.php">SENSEI</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Monitoring<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="listen.php">Simple</a></li>
                            <li><a href="listen_prefilled.php">Prefilled</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Extrinsic Evaluation<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">

                            <li><a href="listen_extrinsic.php">Evaluation</a></li>

                            <li><a href="listen_extrinsicquestionnaire.php">User experience questionnaire</a></li>
                          
                        </ul>
                    </li>
                            
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Extrinsic Reports<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">

                           
                            <li><a href="evaluation_grid.php">Evaluation Report task 1</a></li>
                            <li><a href="evaluation_grid_task2.php">Evaluation Report task 2</a></li>
                              <li><a href="evaluation_grid_exp.php">User Experience Report</a></li>
                        </ul>
                    </li>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <!-- li><a href="report.php">Database</a></li -->
                            <li><a href="es_search.php">Elastic Search</a></li>
                            <li><a href="es_predicted.php">Predicted</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Translate<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="#" onclick="' . $changelan['Ita'] . '"><img style="opacity:' . $opacity['Ita'] . ';filter:alpha(opacity=' . $alpha['Ita'] . ');" title="Imposta la lingua italiana" src="img/it.png" />Italiano</a>
                            </li>
                            <li><a href="#" onclick="' . $changelan['Eng'] . '"><img style="opacity:' . $opacity['Eng'] . ';filter:alpha(opacity=' . $alpha['Eng'] . '>);" title="Set the english language" src="img/en.png"/>English</a></li>
                            <li>
                                <a href="#" onclick="' . $changelan['Fra'] . '"><img style="opacity:' . $opacity['Fra'] . ';filter:alpha(opacity=' . $alpha['Fra'] . ');" title="Dfinit la langue fran&#231;aise" src="img/fr.png" />Fran&#231;ais</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="index.php">Logout</a></li>
                    <li><a href="#">' . $name . ' ' . $surname . '</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>';
    }

    public function head_tag($page = '') {
        $meta = '<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="description" content="">
			<meta name="author" content="">
			<meta http-equiv="content-type" content="text/html;charset=utf-8" />';

        $title = '<title>SENSEI Monitoring Tool</title>';

        $link = '<link rel="icon" href="img/sensei.ico" />
			<link href="css/bootstrap.css" rel="stylesheet" />
			<link href="css/bootstrap.min.css" rel="stylesheet">
			<link href="css/starter-template.css" rel="stylesheet">
			<link href="css/sensei.css" rel="stylesheet">';

        $script = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
			<script src="js/bootstrap.min.js"></script>
			<script type="text/javascript" src="js/default.js"></script>';

        switch ($page) {
            case 'index':
                $link.='<link href="css/signin.css" rel="stylesheet">
					<style>.error {color: #FF0000;}</style>';
                break;

            case 'home':
                $link.='<link href="css/charts.css" rel="stylesheet">';

                $script.='<script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
					<script type="text/javascript" src="http://www.amcharts.com/lib/3/pie.js"></script>
					<script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
					<script type="text/javascript" src="http://www.amcharts.com/lib/3/themes/none.js"></script>';
                break;
            case 'predicted':
                $link.='<link rel="stylesheet" href="lib/jqwidgets/styles/jqx.base.css" type="text/css" />';
                $script.='<script type="text/javascript" src="lib/jqwidgets/jqxcore.js"></script>
                          <script type="text/javascript" src="lib/jqwidgets/jqxbuttons.js"></script>
                          <script type="text/javascript" src="lib/jqwidgets/jqxscrollbar.js"></script>
                          <script type="text/javascript" src="lib/jqwidgets/jqxmenu.js"></script>
                          <script type="text/javascript" src="lib/jqwidgets/jqxgrid.js"></script>
                          <script type="text/javascript" src="lib/jqwidgets/jqxgrid.selection.js"></script> 
                          <script type="text/javascript" src="lib/jqwidgets/jqxgrid.columnsresize.js"></script> 
                          <script type="text/javascript" src="lib/jqwidgets/jqxdata.js"></script> 
                          <script type="text/javascript" src="lib/jqwidgets/demos.js"></script> 
                          <script type="text/javascript" src="lib/jqwidgets/jqxgrid.sort.js"></script>
                          <script type="text/javascript" src="lib/jqwidgets/jqxgrid.filter.js"></script>
                          <script type="text/javascript" src="lib/jqwidgets/jqxdropdownlist.js"></script>
                          <script type="text/javascript" src="lib/jqwidgets/jqxlistbox.js"></script>
                          <script type="text/javascript" src="lib/jqwidgets/jqxgrid.pager.js"></script>
                          <script type="text/javascript" src="lib/jqwidgets/jqxdata.export.js"></script> 
                          <script type="text/javascript" src="lib/jqwidgets/jqxgrid.export.js"></script>';
                break;
        }

        return $meta . $title . $link . $script;
    }

    public function foot_page($page = '') {
        switch ($page) {
            case 'report':
                $foot_page = '<script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
					<script type="text/javascript" src="js/jquery-ui-1.7.1.custom.min.js"></script>
					<script type="text/javascript" src="js/daterangepicker.jQuery.js"></script>
					<link rel="stylesheet" href="css/ui.daterangepicker.css" type="text/css" />
					<link rel="stylesheet" href="css/jquery-ui-1.7.1.custom.css" type="text/css" title="ui-theme" />';
                break;

            default: $foot_page = '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script> 
						<script src="http://code.jquery.com/ui/1.8.20/jquery-ui.min.js" type="text/javascript"></script> 
						<script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js" type="text/javascript"></script> 
						<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/minified/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>';
        }

        return $foot_page;
    }

    public function checkinput($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = html_entity_decode($data);
        return $data;
    }

    public function conv_date($datepost) {
        $date = DateTime::createFromFormat('d/m/Y', $datepost);
        return $date->format('Ymd');
    }

    public function show_info($doc, $LabInstrListen, $LabInfoListen, $case = 'home') {
        switch ($case) {
            case 'home':
                $info = $LabInstrListen;
                break;

            case 'listen':
                $info = '<table class="table table-striped">
					<thead>
						<tr>
							<td>' . $LabInstrListen . '</td>		
							<td colspan=2>
								<a target="blank" href="' . $doc . '" title="' . $LabInfoListen . '">
									<img src="img/info.png"/>
								</a>
							</td>								
						</tr>
					</thead>
				</table>';
                break;

            case 'view':
            case 'edit':
                $info = '<table class="table table-striped">
						<thead>
							<tr>
								<td colspan="2">' . $LabInstrListen . '</td>								
							</tr>
						</thead>
					</table>';
                break;

            case 'e_search':
            case 'report':
                $info = '<table class="table table-striped">
					<thead>
						<tr>
							<td colspan=4>' . $LabInstrListen . '</td>								
						</tr>						
					</thead>
				</table>';
                break;
        }

        return $info;
    }

}

?>