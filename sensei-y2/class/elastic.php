<?php
/**
    
	USAGE: $elastic = Elastic::get_instance();
	NEED: config.php - autoload.php
	
**/
require("./lib/vendor/autoload.php");

class Elastic {

    // Store the single instance of the Database
    private static $instance ;
    
    // Server connection parameters
    public $host = "";
    public $user = "";
    public $pass = "";
    public $index = "";
    public $type = "";
    public $auth = "";
    public $count = "";
    

    /**
      Constructor
     * */
    private function __construct() {
        $this->config = Config::get_instance();
        $config = $this->config;

        //$this->host = $config->get_ini_value("ELASTIC", "AUTH"); if connection need authorization
        $this->host = $config->get_ini_value("ELASTIC", "HOST");
        //$this->user = $config->get_ini_value("ELASTIC", "USERNAME"); if connection need authorization
        //$this->pass = $config->get_ini_value("ELASTIC", "PASSWORD"); if connection need authorization
        $this->index = $config->get_ini_value("ELASTIC", "INDEX");
        $this->type = $config->get_ini_value("ELASTIC", "TYPE");
        
        $url = ($this->host).'/'.($this->index).'/'.($this->type).'/_count';
        $this->count = json_decode(file_get_contents($url))->count;

    } // END CONSTRUCTOR

    /**
        Singleton Declaration
    **/    
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new Elastic ();
        }

        return self::$instance;
    } // END SINGLETON DECARATION

    /**
        Connect to server
    **/
    public function connect() {
        $params = array('hosts' => array($this->host));
        //$params["hosts"]=array('http://192.168.56.101:9200', 'http://192.168.56.101');
        $client = new Elasticsearch\Client($params);
        
        return $client;
    }

    /** 
        insert and/or update document
    **/
    public function insert($client, $body, $type, $id=NULL) {
        $params = array();
        if ($id != NULL) {
            $params['id'] = $id;
        }
        $params['index'] = $this->index;
        $params['type'] = $type;
        $params['body'] = $body;
        $ret = $client->index($params);
        
        return $ret;
    }

    /** 
        search a document - return Json format results
    **/
    public function search($client, $body, $type, $size) {
        $params = array();

        $params['index'] = $this->index;
        $params['type'] = $type;
        $params['body'] = $body;
        $params['size'] = $size;
        $ret = $client->search($params);
        
        return $ret;
    }

    /** 
        search exact phrase - return Json format results
    **/
    public function match_phrase($client, $field, $text, $type, $size) {
        $bodysearch = array();
        $query = array();
        $query['match_phrase'][$field] = $text;

        $bodysearch['query']['filtered'] = array("query" => $query);

        $params = array();
        $params['index'] = $this->index;
        $params['type'] = $type;
        $params['body'] = $bodysearch;
        $params['size'] = $size;
        $ret = $client->search($params);
        
        return $ret;
    }

    /** 
        delete a document
    **/
    public function delete($client, $id, $type) {
        $params = array();
        
        $params['id'] = $id;
        $params['index'] = $this->index;
        $params['type'] = $type;
        $ret = $client->delete($params);
        
        return $ret;
    }
    
    /**
        show elaticsearch query
    **/
    public function show_es_query($param) {
        if (is_array($param)) {
            foreach ($param as $k => $v) {
                echo '"'.$k.'": {<br />';
                $this->show_es_query($v);
            }
        }
        else echo '"'.($param).'"<br />';
    }

}
