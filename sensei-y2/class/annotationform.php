<?php

/**

  USAGE: $annotationform = Annotationform::get_instance();
  NEED: db.php - config.php - lang.php

 * */
class Annotationform {

    // Store the single instance of the object
    private static $instance;

    /**
      Constructor
     * */
    private function __construct() {
        $this->config = Config::get_instance();
        $this->db = Database::get_instance();
        $this->lang = Lang::get_instance();
        $this->elastic = Elastic::get_instance();
    }

// END CONSTRUCTOR

    /**
      Singleton Declaration
     * */
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new Annotationform ();
        }

        return self::$instance;
    }

// END SINGLETON DECARATION

    public function get_transcriptions_to_be_processed($service) {
        // get all transcriptions inside the folder:
        $db = $this->db;
        $config = $this->config;
        $elastic = $this->elastic;
        $client = $elastic->connect();
        $type = $elastic->type;
        $count = $elastic->count;

        $bodysearch['query']['bool']['must'][0]['term']['idUser'] = $db->getUserID($_SESSION["username"]);
        //$dataset = $elastic->search($client, $bodysearch, $type, $count);
        $dataset = $dataset['hits']['hits'];
        foreach ($dataset as $k => $v)
            $es_listen[] = $dataset[$k]['_source']['FileName'];

        $path_trs = $config->get_ini_value("PATH", "PATH_TRS_" . $service);
        $files = opendir($path_trs);
        $ret = array();

        while ($filename = readdir($files)) {
            //take the file extensions
            $extn = explode('.', $filename);
            $extn = array_pop($extn);

            if ($filename != '.' && $filename != '..' && $extn == 'trs') {
                if (!in_array($filename, $es_listen))
                    $ret[] = $filename;
            }
        }

        sort($ret);
        return $ret;
    }

    public function show_file_select($LabServListen, $LabFileListen, $LabLoadFBtn, $service, $filtfile, $filename, $action) {
        $curr_url = basename($_SERVER['PHP_SELF']);
        switch ($action) {
            case 'insert':
                $service_selected = array("LUNA" => "", "" => "DECODA");
                $service_selected[$service] = "selected";

                switch ($filtfile) {
                    case '_eug':
                        $filtfile_selected = array("vuoto" => "", "_eug" => "selected", "_may" => "", "_new" => "");
                        break;

                    case '_may':
                        $filtfile_selected = array("vuoto" => "", "_eug" => "", "_may" => "selected", "_new" => "");
                        break;

                    case '_new':
                        $filtfile_selected = array("vuoto" => "", "_eug" => "", "_may" => "", "_new" => "selected");
                        break;

                    default:
                        $filtfile_selected = array("vuoto" => "", "_eug" => "", "_may" => "", "_new" => "");
                        break;
                }

                $trvect = $this->get_transcriptions_to_be_processed($service);
                foreach ($trvect as $tr) {
                    if (strrpos($tr, $filtfile) == true || $filtfile == '') {
                        $select_filename .= '<option value="' . $tr . '"' . ((isset($filename) && $filename == $tr) ? ' selected="selected"' : '') . '>' . $tr . '</option>';
                    }
                }

                $file_select = '<table>
					<thead>
						<tr>
							<th>' . $LabServListen . '</th>
							<th>' . $LabFileListen . '</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<th>									
								<select class="form-control" style="width: 100%;" id="service" name="service" onchange="submit();">
									<option ' . $service_selected['LUNA'] . ' value="LUNA"/>LUNA</option>
									<option ' . $service_selected['DECODA'] . ' value="DECODA">DECODA</option>										
								</select>									
							</th>	
							<th>	
								<select class="form-control" style="width: 100%;" id="filename" name="filename" class="form-action" >' . $select_filename . '</select>									
							</th>
							<th>
								<select class="form-control" style="width: 100%;" id="filtfile" name="filtfile" class="form-action" onchange="submit();">
									<option ' . $filtfile_selected['vuoto'] . ' value=""></option>
									<option ' . $filtfile_selected['_eug'] . ' value="_eug">_eug</option>
									<option ' . $filtfile_selected['_may'] . ' value="_may">_may</option>
									<option ' . $filtfile_selected['_new'] . ' value="_new">_new</option>
								</select>
							</th>
							<th>&nbsp;
								' . $this->show_button("button", "", "btn btn-sm btn-primary", 'onclick="javascript:loadTranscription(\'' . $service . '\',document.form1.filename.value,\'' . $curr_url . '\'); getTime(\'s\');"', $LabLoadFBtn) . '
							</th>								
						</tr>
					</thead>
				</table>';
                break;

            case 'view':
            case 'update':
                $file_select = '<table>
					<thead>
						<tr>
							<th>' . $LabServListen . '</th>
							<th>' . $LabFileListen . '</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<th>									
								<select class="form-control" style="width: 100%;" id="service" name="service" onchange="submit();">
									<option value="' . $service . '"/>' . $service . '</option>
								</select>									
							</th>	
							<th>
								<select class="form-control" style="width: 100%;" id="filename" name="filename" class="form-action" >
									<option value="' . $filename . '">' . $filename . '</option>
								</select>									
							</th>
							<th>&nbsp;
								' . $this->show_button("button", "btnload", "btn btn-sm btn-primary", 'onclick="javascript:loadTranscription(\'' . $service . '\',document.form1.filename.value,\'' . $curr_url . '\'); getTime(\'s\');"', $LabLoadFBtn, 'display:none;') . '
							</th>
						</tr>
					</thead>
				</table>';
                break;
        }

        return $file_select;
    }

    public function show_file_select_prefilled($LabServListen, $LabFileListen, $LabLoadFBtn, $service, $filtfile, $filename, $action) {
        $curr_url = basename($_SERVER['PHP_SELF']);
        switch ($action) {
            case 'insert':
                $service_selected = array("LUNA" => "", "" => "DECODA");
                $service_selected[$service] = "selected";

                switch ($filtfile) {
                    case '_eug':
                        $filtfile_selected = array("vuoto" => "", "_eug" => "selected", "_may" => "", "_new" => "");
                        break;

                    case '_may':
                        $filtfile_selected = array("vuoto" => "", "_eug" => "", "_may" => "selected", "_new" => "");
                        break;

                    case '_new':
                        $filtfile_selected = array("vuoto" => "", "_eug" => "", "_may" => "", "_new" => "selected");
                        break;

                    default:
                        $filtfile_selected = array("vuoto" => "", "_eug" => "", "_may" => "", "_new" => "");
                        break;
                }

                $trvect = $this->get_transcriptions_to_be_processed($service);
                foreach ($trvect as $tr) {
                    if (strrpos($tr, $filtfile) == true || $filtfile == '') {
                        $select_filename .= '<option value="' . $tr . '"' . ((isset($filename) && $filename == $tr) ? ' selected="selected"' : '') . '>' . $tr . '</option>';
                    }
                }

                $file_select = '<table>
					<thead>
						<tr>
							<th>' . $LabServListen . '</th>
							<th>' . $LabFileListen . '</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<th>									
								<select class="form-control" style="width: 100%;" id="service" name="service" onchange="submit();">
									<option ' . $service_selected['LUNA'] . ' value="LUNA"/>LUNA</option>
									<option ' . $service_selected['DECODA'] . ' value="DECODA">DECODA</option>										
								</select>									
							</th>	
							<th>	
								<select class="form-control" style="width: 100%;" id="filename" name="filename" class="form-action" >' . $select_filename . '</select>									
							</th>
							<th>
								<select class="form-control" style="width: 100%;" id="filtfile" name="filtfile" class="form-action" onchange="submit();">
									<option ' . $filtfile_selected['vuoto'] . ' value=""></option>
									<option ' . $filtfile_selected['_eug'] . ' value="_eug">_eug</option>
									<option ' . $filtfile_selected['_may'] . ' value="_may">_may</option>
									<option ' . $filtfile_selected['_new'] . ' value="_new">_new</option>
								</select>
							</th>
							<th>&nbsp;
								' . $this->show_button("button", "", "btn btn-sm btn-primary", 'onclick="javascript:loadTranscription(\'' . $service . '\',document.form1.filename.value,\'' . $curr_url . '\',\'insert\'); getTime(\'s\');"', $LabLoadFBtn) . '
							</th>								
						</tr>
					</thead>
				</table>';
                break;

            case 'view':
            case 'update':
                $file_select = '<table>
					<thead>
						<tr>
							<th>' . $LabServListen . '</th>
							<th>' . $LabFileListen . '</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<th>									
								<select class="form-control" style="width: 100%;" id="service" name="service" onchange="submit();">
									<option value="' . $service . '"/>' . $service . '</option>
								</select>									
							</th>	
							<th>
								<select class="form-control" style="width: 100%;" id="filename" name="filename" class="form-action" >
									<option value="' . $filename . '">' . $filename . '</option>
								</select>									
							</th>
							<th>&nbsp;
								' . $this->show_button("button", "btnload", "btn btn-sm btn-primary", 'onclick="javascript:loadTranscription(\'' . $service . '\',document.form1.filename.value,\'' . $curr_url . '\',\'update\'); getTime(\'s\');"', $LabLoadFBtn, 'display:none;') . '
							</th>
						</tr>
					</thead>
				</table>';
                break;
        }

        return $file_select;
    }

    private function show_button($type, $id, $class, $event, $label, $style = '') {
        return '<button type="' . $type . '" id="' . $id . '" style="' . $style . '" class="' . $class . '" ' . $event . '>' . $label . '</button>';
    }

    public function show_questionnaire($session_lang, $session_username, $FlagService, $post, $get, $action) {
        $db = $this->db;
        $config = $this->config;
        $lang = $this->lang;

        $questionnaire = '<div class="synopsis-tooltip"></div>';

        switch ($action) {
            case 'insert':
                $questionnaire .= '<table class="table table-bordered">
					<thead>
						<tr>
							<th>Description</th>
							<th>PASS</th>                
							<th>FAIL</th>    
							<th>N.A.</th>    
							<th>Note</th> 																	
						</tr>
					</thead>
					<tbody>';

                //extract the item
                $query = "SELECT IdItem, Description" . $session_lang . " as Description, Weight";
                $query .= " FROM tblItem WHERE active = 1 ";
                $query .= " and Flag" . $FlagService . " = 1";
                $query .= " order by idItem";

                $arrTemp = array();
                $arrReturn = array();

                $arrTemp = $db->fetch_array($query);
                foreach ($arrTemp as $k => $v) {
                    $string = '';
                    foreach ($v as $a => $b)
                        $string .= $b . '|';
                    $arrReturn[$k] = $string;
                }
                reset($arrReturn);

                $i = 1;
                while (list($key, $value) = each($arrReturn)) {
                    $arrId_Item = explode("|", $value);
                    $idItem = $arrId_Item[0];
                    $Item = $arrId_Item[1];
                    $Weight = $arrId_Item[2];

                    $questionnaire .= '<tr><td colspan=5><b>' . $i . ') <u>' . $Item . '</u></b></td></tr>';

                    //extract the subitem
                    $querysub = "SELECT idSubItem, Description" . $session_lang . " as Description ";
                    $querysub .= " FROM tblSubItem WHERE active = 1 and idItem = '" . $idItem . "'";
                    $querysub .= " and Flag" . $FlagService . " = 1";
                    $querysub .= " order by idItem,idSubItem";

                    $arrReturn1 = array();

                    $arrTemp = $db->fetch_array($querysub);
                    foreach ($arrTemp as $k => $v) {
                        $string = '';
                        foreach ($v as $a => $b)
                            $string .= $b . '|';
                        $arrReturn1[$k] = $string;
                    }

                    //number of extract record for this item
                    $PartNumSubItem = count($arrReturn1);

                    while (list($keysub, $valuesub) = each($arrReturn1)) {
                        $arrId_SubItem = explode("|", $valuesub);
                        $idSubItem = $arrId_SubItem[0];
                        $SubItem = $arrId_SubItem[1];

                        $SubWeight = ($Weight / $PartNumSubItem);

                        $radio_checked = array("PASS" => "", "FAIL" => "", "NA" => "");
                        $questionnaire .= '<tr valign="center">
                                                    <td>' . $SubItem . '
                                                            <input size="2" style="border:0px;" class="error" type="text" id="flagerr' . $idSubItem . '" name="flagerr' . $row2["idSubItem"] . '" value="' . $post["flagerr" . $idSubItem] . '" />
                                                    </td>
                                                    <input type="hidden" name="idsub' . $idSubItem . '" id="idsub' . $idSubItem . '" value="' . $idSubItem . '" />
                                                    <input type="hidden" name="subweight' . $idSubItem . '" id="subweight' . $idSubItem . '" value="' . $SubWeight . '" />

                                                    <td align="center"><input class="hand" title="PASS" ' . $radio_checked['PASS'] . ' type="radio" id="radio' . $idSubItem . '" name="radio' . $idSubItem . '" value="PASS"></td>											
                                                    <td align="center"><input class="hand" title="FAIL" ' . $radio_checked['FAIL'] . ' type="radio" id="radio' . $idSubItem . '" name="radio' . $idSubItem . '" value="FAIL"></td>	
                                                    <td align="center"><input class="hand" title="NA" ' . $radio_checked['NA'] . ' type="radio" id="radio' . $idSubItem . '" name="radio' . $idSubItem . '" value="NA"></td>

                                                    <td align="center">
                                                            <textarea id="note' . $idSubItem . '" name="note' . $idSubItem . '" maxlength="500" rows="2">' . $post["note" . $idSubItem] . '</textarea>														
                                                    </td>
                                                </tr>';

                        $LabSpeechListen = $lang->get_language($session_username, 'LabSpeechListen', $session_lang);
                        if ($post["turn" . $idSubItem] == "" || $post["turn" . $idSubItem] != $LabSpeechListen)
                            $textarea_turn = $LabSpeechListen;
                        else
                            $textarea_turn = $post["turn" . $idSubItem];

                        $LabConsListen = $lang->get_language($session_username, 'LabConsListen', $session_lang);
                        $LabCkSpeechListen = $lang->get_language($session_username, 'LabCkSpeechListen', $session_lang);

                        if ($post["ckGen" . $idSubItem] === "on")
                            $checked = "checked";
                        else
                            $checked = "";

                        $questionnaire .= '<tr>
								<td colspan="4" height="60pt">
									<textarea readonly id="turn' . $idSubItem . '" name="turn' . $idSubItem . '" class="droppable" type="text" style="overflow:auto" rows="3">' . $LabSpeechListen . '</textarea>
								</td>	
								<td > 
									<input onclick="ConCheckbox(\'' . $idSubItem . '\');" id="ckGen' . $idSubItem . '" name="ckGen' . $idSubItem . '" type="checkbox" value="1" ' . $checked . ' > ' . $LabCkSpeechListen . '
									<input id="FlagGen' . $idSubItem . '" name="FlagGen' . $idSubItem . '" type="hidden" value="N" />
								</td>												
							</tr>';
                        $j++;
                    }
                    $i++;
                }

                $LabConsListen = $lang->get_language($session_username, 'LabConsListen', $session_lang);
                $questionnaire .= '<tr>
							<td colspan=6>' . $i++ . '. <u>' . $LabConsListen . '</u>' . $flagerrcons . '</td>
							</tr>
							<tr>
							<td colspan=5>
								<textarea id="finalnote" name="finalnote" maxlength="1000" cols="80" rows="6">' . $_POST["finalnote"] . '</textarea>								
							</td>
						</tr>
						<tr>
							<td colspan=6>' . $i++ . '. <u>Synopsis</u>' . $flagerrsyn . '</td>											
						</tr>	
						<tr>
							<td colspan=5>
								<textarea id="synopsisnote" name="synopsisnote" maxlength="1000" cols="80" rows="6">' . $_POST["synopsisnote"] . '</textarea>
								<span id="lenSys" class="badge"></span><input id="lenxml" name="lenxml" type="hidden" readonly />	
							</td>
						</tr>
					</tbody>
				</table>';

                $LabSaveBtn = $lang->get_language($session_username, 'LabSaveBtn', $session_lang);
                $questionnaire .= '<p align="center">' . $this->show_button("button", "", "btn btn-lg btn-success", 'onclick="Save();"', $LabSaveBtn) . '</p>';
                break;

//			case 'view':
//				$items  = $db->fetch_array("SELECT * FROM tblListenScore 
//					LEFT JOIN tblSubItem ON tblSubItem.idSubItem = tblListenScore.idSubItem 															
//					WHERE idlisten = '".$get['id']."' order by tblSubItem.iditem, tblSubItem.idsubitem");
//				
//				$questionnaire .= '<table class="table table-bordered">
//						<tr>
//							<th>Description</th>
//							<th>PASS</th>                
//							<th>FAIL</th>    
//							<th>N.A.</th>
//							<th>Note</th>
//						</tr>';
//				
//				foreach ($items as $it) {
//					$idScore = $it['idScore'];
//					$idsubitem = $it['idSubItem'];
//					
//					if ($it['FlagGeneral']=='Y'){
//						$valcheck = "checked";
//						$classdrop = "droppable nodroppable";
//					}
//					else
//					{
//						$valcheck='';
//						$classdrop = "droppable";
//					}
//
//					switch ($it['Score']) {
//						case 'PASS':
//							$checked = array("PASS"=>"checked", "FAIL"=>"", "NA"=>"");
//						break;
//						
//						case 'FAIL':
//							$checked = array("PASS"=>"", "FAIL"=>"checked", "NA"=>"");
//						break;
//						
//						case 'NA':
//							$checked = array("PASS"=>"", "FAIL"=>"", "NA"=>"checked");
//						break;
//					}
//					
//					$questionnaire .= '<tr>
//							<td>'.$it["Description".$session_lang].'</td>
//							<td align="center"><input disabled class="hand" title="PASS" '.$checked['PASS'].' type="radio" id="radio'.$idScore.'" name="radio'.$idScore.'" value="PASS"></td>											
//							<td align="center"><input disabled class="hand" title="FAIL" '.$checked['FAIL'].' type="radio" id="radio'.$idScore.'" name="radio'.$idScore.'" value="FAIL"></td>	
//							<td align="center"><input disabled class="hand" title="NA" '.$checked['NA'].' type="radio" id="radio'.$idScore.'" name="radio'.$idScore.'" value="NA"></td>	
//							<td align="center"><textarea readonly id="note'.$idScore.'" name="note'.$idScore.'" maxlength="500" rows="2">'.$it['Note'].'</textarea></td>
//						</tr>
//						<tr>
//							<td colspan="4"><textarea readonly data-id="'.$it['idScore'].'" class="'.$classdrop.'" type="text" style="overflow:auto;" rows="3">'.($it['Turn']=='-'?'':$it['Turn']).'</textarea></td>
//							<td><input id="ckGen'.$it['idScore'].'" '.$valcheck.' type="checkbox" data-id="'.$it['idScore'].'" class="general" disabled/> '.$lang -> get_language($session_username,'LabCkSpeechListen',$session_lang).'</td>
//						</tr>';
//				}
//				$questionnaire .= '</table>';
//				
//				$questionnaire .=  '<table class="table table-bordered">
//						<tr>
//							<td colspan=5><u>'.$lang -> get_language($session_username,'LabConsListen',$session_lang).'</u></td>
//						</tr>	
//						<tr>
//							<td colspan=4><textarea readonly id="finalnote" name="finalnote" maxlength="1000" cols="80" rows="6">'.$post["finalnote"].'</textarea></td>
//						</tr>
//						<tr>
//							<td colspan=5><u>Synopsis</u></td>											
//						</tr>	
//						<tr>
//							<td colspan=4> 
//								<textarea readonly id="synopsisnote" name="synopsisnote" maxlength="1000" cols="80" rows="6">'.$post["synopsisnote"].'</textarea>																	
//								<span id="lenSys" title="7% Synopsis" class="badge"></span>
//								<input id="lenxml" name="lenxml" type="hidden" />									
//							</td>
//						</tr>							
//					</table>';
//					
//				$LabBackBtn = $lang -> get_language($session_username,'LabBackBtn', $session_lang);
//				$questionnaire .=  '<p align="center">'.$this->show_button("button", "", "btn btn-lg btn-primary", 'onclick="goBack();"', $LabBackBtn).'</p>';
//			break;
//			
            case 'view':
                $questionnaire .= '<table class="table table-bordered">
                                <thead>
                                        <tr>
                                                <th>Description</th>
                                                <th>PASS</th>                
                                                <th>FAIL</th>    
                                                <th>N.A.</th>    
                                                <th>Note</th> 																	
                                        </tr>
                                </thead>
                                <tbody>';

                //extract the item
                $query = "SELECT IdItem, Description" . $session_lang . " as Description, Weight";
                $query .= " FROM tblItem WHERE active = 1 ";
                $query .= " and Flag" . $FlagService . " = 1";
                $query .= " order by idItem";

                $arrTemp = array();
                $arrReturn = array();

                $arrTemp = $db->fetch_array($query);
                foreach ($arrTemp as $k => $v) {
                    $string = '';
                    foreach ($v as $a => $b)
                        $string .= $b . '|';
                    $arrReturn[$k] = $string;
                }
                reset($arrReturn);

                $i = 1;
                while (list($key, $value) = each($arrReturn)) {
                    $arrId_Item = explode("|", $value);
                    $idItem = $arrId_Item[0];
                    $Item = $arrId_Item[1];
                    $Weight = $arrId_Item[2];

                    $questionnaire .= '<tr><td colspan=5><b>' . $i . ') <u>' . $Item . '</u></b></td></tr>';

                    //extract the subitem
                    $querysub = "SELECT idSubItem, Description" . $session_lang . " as Description ";
                    $querysub .= " FROM tblSubItem WHERE active = 1 and idItem = '" . $idItem . "'";
                    $querysub .= " and Flag" . $FlagService . " = 1";
                    $querysub .= " order by idItem,idSubItem";

                    $arrReturn1 = array();

                    $arrTemp = $db->fetch_array($querysub);
                    foreach ($arrTemp as $k => $v) {
                        $string = '';
                        foreach ($v as $a => $b)
                            $string .= $b . '|';
                        $arrReturn1[$k] = $string;
                    }

                    //number of extract record for this item
                    $PartNumSubItem = count($arrReturn1);

                    while (list($keysub, $valuesub) = each($arrReturn1)) {
                        $arrId_SubItem = explode("|", $valuesub);
                        $idSubItem = $arrId_SubItem[0];
                        $SubItem = $arrId_SubItem[1];

                        $SubWeight = ($Weight / $PartNumSubItem);

                        $radio_checked = array("PASS" => "", "FAIL" => "", "NA" => "");
                        $questionnaire .= '<tr valign="center">
                                                <td>' . $SubItem . '
                                                        <input size="2" style="border:0px;" class="error" type="text" id="flagerr' . $idSubItem . '" name="flagerr' . $row2["idSubItem"] . '" value="' . $post["flagerr" . $idSubItem] . '" />
                                                </td>
                                                <input type="hidden" name="idsub' . $idSubItem . '" id="idsub' . $idSubItem . '" value="' . $idSubItem . '" />
                                                <input type="hidden" name="subweight' . $idSubItem . '" id="subweight' . $idSubItem . '" value="' . $SubWeight . '" />

                                                <td align="center"><input class="hand" title="PASS" ' . $radio_checked['PASS'] . ' type="radio" id="radio' . $idSubItem . '" name="radio' . $idSubItem . '" value="PASS"></td>											
                                                <td align="center"><input class="hand" title="FAIL" ' . $radio_checked['FAIL'] . ' type="radio" id="radio' . $idSubItem . '" name="radio' . $idSubItem . '" value="FAIL"></td>	
                                                <td align="center"><input class="hand" title="NA" ' . $radio_checked['NA'] . ' type="radio" id="radio' . $idSubItem . '" name="radio' . $idSubItem . '" value="NA"></td>

                                                <td align="center">
                                                        <textarea id="note' . $idSubItem . '" name="note' . $idSubItem . '" maxlength="500" rows="2">' . $post["note" . $idSubItem] . '</textarea>														
                                                </td>
                                            </tr>';

                        $LabSpeechListen = $lang->get_language($session_username, 'LabSpeechListen', $session_lang);
                        if ($post["turn" . $idSubItem] == "" || $post["turn" . $idSubItem] != $LabSpeechListen)
                            $textarea_turn = $LabSpeechListen;
                        else
                            $textarea_turn = $post["turn" . $idSubItem];

                        $LabConsListen = $lang->get_language($session_username, 'LabConsListen', $session_lang);
                        $LabCkSpeechListen = $lang->get_language($session_username, 'LabCkSpeechListen', $session_lang);

                        if ($post["ckGen" . $idSubItem] === "on")
                            $checked = "checked";
                        else
                            $checked = "";

                        $questionnaire .= '
                                            <tr>
                                               <td colspan="4" height="60pt">
                                                  <textarea readonly id="turn' . $idSubItem . '" name="turn' . $idSubItem . '" class="droppable" type="text" style="overflow:auto" rows="3">' . $LabSpeechListen . '</textarea>
                                               </td>
                                               <td > 
                                                  <input onclick="ConCheckbox(\'' . $idSubItem . '\');" id="ckGen' . $idSubItem . '" name="ckGen' . $idSubItem . '" type="checkbox" value="1" ' . $checked . ' > ' . $LabCkSpeechListen . '
                                                  <input id="FlagGen' . $idSubItem . '" name="FlagGen' . $idSubItem . '" type="hidden" value="N" />
                                               </td>
                                            </tr>                            
                                            ';
                        $j++;
                    }
                    $i++;
                }

                $LabConsListen = $lang->get_language($session_username, 'LabConsListen', $session_lang);
                $questionnaire .= '<tr>
                                            <td colspan=6>' . $i++ . '. <u>' . $LabConsListen . '</u>' . $flagerrcons . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan=5>
                                                <textarea id="finalnote" name="finalnote" maxlength="1000" cols="80" rows="6">' . $_POST["finalnote"] . '</textarea>								
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan=6>' . $i++ . '. <u>Synopsis</u>' . $flagerrsyn . '</td>											
                                        </tr>	
                                        <tr>
                                            <td colspan=5>
                                                <textarea id="synopsisnote" name="synopsisnote" maxlength="1000" cols="80" rows="6">' . $_POST["synopsisnote"] . '</textarea>
                                                <span id="lenSys" class="badge"></span><input id="lenxml" name="lenxml" type="hidden" readonly />	
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>';

                $LabSaveBtn = $lang->get_language($session_username, 'LabSaveBtn', $session_lang);
                $questionnaire .= '<p align="center">' . $this->show_button("button", "", "btn btn-lg btn-success", 'onclick="Save();"', $LabSaveBtn) . '</p>';
                break;

            case 'update':
                $items = $db->fetch_array("SELECT * FROM tblListenScore 
					LEFT JOIN tblSubItem ON tblSubItem.idSubItem = tblListenScore.idSubItem 															
					WHERE idlisten = '" . $get['id'] . "' order by tblSubItem.iditem, tblSubItem.idsubitem");

                $questionnaire .= '<table class="table table-bordered">
                                    <tr>
                                        <th>Description</th>
					<th>PASS</th>                
					<th>FAIL</th>    
					<th>N.A.</th>
					<th>Note</th>
                                    </tr>';

                foreach ($items as $it) {
                    $idScore = $it['idScore'];
                    $idsubitem = $it['idSubItem'];

                    if ($it['FlagGeneral'] == 'Y') {
                        $valcheck = "checked";
                        $classdrop = "droppable nodroppable";
                    } else {
                        $valcheck = '';
                        $classdrop = "droppable";
                    }

                    switch ($it['Score']) {
                        case 'PASS':
                            $checked = array("PASS" => "checked", "FAIL" => "", "NA" => "");
                            break;

                        case 'FAIL':
                            $checked = array("PASS" => "", "FAIL" => "checked", "NA" => "");
                            break;

                        case 'NA':
                            $checked = array("PASS" => "", "FAIL" => "", "NA" => "checked");
                            break;
                    }

                    $questionnaire .= '
                                        <tr>
                                           <td>' . $it["Description" . $session_lang] . '</td>
                                           <td align="center"><input disabled class="hand" title="PASS" ' . $checked['PASS'] . ' type="radio" id="radio' . $idScore . '" name="radio' . $idScore . '" value="PASS"></td>
                                           <td align="center"><input disabled class="hand" title="FAIL" ' . $checked['FAIL'] . ' type="radio" id="radio' . $idScore . '" name="radio' . $idScore . '" value="FAIL"></td>
                                           <td align="center"><input disabled class="hand" title="NA" ' . $checked['NA'] . ' type="radio" id="radio' . $idScore . '" name="radio' . $idScore . '" value="NA"></td>
                                           <td align="center"><textarea readonly id="note' . $idScore . '" name="note' . $idScore . '" maxlength="500" rows="2">' . $it['Note'] . '</textarea></td>
                                        </tr>
                                        <tr>
                                           <td colspan="4"><textarea readonly data-id="' . $it['idScore'] . '" class="' . $classdrop . '" type="text" style="overflow:auto;" rows="3">' . ($it['Turn'] == '-' ? '' : $it['Turn']) . '</textarea></td>
                                           <td><input id="ckGen' . $it['idScore'] . '" ' . $valcheck . ' type="checkbox" data-id="' . $it['idScore'] . '" class="general" disabled/> ' . $lang->get_language($session_username, 'LabCkSpeechListen', $session_lang) . '</td>
                                        </tr>
                            ';
                }
                $questionnaire .= '</table>';

                $questionnaire .= '
                                    <table class="table table-bordered">
                                       <tr>
                                          <td colspan=5><u>' . $lang->get_language($session_username, 'LabConsListen', $session_lang) . '</u></td>
                                       </tr>
                                       <tr>
                                          <td colspan=4><textarea id="finalnote" name="finalnote" maxlength="1000" cols="80" rows="6">' . $post["finalnote"] . '</textarea></td>
                                       </tr>
                                       <tr>
                                          <td colspan=5><u>Synopsis</u></td>
                                       </tr>
                                       <tr>
                                          <td colspan=4> 
                                             <textarea id="synopsisnote" name="synopsisnote" maxlength="1000" cols="80" rows="6">' . $post["synopsisnote"] . '</textarea>																	
                                             <span id="lenSys" title="7% Synopsis" class="badge"></span>
                                             <input id="lenxml" name="lenxml" type="hidden" />									
                                          </td>
                                       </tr>
                                    </table>
                        ';

                $LabBackBtn = $lang->get_language($session_username, 'LabBackBtn', $session_lang);
                $LabSaveBtn = $lang->get_language($session_username, 'LabSaveBtn', $session_lang);
                $questionnaire .= '<p align="center">' .
                        $this->show_button("button", "", "btn btn-lg btn-primary", 'onclick="goBack();"', $LabBackBtn) .
                        ' ' .
                        $this->show_button("button", "", "btn btn-lg btn-success", 'onclick="Save();"', $LabSaveBtn) .
                        '</p>';
                break;
        }

        return $questionnaire;
    }

    private function len_value_xml($xml) {
        /* estraggo tutti i contenuti dei nodi dall'xml */
        $xmlstr = new SimpleXMLElement($xml);
        $strTrs = $xmlstr->asXML();
        $lenTrs = strlen($strTrs);

        return $lenTrs;
    }

    public function process_xsl($xml, $template) {
        $xsl = new DOMDocument();
        $xsl->load($template);

        $xsl_proc = new XsltProcessor();
        $xsl_proc->registerPHPFunctions();
        $xsl_proc->importStyleSheet($xsl);

        $xml_doc = new DOMDocument();
        $xml_doc->loadXML($xml);

        $xmlDiv = $xsl_proc->transformToXML($xml_doc);

        //ritorna la lunghezza del solo testo + il testo con i tempi inizio fine
        return '|' . ($this->len_value_xml($xml)) . '|' . $xmlDiv;
    }

    public function show_file_select_extr($LabServListen, $LabFileListen, $LabLoadFBtn, $service, $filtfile, $filename, $action) {
        $curr_url = basename($_SERVER['PHP_SELF']);


        $file_select = '
                                <table>
                                   <thead>
                                      <tr>
                                         <th>Service</th>
                                         <th>Scenario</th>
                                         <th>Condition</th>
                                         <th>Task</th>
                                      </tr>
                                      <tr>
                                         <th>
                                            <select class="form-control" style="width: 100%;" id="service" name="service" >
                                            <option  value="DECODA">DECODA</option>                                               
                                            <option  value="LUNA"/>LUNA</option>
                                              
                                            </select>
                                         </th>
                                         <th>
                                            <select class="form-control" style="width: 100%;" id="scenario" name="scenario" >
                                               <option  value="1"/>Scenario 1</option>
                                               <option  value="2">Scenario 2</option>
                                            </select>
                                         </th>
                                         <th>
                                            <select class="form-control" style="width: 100%;" id="condition" name="condition" >
                                               <option  value="1"/>C1</option>
                                               <option  value="2">C2</option>
                                            </select>
                                         </th>
                                         <th>
                                            <select class="form-control" style="width: 100%;" id="task" name="task" >
                                               <option  value="1"/>Task 1</option>
                                               <option  value="2"/>Task 2</option>
                                            </select>
                                         </th>
                                         <th>
                                            <select class="form-control" style="display:none; width: 100%;" id="filtfile" name="filtfile" class="form-action" onchange="submit();">
                                            <option ' . $filtfile_selected['vuoto'] . ' value=""></option>
                                            <option ' . $filtfile_selected['_eug'] . ' value="_eug">_eug</option>
                                            <option ' . $filtfile_selected['_may'] . ' value="_may">_may</option>
                                            <option ' . $filtfile_selected['_new'] . ' value="_new">_new</option>
                                            </select>
                                         </th>
                                         <th>&nbsp;
                                            ' . $this->show_button("button", "", "btn btn-sm btn-primary", 'onclick="javascript:loadExtrEvaluation(); getTime(\'s\');"', $LabLoadFBtn) . '
                                         </th>
                                      </tr>
                                   </thead>
                                </table>
                                ';


        return $file_select;
    }

}

?>