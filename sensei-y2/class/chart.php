<?php
/**
     
    USAGE: $chart = Chart::get_instance();
	NEED: db.php - log.php
     
**/
class Chart {
    // Store the single instance of the object
    private static $instance ;
	
	/**
        Constructor
    **/
    private function __construct() {
		$this->db = Database::get_instance();
		$this->log = Log::get_instance();
    }// END CONSTRUCTOR
	
	/**
        Singleton Declaration
    **/
    public static function get_instance() {
        if( !self::$instance) {
            self::$instance = new Chart () ;    
        }
        
        return self::$instance ;
    }// END SINGLETON DECARATION


	public function get_values_by_json_from_query($arrReturnTot) {
		$strKeyVal = '';
		$jsonData = json_encode($arrReturnTot, JSON_PRETTY_PRINT);
		$phpArray = json_decode($jsonData);

		foreach ($phpArray as $key => $value) { 
			foreach ($value as $keys => $values) { 
				if ($keys=='type'){
					$arrKey[] = ''.$keys.': '.'"'.$values.'", ';
				}
				if ($keys=='percent'){
					$arrVal[] = $keys.': '.$values;
				}
			}
		}
		
		//make the output string for the js
		for ($i = 0; $i < count($arrKey); $i++){
			$strKeyVal .= $arrKey[$i].$arrVal[$i].',|';
		}

		//remove the last ","
		return substr($strKeyVal, 0, strlen($strKeyVal)-2);	
	}

	public function select_query_json($query) {
		$db = $this->db;
		
		$arrJson = array();
		$arr = $db->fetch_array($query);
		foreach ($arr as $k => $v) {
			$n=0;
			foreach ($v as $i => $j) {
				$arrJson[$k][$n]=$j;
				$arrJson[$k][$i]=$j;
				$n++;
			}	
		}
		
		return $arrJson;
	}

	public function get_strJs($query) {
		$log = $this->log;
		$arrReturnTot = $this->select_query_json($query);

		if (count($arrReturnTot)==0){
			$log->ins_log('Chart line 10', $_SESSION["username"] );
			echo "Warning line 10! Contact the web administrator.";
		} else {
			reset($arrReturnTot);
			
			$strStatService  = $this->get_values_by_json_from_query($arrReturnTot);
			$arrService = explode('|',$strStatService);		
			$strJs = '';
			
			//make the output string for the js
			for ($i = 0; $i < count($arrService); $i++){
			
				//echo str_replace(',,',',','{'.$arrService[$i].', color: getRandomColor(), subs:[');
				$strJs .= '{'.str_replace(',,',',',$arrService[$i].', color: getRandomColor(), subs:[');
				
				$service = $arrReturnTot[$i][0];
				//echo $service."+";
				
				//***************************COUNT THE USERS'S ANNOTATIONS OF EACH SERVICE**************************
				$query   = " select Sysuser as type, service, count(idListen)percent ";
				$query  .= " from tblListen ";
				$query  .= " where service='".$service."'";
				$query  .= " group by type,service ORDER BY COUNT( idListen )";
				//echo $query.'<br>';
				
				$arrReturnTotDet = array();
				//call the query function
				$arrReturnTotDet = $this->select_query_json($query);

				if (count($arrReturnTotDet)==0){
					$log->ins_log('Chart line 40', $_SESSION["username"] );
					echo "Warning line 40! Contact the web administrator.";
				}
				else
				{
					reset($arrReturnTotDet);
					
					$strStatServiceDet = $this->get_values_by_json_from_query($arrReturnTotDet);
					$strJs .= '{'.str_replace(',|','},{',$strStatServiceDet);
				}
				$strJs .= '}]}';
				//**************************************************************************************************		
			}
			$strJs .= ',';
		}
		
		/*MAKE THE STRING IN THE JS FORMAT*/
		$strJs = str_replace('}{type','},{type',$strJs);
		$strJs = substr($strJs, 0, strlen($strJs)-1);
		
		return $strJs;
	}

	public function get_strJsIsto($query) {
		$log = $this->log;
		$arrReturnTotDet = array();
		
		//call the query function
		$arrReturnTotDet = $this->select_query_json($query);

		if (count($arrReturnTotDet)==0){
			$log->ins_log('Chart line 111', $_SESSION["username"] );
			echo "Warning line 111! Contact the web administrator.";
		}
		else
		{
			reset($arrReturnTotDet);
			$strStatServiceDet = $this->get_values_by_json_from_query($arrReturnTotDet);
			
			$strJsIsto = '{'.str_replace(',|',', "color": getRandomColor()},{',$strStatServiceDet).', "color": getRandomColor()},';
		}

		/*MAKE THE STRING IN THE JS FORMAT*/
		$strJsIsto = str_replace('}{type','},{"type"',$strJsIsto);
		$strJsIsto = substr($strJsIsto, 0, strlen($strJsIsto)-1);
		$strJsIsto = str_replace('type','"filename"',$strJsIsto);
		$strJsIsto = str_replace('percent','"percent"',$strJsIsto);
		
		return $strJsIsto;
	}

}
?>