<?php

session_start();

date_default_timezone_set('Europe/Rome');

require("class/config.php");
require("class/db.php");

/** Include PHPExcel */
require_once './lib/PHPExcel.php';

$config = Config::get_instance();

$db = Database::get_instance();
$db->connect();

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Teleperformance")
        ->setLastModifiedBy($_SESSION["username"])
        ->setTitle("Sensei")
        ->setSubject("Sensei Report")
        ->setDescription("Dataset result Elasticsearch query")
        ->setKeywords("")
        ->setCategory("");

// Set fixed Column label
$colLabel = array();
$colLabel[]="ID";
$colLabel[]="Conversation Name";

// Set Column [showcol] label
foreach($_SESSION['postdata']['showcol'] as $k => $v) $colLabel[]=$v;

// Set fixed Column label
$colLabel[]="Service";
$colLabel[]="Score";
$colLabel[]="Insert Date";
$colLabel[]="Modify Date";
$colLabel[]="Inserted By";

// Set Column [showsubitem] label
foreach($_SESSION['postdata']['showsubitem'] as $k => $v) $colLabel[]=$v;

// Print Column label
for ($i = 0; $i < count($colLabel); $i++) {
    $colString = PHPExcel_Cell::stringFromColumnIndex($i);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.'1', $colLabel[$i]);
}

$curr_row=$objPHPExcel->getActiveSheet()->getHighestRow();
$curr_row++;
foreach ($_SESSION['dataset']['hits']['hits'] as $k => $v) {
    $i=0;
    $colString = PHPExcel_Cell::stringFromColumnIndex($i++);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.$curr_row, $v['_source']['idListen']);
    
    $colString = PHPExcel_Cell::stringFromColumnIndex($i++);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.$curr_row, $v['_source']['Conversation_Name']);
    
    //Print Column [showcol] value
    foreach($_SESSION['postdata']['showcol'] as $a => $b) {
      $colString = PHPExcel_Cell::stringFromColumnIndex($i++);
      if (($b==='StartDate') || ($b==='EndDate')) $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.$curr_row, str_replace(array("T","Z"), " ", $v['_source'][$b]));
      else $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.$curr_row, $v['_source'][$b]);
    }
    
    $colString = PHPExcel_Cell::stringFromColumnIndex($i++);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.$curr_row, $v['_source']['Service']);
    
    $colString = PHPExcel_Cell::stringFromColumnIndex($i++);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.$curr_row, $v['_source']['Scorelisten']);
    
    $colString = PHPExcel_Cell::stringFromColumnIndex($i++);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.$curr_row, str_replace(array("T","Z"), " ", $v['_source']['sysdate']));
    
    $colString = PHPExcel_Cell::stringFromColumnIndex($i++);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.$curr_row, str_replace(array("T","Z"), " ", $v['_source']['sysdatemod']));
    
    $user = $v['_source']['Annotator'];
    $colString = PHPExcel_Cell::stringFromColumnIndex($i++);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.$curr_row, $user);
    
    foreach($_SESSION['postdata']['showsubitem'] as $a => $b) {
        $colString = PHPExcel_Cell::stringFromColumnIndex($i++);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($colString.$curr_row, $v['_source']['ScoreQuestion'.$a]);
    }
    
    $curr_row++;
}

// Set Column width
$lastColumn = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
$lastColumn = PHPExcel_Cell::columnIndexFromString($lastColumn);
for ($column = 0; $column <= $lastColumn; $column++) {
     $colString = PHPExcel_Cell::stringFromColumnIndex($column);
     $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($colString)->setWidth(20);
}

$objPHPExcel->getActiveSheet()->setTitle('Sensei Report');

// Add new sheet
$objWorkSheet = $objPHPExcel->createSheet(1);

//show post data
$i=0;
foreach($_SESSION['postdata'] as $k => $v) {
    $i++;
    $objWorkSheet->setCellValue('A'.$i, $k);
    $objWorkSheet->setCellValue('B'.$i, $v);
}

// Set Column width
$lastColumn = $objWorkSheet->getHighestColumn();
$lastColumn = PHPExcel_Cell::columnIndexFromString($lastColumn);
for ($column = 0; $column <= $lastColumn; $column++) {
     $colString = PHPExcel_Cell::stringFromColumnIndex($column);
     $objWorkSheet->getColumnDimension($colString)->setWidth(20);
}

$objWorkSheet->setTitle("Postdata");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.date('Ymd').'_report.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;