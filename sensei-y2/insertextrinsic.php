<?php
session_start();

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if (!$_SESSION["username"])
    header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/annotationform.php");
require("class/extrinsic.php");

//implementing ElasticSearch
require("class/elastic.php");

$config = Config::get_instance();
$extrinsic = extrinsic::get_instance();
$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('Listen Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$annotationform = Annotationform::get_instance();



$msgerr = "";
$nerr = 0;
$str = "*";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $lang->translate($_POST['lang']);

    //if postback is true by save button
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <?php echo $acof->head_tag(); ?>
        <script src="js/extrinsic.js"></script>
        <script language="javascript">
            var strSt = "<?php echo $lang->get_language($_SESSION["username"], 'LabSpeechListen', $_SESSION["language"]); ?>";
            //take the value of each checkbox 
            function ConCheckbox(idsubitem) {
                if (document.getElementById('ckGen' + idsubitem).checked == true) {
                    document.getElementById('FlagGen' + idsubitem).value = "Y";
                    document.getElementById('turn' + idsubitem).value = "";
                    document.getElementById('turn' + idsubitem).className = "nodroppable";
                } else {
                    document.getElementById('FlagGen' + idsubitem).value = "N";
                    document.getElementById('turn' + idsubitem).className = "droppable";
                    document.getElementById('turn' + idsubitem).value = strSt;
                }
            }
        </script>
       
    </head>
    <body>
        <?php $acof->navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']); ?>
        <div class="container">


            <div class="col-md-12 pre-scrollable" style="min-height: 100%;">
                <form action="insertextrinsic.php" method="post" id="form1" name="form1">
                    <input type="hidden" name="lang" id="lang" value="<?php echo $_POST['lang']; ?>"/>
                </form>
                <?php 
                
                ?>
            </div>
        </div>