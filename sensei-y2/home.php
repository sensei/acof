<?php
session_start();

if (!$_SESSION["username"]) header("Location: index.php"); // User not logged in, redirect to login page

require("class/config.php");
require("class/db.php");
require("class/log.php");
require("class/acof.php");
require("class/lang.php");
require("class/chart.php");

$config = Config::get_instance();

$db = Database::get_instance();
$db->connect();

$log = Log::get_instance();
$log->ins_log('Home Page', $_SESSION["username"]);

$acof = Acof::get_instance();
$lang = Lang::get_instance();
$chart = Chart::get_instance();

if ($_SERVER["REQUEST_METHOD"] == "POST" || $_SESSION['language']=='') $lang -> translate($_POST['lang']);
?>	

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
	<?php echo $acof -> head_tag('home'); ?>	
  </head>

  <body>
	<?php $acof -> navbar($_SESSION['language'], $_SESSION['name'], $_SESSION['surname']); ?>
	<form action="home.php" method="post" id="form1" name="form1">
		<input type="hidden" name="lang" id="lang" value="<?php echo $_POST['lang'];?>"/>
	</form>
	
	<div class="container">
		<br>
		<br>

		<?php
			//LabWelcomeHome
			$LabWelcomeHome = ($lang -> get_language($_SESSION['username'], 'LabWelcomeHome', $_SESSION['language']));
			echo ($acof -> show_info('', $LabWelcomeHome, '', $case='home'));
		
			$lastSevenDay = date('Ymd',time()-(7*86400)); // 7 days ago

			//extract the global monitoring
			$query   = "SELECT round(sum(score)/count(idListen)) as MediaScore";
			$query  .= ",(SELECT count(idListen) FROM tblListen where (DATE_FORMAT(sysdate, '%Y%m%d')>'20150212' or DATE_FORMAT(sysdatemod, '%Y%m%d')>'20150212') )as Tot";
			$query  .= ",(SELECT count(idListen) FROM tblListen where DATE_FORMAT(sysdate, '%Y%m%d') = DATE_FORMAT(now(), '%Y%m%d'))as TotDay";
			$query  .= ",(SELECT count(idListen) FROM tblListen where DATE_FORMAT(sysdate, '%Y%m%d') >= '".$lastSevenDay."')as TotWeek";
			//extract the user monitoring
			$query  .= ",(SELECT round(sum(score)/count(idListen)) FROM tblListen where sysuser='".$_SESSION["username"]."')as MyScore";
			$query  .= ",(SELECT count(idListen) FROM tblListen where sysuser='".$_SESSION["username"]."')as MyTot";
			$query  .= ",(SELECT count(idListen) FROM tblListen where DATE_FORMAT(sysdate, '%Y%m%d') = DATE_FORMAT(now(), '%Y%m%d') and sysuser='".$_SESSION["username"]."')as MyTotDay";
			$query  .= ",(SELECT count(idListen) FROM tblListen where DATE_FORMAT(sysdate, '%Y%m%d') >= '".$lastSevenDay."' and sysuser='".$_SESSION["username"]."')as MyTotWeek";
			
			$query  .= " FROM tblListen ";						
	
			$arrReturn = array();
			$fetchReturn =  $db->query_first($query);
			
			if (count($fetchReturn)==0){
				$msg = "Warning! Contact the web administrator.";
				echo $msg;
			}
			else
			{
				$Score = $fetchReturn['MediaScore'];
				$TotRow = $fetchReturn['Tot'];
				$TotDay = $fetchReturn['TotDay'];
				$TotWeek = $fetchReturn['TotWeek'];
				
				$MyScore = $fetchReturn['MyScore'];
				$MyTot = $fetchReturn['MyTot'];
				$MyTotDay = $fetchReturn['MyTotDay'];
				$MyTotWeek = $fetchReturn['MyTotWeek'];
			}
			
			$query   = "select Service as type,count(idListen)percent  ";
			$query  .= " from tblListen where (DATE_FORMAT(sysdate, '%Y%m%d')>'20150212' or DATE_FORMAT(sysdatemod, '%Y%m%d')>'20150212')";
			$query  .= " group by Service order by 2";
			$strJs = $chart->get_strJs($query);

			$query   = " select concat(service,'_',FileName) as type, ROUND(avg(Score)) as percent ";						
			$query  .= " from tblListen ";
			$query  .= " where score>0 and (DATE_FORMAT(sysdate, '%Y%m%d')>'20150212' or DATE_FORMAT(sysdatemod, '%Y%m%d')>'20150212')"; //since new release date
			$query  .= " group by FileName ORDER BY ROUND(avg(Score)) desc ";

			$strJsIsto = $chart->get_strJsIsto($query);		
		?>
		<script type="text/javascript">
			function getRandomColor() {
				var letters = '0123456789ABCDEF'.split('');
				var color = '#';
				for (var i = 0; i < 6; i++ ) {
					color += letters[Math.floor(Math.random() * 16)];
				}
				return color;
			}
			
			
			function CreatePie(){
				//SCRIPT FOR CREATE THE PIE CHARTS
				var chart;
				var legend;
				var selected;
				var types;
				//DATA ARRAY CREATED BY querycharts.php page
				types = [<?php echo $strJs;?>];

				function generateChartData () {
					var chartData = [];
					for (var i = 0; i < types.length; i++) {
						if (i == selected) {
							for (var x = 0; x < types[i].subs.length; x++) {
								chartData.push({
									type: types[i].subs[x].type,
									percent: types[i].subs[x].percent,
									color: types[i].color,
									pulled: true
								});
							}
						}
						else {
							chartData.push({
								type: types[i].type,
								percent: types[i].percent,
								color: types[i].color,
								id: i
							});
						}
					}
					return chartData;
				}

				AmCharts.ready(function() {
					// PIE CHART
					chart = new AmCharts.AmPieChart();
					chart.dataProvider = generateChartData();
					chart.titleField = "type";
					chart.valueField = "percent";
					chart.outlineColor = "#FFFFFF";
					chart.outlineAlpha = 0.8;
					chart.outlineThickness = 2;
					chart.colorField = "color";
					chart.pulledField = "pulled";
					
					// ADD TITLE
					//chart.addTitle("Click a slice to see the details");
					
					// AN EVENT TO HANDLE SLICE CLICKS
					chart.addListener("clickSlice", function (event) {
						if (event.dataItem.dataContext.id != undefined) {
							selected = event.dataItem.dataContext.id;
						}
						else {
							selected = undefined;
						}
						chart.dataProvider = generateChartData();
						chart.validateData();
					});

					//alert(orderChart);
					
					// WRITE
					chart.write("chartdivPie");
				});
			}
			
			
			function CreateCol(){
				var chart = AmCharts.makeChart("chartdivCol", {
					"type": "serial",
					"theme": "none",
					"pathToImages":"",
					"dataProvider": [<?php echo $strJsIsto;?>],
					"valueAxes": [{
						"axisAlpha": 0,
						"position": "left",
						"title": "Score"
					}],
					"startDuration": 0.5,
					"graphs": [{
						"balloonText": "<font size='1'>[[category]]:[[value]]</font>",
						"colorField": "color",
						"fillAlphas": 0.9,
						"lineAlpha": 0.1,
						"type": "column",
						"valueField": "percent"
					}],
					"chartCursor": {
						"categoryBalloonEnabled": true,
						"cursorAlpha": 0,
						"zoomable": true
					},
					"categoryField": "filename",
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 90
					},
					"amExport":{}
					 
				});
			}
			
			//show the charts
			CreatePie();
			CreateCol();
		</script>	
		<br><br>
		<table width="100%">
			<tr>
				<td width="55%">	
					<div>
						<h1>
						<?php 
							echo ($lang -> get_language($_SESSION['username'], 'LabActHome', $_SESSION['language']));
						?>
						</h1>
					</div>
				</td>
				<td width="45%">
					<div >
						<h1>
						<?php 
							echo ($lang -> get_language($_SESSION['username'], 'LabMyActHome', $_SESSION['language']));
						?>
						</h1>
					</div>					
				</td>
			</tr>
			<tr>
				<td>
					<p>			
						<a href="#">
							<?php 
								echo ($lang -> get_language($_SESSION['username'], 'LabScoreHome', $_SESSION['language']));
							?> 
							<span class="badge"><?php echo $Score;?> %</span>
						</a>			
					</p>	
					<p>
						<ul class="nav nav-pills">
							<li class="active">
								<a href="#">
									<?php 
										echo ($lang -> get_language($_SESSION['username'], 'LabTotHome', $_SESSION['language']));
									?>
									<span class="badge"><?php echo $TotRow;?></span>
								</a>
							</li>
							<li>
								<a href="#">
									<?php 
										echo ($lang -> get_language($_SESSION['username'], 'LabDayHome', $_SESSION['language']));
									?>
									<span class="badge"><?php echo $TotDay;?></span>
								</a>
							</li>
							<li>
								<a href="#">
									<?php 
										//echo getLanguage('LabWeekHome',$_SESSION['language']);
										echo ($lang -> get_language($_SESSION['username'], 'LabWeekHome', $_SESSION['language']));
									?>
									<span class="badge"><?php echo $TotWeek;?></span>
								</a>
							</li>
						</ul>
					</p>
				</td>
				<td>
					<p>			
						<a href="#">
							<?php 
								echo ($lang -> get_language($_SESSION['username'], 'LabScoreHome', $_SESSION['language']));
							?>
							<span class="badge"><?php echo $MyScore;?> %</span>
						</a>			
					</p>
					<p>
						<ul class="nav nav-pills">
							<li class="active">
								<a href="#">
									<?php 
										echo ($lang -> get_language($_SESSION['username'], 'LabTotHome', $_SESSION['language']));
									?>
									<span class="badge"><?php echo $MyTot;?></span>
								</a>
							</li>
							<li>
								<a href="#">
									<?php 
										echo ($lang -> get_language($_SESSION['username'], 'LabDayHome', $_SESSION['language']));
									?>
									<span class="badge"><?php echo $MyTotDay;?></span>
								</a>
							</li>
							<li>
								<a href="#">
									<?php 
										echo ($lang -> get_language($_SESSION['username'], 'LabWeekHome', $_SESSION['language']));
									?>
									<span class="badge"><?php echo $MyTotWeek;?></span>
								</a>
							</li>
						</ul>
					</p>	
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><div id="chartdivPie"></div></td>
				<td><div id="chartdivCol"></div></td>		
			</tr>
		</table>
	</div>
  </body>
</html>

<?php
	$db->close();
?>